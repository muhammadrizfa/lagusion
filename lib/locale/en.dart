var english = {
  //halaman home
  "see_all": "See all",
  "recent_song": "Recent Song",
  "recent_book": "Recent Book",
  "favorit": "Favorite",
  "unduhan": "Downloads",
  "unduh": "Download",
  "lagu": "Song",
  "buku": "Book",
  "trending": "Trending",
  "playlist": "Playlist",
  "audiobook": "Audio Book",
  "tersimpan": "Saved",

  //sidebar
  "pesan": "Inbox",
  "kirim_feedback": "Send Feedback",
  "feedback": "Feedback",
  "bagikan_aplikasi": "Share App",
  "tentang_aplikasi": "About App",

  //playlist
  "recently_added": "Recently Added",

  //Buku
  "ibadah": "Worship",

  //search
  "cari": "Search",
  "judul": "TITLE",
  "lirik": "LYRICS",
  "all_judul": "ALL TITLE",
  "all_lirik": "ALL LYRICS",

  // halaman pengaturan
  "pengaturan": "Settings",
  "lagu_utama": "Main Song",
  "jenis_audio": "Audio Type",
  "pengulangan_lagu": "Song Repetition",
  "pemutaran_bergilir": "Song Playback",
  "pemutaran_jenis_audio":
      "The audio type playback will not change when repeating the same song",
  "ukuran_text": "Text Size",
  "refrain": "Refrain in every verse",
  "teks_refrain":
      "The Refrain text will not be repeated at the end of each verse",
  "jenis_font": "Font Style",
  "ketika_menggunakan": "While using data connection",
  "ketika_terkoneksi": "While connected to the Wi-Fi",
  "tema": "Theme",
  "bahasa_aplikasi": "Language"
};
