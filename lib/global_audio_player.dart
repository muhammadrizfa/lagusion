import 'package:audioplayers/audioplayers.dart';

class LagusionAudioPlayer {
  static AudioPlayer _audioPlayer;

  static void init() {
    _audioPlayer = AudioPlayer();
  }

  static final AudioPlayer instance = _audioPlayer;
}
