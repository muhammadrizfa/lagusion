import 'dart:convert';

import 'package:music_mulai/util/shared_data_get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:music_mulai/model/banner_model.dart' as BannerModel;
import 'package:music_mulai/model/song_service/song_trending_model.dart'
    as SongTrendingModel;
import 'package:music_mulai/model/song_service/song_list_model.dart'
    as SongListModel;

class SharedDataSave {
  static saveDataSongList(
      List<SongListModel.Song> songs, List<SongListModel.Link> links) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString('songs', json.encode(songs));
    await _prefs.setString('links', json.encode(songs));
  }

  static saveDataTrendingSongList(
    List<SongTrendingModel.Song> trendingSongs,
  ) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString('trendingSongs', json.encode(trendingSongs));
  }

  static saveDataBanner(
    List<BannerModel.Banner> banners,
  ) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString('banners', json.encode(banners));
  }
}
