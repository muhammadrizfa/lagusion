import 'dart:convert';

import 'package:music_mulai/model/banner_model.dart' as BannerModel;
import 'package:music_mulai/model/song_service/song_trending_model.dart'
    as SongTrendingModel;
import 'package:music_mulai/model/song_service/song_list_model.dart'
    as SongListModel;
import 'package:shared_preferences/shared_preferences.dart';

//List Song Response - Begin
class SongsModelResponse {
  final List<SongListModel.Song> list;

  SongsModelResponse({
    this.list,
  });

  factory SongsModelResponse.fromJson(List<dynamic> parsedJson) {
    List<SongListModel.Song> list = new List<SongListModel.Song>();
    list = parsedJson.map((i) => SongListModel.Song.fromJson(i)).toList();

    return new SongsModelResponse(list: list);
  }
}

class LinksModelResponse {
  final List<SongListModel.Link> list;

  LinksModelResponse({
    this.list,
  });

  factory LinksModelResponse.fromJson(List<dynamic> parsedJson) {
    List<SongListModel.Link> list = new List<SongListModel.Link>();
    list = parsedJson.map((i) => SongListModel.Link.fromJson(i)).toList();

    return new LinksModelResponse(list: list);
  }
}
//List Song Response - End

class TrendingSongsModelResponse {
  final List<SongTrendingModel.Song> list;

  TrendingSongsModelResponse({
    this.list,
  });

  factory TrendingSongsModelResponse.fromJson(List<dynamic> parsedJson) {
    List<SongTrendingModel.Song> list = new List<SongTrendingModel.Song>();
    list = parsedJson.map((i) => SongTrendingModel.Song.fromJson(i)).toList();

    return new TrendingSongsModelResponse(list: list);
  }
}

class BannerModelResponse {
  final List<BannerModel.Banner> list;

  BannerModelResponse({
    this.list,
  });

  factory BannerModelResponse.fromJson(List<dynamic> parsedJson) {
    List<BannerModel.Banner> list = new List<BannerModel.Banner>();
    list = parsedJson.map((i) => BannerModel.Banner.fromJson(i)).toList();

    return new BannerModelResponse(list: list);
  }
}

class GetSharedData {
  static List<SongListModel.Song> songListData;
  static List<SongListModel.Link> songLinkListData;
  static List<SongTrendingModel.Song> trendingSongListData;
  static List<BannerModel.Banner> bannerListData;

  static getAllData() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();

    final List parsedSongs = json.decode(_prefs.get('songs'));
    final List parsedLinks = json.decode(_prefs.get('links'));
    final List parsedTrending = json.decode(_prefs.get('trendingSongs'));
    final List parsedBanner = json.decode(_prefs.get('banners'));

    List<SongListModel.Song> listSongResponse =
        new SongsModelResponse.fromJson(parsedSongs).list;

    List<SongListModel.Link> listLinkResponse =
        new LinksModelResponse.fromJson(parsedLinks).list;

    List<SongTrendingModel.Song> listTrendingSongResponse =
        new TrendingSongsModelResponse.fromJson(parsedTrending).list;

    List<BannerModel.Banner> listBannerModelResponse =
        new BannerModelResponse.fromJson(parsedBanner).list;

    GetSharedData.songListData = listSongResponse;
    GetSharedData.songLinkListData = listLinkResponse;
    GetSharedData.trendingSongListData = listTrendingSongResponse;
    GetSharedData.bannerListData = listBannerModelResponse;
  }
}
