import 'dart:io';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GlobalVariable {
  static String textGetDataFailed = 'Gagal mendapatkan data';
  static String textOfflineMode = 'Kamu sedang dalam mode offline';
  static bool isConnected = false;
}

class GlobalFunction {
  static checkConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        GlobalVariable.isConnected = true;
      }
    } on SocketException catch (_) {
      print('no internet');
      GlobalVariable.isConnected = false;
    }
  }

}

class GlobalComponent {
  static feedbackFlushbar(BuildContext context, String errorMessage) {
    Flushbar(
      isDismissible: true,
      flushbarPosition: FlushbarPosition.TOP,
      duration: Duration(seconds: 3),
      backgroundColor: Colors.orange,
      message: errorMessage,
    )..show(context);
  }
}
