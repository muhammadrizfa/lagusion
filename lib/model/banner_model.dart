// To parse this JSON data, do
//
//     final bannerModel = bannerModelFromJson(jsonString);

import 'dart:convert';

BannerModel bannerModelFromJson(String str) => BannerModel.fromJson(json.decode(str));

String bannerModelToJson(BannerModel data) => json.encode(data.toJson());

class BannerModel {
    BannerModel({
        this.status,
        this.message,
        this.banners,
    });

    String status;
    String message;
    List<Banner> banners;

    factory BannerModel.fromJson(Map<String, dynamic> json) => BannerModel(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        banners: json["banners"] == null ? null : List<Banner>.from(json["banners"].map((x) => Banner.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "banners": banners == null ? null : List<dynamic>.from(banners.map((x) => x.toJson())),
    };
}

class Banner {
    Banner({
        this.id,
        this.authorId,
        this.title,
        this.message,
        this.img,
        this.deletedAt,
        this.createdAt,
        this.updatedAt,
        this.author,
    });

    int id;
    int authorId;
    String title;
    String message;
    String img;
    DateTime deletedAt;
    String createdAt;
    String updatedAt;
    Author author;

    factory Banner.fromJson(Map<String, dynamic> json) => Banner(
        id: json["id"] == null ? null : json["id"],
        authorId: json["author_id"] == null ? null : json["author_id"],
        title: json["title"] == null ? null : json["title"],
        message: json["message"] == null ? null : json["message"],
        img: json["img"] == null ? null : json["img"],
        deletedAt: json["deleted_at"] == null ? null : DateTime.parse(json["deleted_at"]),
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        author: json["author"] == null ? null : Author.fromJson(json["author"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "author_id": authorId == null ? null : authorId,
        "title": title == null ? null : title,
        "message": message == null ? null : message,
        "img": img == null ? null : img,
        "deleted_at": deletedAt == null ? null : "${deletedAt.year.toString().padLeft(4, '0')}-${deletedAt.month.toString().padLeft(2, '0')}-${deletedAt.day.toString().padLeft(2, '0')}",
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "author": author == null ? null : author.toJson(),
    };
}

class Author {
    Author({
        this.id,
        this.name,
        this.username,
        this.email,
        this.phone,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    String name;
    String username;
    String email;
    String phone;
    String createdAt;
    String updatedAt;

    factory Author.fromJson(Map<String, dynamic> json) => Author(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        username: json["username"] == null ? null : json["username"],
        email: json["email"] == null ? null : json["email"],
        phone: json["phone"] == null ? null : json["phone"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "username": username == null ? null : username,
        "email": email == null ? null : email,
        "phone": phone == null ? null : phone,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
    };
}
