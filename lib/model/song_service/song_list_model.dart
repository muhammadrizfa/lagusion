// To parse this JSON data, do
//
//     final songListModel = songListModelFromJson(jsonString);

import 'dart:convert';

SongListModel songListModelFromJson(String str) => SongListModel.fromJson(json.decode(str));

String songListModelToJson(SongListModel data) => json.encode(data.toJson());

class SongListModel {
    SongListModel({
        this.status,
        this.message,
        this.links,
        this.songs,
    });

    String status;
    String message;
    List<Link> links;
    List<Song> songs;

    factory SongListModel.fromJson(Map<String, dynamic> json) => SongListModel(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        links: json["links"] == null ? null : List<Link>.from(json["links"].map((x) => Link.fromJson(x))),
        songs: json["songs"] == null ? null : List<Song>.from(json["songs"].map((x) => Song.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
        "songs": songs == null ? null : List<dynamic>.from(songs.map((x) => x.toJson())),
    };
}

class Link {
    Link({
        this.id,
        this.music,
    });

    int id;
    String music;

    factory Link.fromJson(Map<String, dynamic> json) => Link(
        id: json["id"] == null ? null : json["id"],
        music: json["music"] == null ? null : json["music"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "music": music == null ? null : music,
    };
}

class SongVersion {
    SongVersion({
        this.id,
        this.versionName,
        this.file,
        this.createdAt,
        this.updatedAt,
        this.songId,
        this.song,
    });

    int id;
    String versionName;
    String file;
    dynamic createdAt;
    String updatedAt;
    int songId;
    Song song;

    factory SongVersion.fromJson(Map<String, dynamic> json) => SongVersion(
        id: json["id"] == null ? null : json["id"],
        versionName: json["version_name"] == null ? null : json["version_name"],
        file: json["file"] == null ? null : json["file"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        songId: json["song_id"] == null ? null : json["song_id"],
        song: json["song"] == null ? null : Song.fromJson(json["song"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "version_name": versionName == null ? null : versionName,
        "file": file == null ? null : file,
        "created_at": createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "song_id": songId == null ? null : songId,
        "song": song == null ? null : song.toJson(),
    };
}

class Song {
    Song({
        this.id,
        this.albumId,
        this.title,
        this.artist,
        this.thumbnail,
        this.duration,
        this.basicNotes,
        this.story,
        this.musicalNotes,
        this.music,
        this.views,
        this.createdAt,
        this.updatedAt,
        this.index,
        this.code,
        this.correlations,
        this.album,
        this.verses,
        this.songVersions,
    });

    int id;
    int albumId;
    String title;
    String artist;
    String thumbnail;
    String duration;
    String basicNotes;
    String story;
    String musicalNotes;
    String music;
    int views;
    String createdAt;
    String updatedAt;
    int index;
    String code;
    List<Correlation> correlations;
    Album album;
    List<Verse> verses;
    List<SongVersion> songVersions;

    factory Song.fromJson(Map<String, dynamic> json) => Song(
        id: json["id"] == null ? null : json["id"],
        albumId: json["album_id"] == null ? null : json["album_id"],
        title: json["title"] == null ? null : json["title"],
        artist: json["artist"] == null ? null : json["artist"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        duration: json["duration"] == null ? null : json["duration"],
        basicNotes: json["basic_notes"] == null ? null : json["basic_notes"],
        story: json["story"] == null ? null : json["story"],
        musicalNotes: json["musical_notes"] == null ? null : json["musical_notes"],
        music: json["music"] == null ? null : json["music"],
        views: json["views"] == null ? null : json["views"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        index: json["index"] == null ? null : json["index"],
        code: json["code"] == null ? null : json["code"],
        correlations: json["correlations"] == null ? null : List<Correlation>.from(json["correlations"].map((x) => Correlation.fromJson(x))),
        album: json["album"] == null ? null : Album.fromJson(json["album"]),
        verses: json["verses"] == null ? null : List<Verse>.from(json["verses"].map((x) => Verse.fromJson(x))),
        songVersions: json["song_versions"] == null ? null : List<SongVersion>.from(json["song_versions"].map((x) => SongVersion.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "album_id": albumId == null ? null : albumId,
        "title": title == null ? null : title,
        "artist": artist == null ? null : artist,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "duration": duration == null ? null : duration,
        "basic_notes": basicNotes == null ? null : basicNotes,
        "story": story == null ? null : story,
        "musical_notes": musicalNotes == null ? null : musicalNotes,
        "music": music == null ? null : music,
        "views": views == null ? null : views,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "index": index == null ? null : index,
        "code": code == null ? null : code,
        "correlations": correlations == null ? null : List<dynamic>.from(correlations.map((x) => x.toJson())),
        "album": album == null ? null : album.toJson(),
        "verses": verses == null ? null : List<dynamic>.from(verses.map((x) => x.toJson())),
        "song_versions": songVersions == null ? null : List<dynamic>.from(songVersions.map((x) => x.toJson())),
    };
}

class Album {
    Album({
        this.id,
        this.thumbnail,
        this.albumName,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    String thumbnail;
    String albumName;
    String createdAt;
    String updatedAt;

    factory Album.fromJson(Map<String, dynamic> json) => Album(
        id: json["id"] == null ? null : json["id"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        albumName: json["album_name"] == null ? null : json["album_name"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "album_name": albumName == null ? null : albumName,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
    };
}

class Correlation {
    Correlation({
        this.id,
        this.albumId,
        this.title,
        this.code,
        this.albumName,
        this.correlation,
    });

    int id;
    int albumId;
    String title;
    String code;
    String albumName;
    int correlation;

    factory Correlation.fromJson(Map<String, dynamic> json) => Correlation(
        id: json["id"] == null ? null : json["id"],
        albumId: json["album_id"] == null ? null : json["album_id"],
        title: json["title"] == null ? null : json["title"],
        code: json["code"] == null ? null : json["code"],
        albumName: json["album_name"] == null ? null : json["album_name"],
        correlation: json["correlation"] == null ? null : json["correlation"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "album_id": albumId == null ? null : albumId,
        "title": title == null ? null : title,
        "code": code == null ? null : code,
        "album_name": albumName == null ? null : albumName,
        "correlation": correlation == null ? null : correlation,
    };
}

class Verse {
    Verse({
        this.id,
        this.songId,
        this.duration,
        this.versePart,
        this.lyrics,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    int songId;
    String duration;
    String versePart;
    String lyrics;
    String createdAt;
    String updatedAt;

    factory Verse.fromJson(Map<String, dynamic> json) => Verse(
        id: json["id"] == null ? null : json["id"],
        songId: json["song_id"] == null ? null : json["song_id"],
        duration: json["duration"] == null ? null : json["duration"],
        versePart: json["part"] == null ? null : json["part"],
        lyrics: json["lyrics"] == null ? null : json["lyrics"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "song_id": songId == null ? null : songId,
        "duration": duration == null ? null : duration,
        "part": versePart == null ? null : versePart,
        "lyrics": lyrics == null ? null : lyrics,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
    };
}
