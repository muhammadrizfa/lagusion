// To parse this JSON data, do
//
//     final searchIbadahModel = searchIbadahModelFromJson(jsonString);

import 'dart:convert';

SearchIbadahModel searchIbadahModelFromJson(String str) => SearchIbadahModel.fromJson(json.decode(str));

String searchIbadahModelToJson(SearchIbadahModel data) => json.encode(data.toJson());

class SearchIbadahModel {
    SearchIbadahModel({
        this.status,
        this.message,
        this.ibadahs,
    });

    String status;
    String message;
    List<Ibadah> ibadahs;

    factory SearchIbadahModel.fromJson(Map<String, dynamic> json) => SearchIbadahModel(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        ibadahs: json["ibadahs"] == null ? null : List<Ibadah>.from(json["ibadahs"].map((x) => Ibadah.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "ibadahs": ibadahs == null ? null : List<dynamic>.from(ibadahs.map((x) => x.toJson())),
    };
}

class Ibadah {
    Ibadah({
        this.id,
        this.thumbnail,
        this.ibadahName,
        this.createdAt,
        this.updatedAt,
        this.parts,
    });

    int id;
    String thumbnail;
    String ibadahName;
    dynamic createdAt;
    dynamic updatedAt;
    List<Part> parts;

    factory Ibadah.fromJson(Map<String, dynamic> json) => Ibadah(
        id: json["id"] == null ? null : json["id"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        ibadahName: json["ibadah_name"] == null ? null : json["ibadah_name"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        parts: json["parts"] == null ? null : List<Part>.from(json["parts"].map((x) => Part.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "ibadah_name": ibadahName == null ? null : ibadahName,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "parts": parts == null ? null : List<dynamic>.from(parts.map((x) => x.toJson())),
    };
}

class Part {
    Part({
        this.id,
        this.ibadahId,
        this.title,
        this.duration,
        this.lyrics,
        this.music,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    int ibadahId;
    String title;
    String duration;
    String lyrics;
    String music;
    dynamic createdAt;
    dynamic updatedAt;

    factory Part.fromJson(Map<String, dynamic> json) => Part(
        id: json["id"] == null ? null : json["id"],
        ibadahId: json["ibadah_id"] == null ? null : json["ibadah_id"],
        title: json["title"] == null ? null : json["title"],
        duration: json["duration"] == null ? null : json["duration"],
        lyrics: json["lyrics"] == null ? null : json["lyrics"],
        music: json["music"] == null ? null : json["music"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "ibadah_id": ibadahId == null ? null : ibadahId,
        "title": title == null ? null : title,
        "duration": duration == null ? null : duration,
        "lyrics": lyrics == null ? null : lyrics,
        "music": music == null ? null : music,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
