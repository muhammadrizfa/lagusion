import 'package:carousel_slider/carousel_slider.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:music_mulai/bloc/main_page/home/home_bloc.dart';
import 'package:music_mulai/model/banner_model.dart' as BannerModel;
import 'package:music_mulai/model/song_service/song_trending_model.dart'
    as SongTrendingModel;
import 'package:music_mulai/util/shared_data_get.dart';
import 'package:music_mulai/util/utils.dart';

class HomeLayout extends StatefulWidget {
  @override
  _HomeLayoutState createState() => _HomeLayoutState();
}

class _HomeLayoutState extends State<HomeLayout> {
  List<BannerModel.Banner> _bannerModel;
  List<SongTrendingModel.Song> _songTrendingModel;
  bool _onLoad = false;
  int _currentBanner = 0;

  @override
  void initState() {
    // BlocProvider.of<HomeBloc>(context).add(HomeInit());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (context, state) {
        if (state is HomeLoading) {
          _onLoad = true;
        } else if (state is HitApiFailed) {
          GlobalComponent.feedbackFlushbar(context, state.errorMessage);
        } else if (state is HomeGetBannerSuccess) {
          _bannerModel = state.bannerModel.banners;
        } else if (state is HomeGetSongTrendingSuccess) {
          _onLoad = false;
          _songTrendingModel = state.songTrendingModel.songs;
        }
      },
      child: BlocBuilder<HomeBloc, HomeState>(builder: (context, homeState) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            width: double.infinity,
            child: RefreshIndicator(
              child: _onLoad ? myCircularProgressIndicator() : _viewBanner(),
              onRefresh: () async {
                setState(() {
                  _onLoad = true;
                  BlocProvider.of<HomeBloc>(context).add(HomeRefresh());
                });
              },
            ),
          ),
        );
      }),
    );
  }

  //View Layout - Begin
  Widget _viewBanner() => ListView(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          CarouselSlider.builder(
            itemCount: GetSharedData.bannerListData.length,
            options: CarouselOptions(
                autoPlay: false,
                enlargeCenterPage: true,
                height: 200.0,
                initialPage: 0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _currentBanner = index;
                  });
                }),
            itemBuilder: (BuildContext context, int i) => Container(
              child: GestureDetector(
                onTap: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) => DetailPesan(
                  //           banner[i]['img'],
                  //           banner[i]['title'],
                  //           banner[i]['message'])),
                  // );
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(GetSharedData.bannerListData[i].img,
                        fit: BoxFit.cover),
                  ),
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: GetSharedData.bannerListData.map((banner) {
              int index = GetSharedData.bannerListData.indexOf(banner);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _currentBanner == index
                      ? Colors.orange
                      : Colors.grey.withOpacity(0.3),
                ),
              );
            }).toList(),
          ),
        ],
      );
  //View Layout - End

  //View Utilities - Begin
  myCircularProgressIndicator() => Center(
        child: CircularProgressIndicator(),
      );
  //View Utilities - End

  //Form Function - Begin
  //Form Function - End

  //Get Function - Begin
  _sendEvent(eventName) {
    BlocProvider.of<HomeBloc>(context).add(eventName);
  }
  //Get Function - End

  //Post Function - Begin
  //Post Function - End

  //Process Function - Begin
  //Process Function - End

  //Show Function - Begin
  //Show Function - End

}
