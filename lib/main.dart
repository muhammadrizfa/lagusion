import 'dart:async';
import 'dart:convert';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_mulai/bloc/lagusion_player_control_cubit.dart';
import 'package:music_mulai/detail/audiobook/downloaded/downloaded_book.dart';
import 'package:music_mulai/detail/search.dart';
import 'package:music_mulai/global_audio_player.dart';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/detail/sidebar/about.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:music_mulai/detail/sidebar/feedback.dart';
import 'package:music_mulai/provider/lagu_sion_model.dart';
import 'package:music_mulai/tabs/lagu_sion_home.dart';
import 'package:music_mulai/tabs/buku.dart';
import 'package:music_mulai/tabs/lagu.dart';
import 'package:music_mulai/tabs/main_page.dart';
import 'package:music_mulai/tabs/playlist.dart';
import 'package:music_mulai/detail/sidebar/pengaturan.dart';
import 'package:music_mulai/detail/sidebar/pesan.dart';
import 'package:music_mulai/detail/audiobook/search_buku.dart';
import 'package:music_mulai/view/launcher/splash/splash_page.dart';
import 'package:music_mulai/view/main_page/home/home_page.dart';
import 'package:music_mulai/view/main_page/songs/songs_page.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'bloc/audio_model.dart';
import 'bloc/lagusion_player_state.dart';
import 'detail/audiobook/detail_book.dart';
import 'detail/audiobook/detail_ibadah.dart';
import 'detail/buku_icons.dart';
import 'package:flutter_downloader/flutter_downloader.dart';

import 'mini_player_widget.dart';

const debug = true;
Brightness brightness;
void main() async {
  LagusionAudioPlayer.init();
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(debug: debug);
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  Brightness brightness;
  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
        defaultBrightness: Brightness.light,
        data: (brightness) => new ThemeData(
              primarySwatch: Colors.deepOrange,
              brightness: brightness,
            ),
        themedWidgetBuilder: (context, theme) {
          return MultiProvider(
            providers: [
              Provider<AppDatabase>(
                create: (context) => AppDatabase(),
                dispose: (context, db) => db.close(),
              ),
              ChangeNotifierProvider<HomeModel>.value(value: HomeModel()),
              BlocProvider(
                create: (context) => LagusionPlayerControlCubit(),
              )
            ],
            child: new MaterialApp(
              title: 'Sion',
              theme: theme,
              home: new SplashPage(),
              routes: <String, WidgetBuilder>{
                '/searchnumpad': (BuildContext context) => new MyHomePage(4),
                '/buku': (BuildContext context) => new MyHomePage(3),
              },
            ),
          );
        });
  }
}

class MyHomePage extends StatefulWidget {
  final index;
  MyHomePage(this.index);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var value = 0.1;
  var language;
  var jenis_font = 'Roboto';
  var bottomNavigationSize = 60.0;
  LagusionPlayerControlCubit _playerControlCubit;

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
      if (prefs.getString('jenis_font') == null) {
        jenis_font = 'Roboto';
      } else {
        jenis_font = prefs.getString('jenis_font');
      }
    });
  }

  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';
  var flume =
      'https://i.scdn.co/image/8d84f7b313ca9bafcefcf37d4e59a8265c7d3fff';

  List<dynamic> laguSion;
  var lagu_utama;

  Future getDataLagu() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    lagu_utama = prefs.getString('lagu_utama') == null
        ? laguSion[0]['album']['album_name']
        : prefs.getString('lagu_utama');
    http.Response response =
        await http.post(apiMaster + '/album/search', headers: {
      'Accept': 'application/json',
    }, body: {
      'album_name': lagu_utama,
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> laguDataMentah = json.decode(response.body);
      laguSion = laguDataMentah["albums"][0]['songs'];
      // print(laguSion);
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    var styleFont;
    if (fontstyle == 'Roboto') {
      styleFont = GoogleFonts.roboto(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Average') {
      styleFont = GoogleFonts.average(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Aleo') {
      styleFont = GoogleFonts.aleo(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Delius') {
      styleFont = GoogleFonts.delius(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Vollkorn') {
      styleFont = GoogleFonts.vollkorn(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    }
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: styleFont,
    );
  }

  int currentTab = 0;

  getLaguUtama() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      lagu_utama = prefs.getString('lagu_utama');
    });
    Future.delayed(Duration(seconds: 5), () {
      getLaguUtama();
    });
  }

  @override
  void initState() {
    _playerControlCubit = context.bloc();
    ukuranText();
    // getLaguUtama();
    getDataLagu();
    currentTab = widget.index;
  }

  final List<Widget> screens = [
    MyHomePage(0),
  ];
  Widget currentScreen = MyHomePage(0);
  final _appbar = <Widget>[
    Text("Home"),
    Text("Playlist"),
    Text("Lagu"),
    Text("Buku"),
    Container(
      transform: Matrix4.translationValues(-20.0, 0.0, 0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 35.0,
                width: 35.0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(
                    'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(left: 5),
            width: 200,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Lagu Sion",
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        ],
      ),
    ),
    Container(
      // padding: EdgeInsets.only(top: 10),
      transform: Matrix4.translationValues(-20.0, -10.0, 0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 35.0,
                width: 35.0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(
                    'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(left: 5),
            width: 200,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Lagu Sion Edisi Baru",
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  ];

  final _pages = <Widget>[
    HalamanUtama(),
    PlaylistPage(),
    LaguPage(),
    BukuPage(),
    LaguSionBaruHome(),
  ];

  Widget _buildSearch() {
    if (currentTab == 3) {
      return IconButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => SearchBukuPage()));
        },
        icon: Icon(Icons.search),
      );
    } else {
      return IconButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => SearchPage()));
        },
        icon: Icon(Icons.search),
      );
    }
  }

  TextEditingController _textFieldController = TextEditingController();

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          var nilai = '';
          return StatefulBuilder(builder: (context, setState) {
            return Dialog(
              child: Container(
                margin: EdgeInsets.only(bottom: 20, top: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.centerRight,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                nilai,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Theme.of(context)
                                                .brightness
                                                .toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black),
                              ),
                              Divider()
                            ],
                          ),
                        ),
                        FlatButton(
                          child: Icon(
                            Icons.backspace,
                            color: Theme.of(context).brightness.toString() ==
                                    'Brightness.dark'
                                ? Colors.white
                                : Colors.black.withOpacity(0.5),
                          ),
                          onPressed: () {
                            setState(() {
                              if (nilai.length > 0) {
                                nilai = nilai.substring(0, nilai.length - 1);
                              }
                            });
                          },
                          onLongPress: () {
                            setState(() {
                              nilai = '';
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '1';
                                  });
                                },
                                child: Container(
                                  child: Text('1',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '2';
                                  });
                                },
                                child: Container(
                                  child: Text('2',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '3';
                                  });
                                },
                                child: Container(
                                  child: Text('3',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '4';
                                  });
                                },
                                child: Container(
                                  child: Text('4',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '5';
                                  });
                                },
                                child: Container(
                                  child: Text('5',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '6';
                                  });
                                },
                                child: Container(
                                  child: Text('6',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '7';
                                  });
                                },
                                child: Container(
                                  child: Text('7',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '8';
                                  });
                                },
                                child: Container(
                                  child: Text('8',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '9';
                                  });
                                },
                                child: Container(
                                  child: Text('9',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Stack(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SearchPage()));
                                    },
                                    child: Container(
                                      child: Text('ABC',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16)),
                                    ),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        nilai = nilai + '0';
                                      });
                                    },
                                    child: Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 30),
                                      child: Text('0',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 30)),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      // Navigator.push(
                                      //     context,
                                      //     MaterialPageRoute(
                                      //         builder: (context) =>
                                      //             SearchNumpad(
                                      //               search: nilai,
                                      //             )));
                                      Navigator.pop(context);
                                      laguSion.length < int.parse(nilai)
                                          ? showDialog(
                                              context: context,
                                              builder: (context) {
                                                return StatefulBuilder(builder:
                                                    (context, setState) {
                                                  return AlertDialog(
                                                    title: Text(
                                                        'Lagu tidak tersedia'),
                                                    content: Text(
                                                        'Lagu yang anda masukkan tidak tersedia'),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        onPressed: () =>
                                                            Navigator.pop(
                                                                context),
                                                        child: Text('OK'),
                                                      ),
                                                    ],
                                                  );
                                                });
                                              },
                                            )
                                          : Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailMusicApp(
                                                        indexCurrentPlay:
                                                            int.parse(nilai) -
                                                                1,
                                                        listMusic: laguSion
                                                            .map((e) =>
                                                                SongModel
                                                                    .fromJson(
                                                                        e))
                                                            .toList()
                                                      )),
                                            );
                                    },
                                    child: Container(
                                      child: Text('OK',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: currentTab == 4
          ? null
          : AppBar(
              title: currentTab == 2
                  ? tampil_text('lagu', 20, jenis_font, FontWeight.normal, null)
                  : currentTab == 3
                      ? tampil_text(
                          'buku', 20, jenis_font, FontWeight.normal, null)
                      : _appbar[currentTab],
              actions: <Widget>[
                _buildSearch(),
                currentTab == 3
                    ? IconButton(
                        icon: Icon(Icons.file_download),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ListDownloadBuku()),
                          );
                        },
                      )
                    : Container()
              ],
            ),
      
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 155.0,
              child: DrawerHeader(
                child: Row(
                  children: <Widget>[
                    Container(
                      transform: Matrix4.translationValues(0, -13, 0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: Colors.white,
                        ),
                        width: 60,
                        height: 60,
                        child: Icon(
                          Icons.music_note,
                          size: 30,
                          color: Colors.deepOrange,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Online MP3",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 25 + (5 * value)),
                            textAlign: TextAlign.left,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: 190,
                            child: Text(
                              "Play your favorite Songs",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15 + (5 * value)),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Colors.deepOrange,
                ),
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.home),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Home',
                    style: TextStyle(fontSize: 15 + (5 * value)),
                  ),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.settings),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text(
                      'pengaturan', 15, jenis_font, FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PengaturanPage()));
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.mail),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text('pesan', 15, jenis_font, FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PesanPage()));
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.feedback),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text('kirim_feedback', 15, jenis_font,
                      FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FeedBackPage()));
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.share),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text('bagikan_aplikasi', 15, jenis_font,
                      FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.info_outline),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text('tentang_aplikasi', 15, jenis_font,
                      FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AboutPage()));
              },
            ),
          ],
        ),
      ),
      body: BlocListener<LagusionPlayerControlCubit, LagusionPlayerState>(
        listener: (context, state) {
          if (state is LagusionIsPlaying) {
            setState(() {
              bottomNavigationSize = 140.0;
            });
          } else {
            setState(() {
              bottomNavigationSize = 60.0;
            });
          }
        },
        child: IndexedStack(
          index: currentTab,
          children: <Widget>[
            // HalamanUtama(),
            HomePage(),
            PlaylistPage(),
            // LaguPage(),
            SongsPage(),
            BukuPage(),
            LaguSionBaruHome(),
          ],
        ),
      ),

      // _pages[currentTab],
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final SharedPreferences prefs = await SharedPreferences.getInstance();
          setState(() {
            currentTab = 4;
            prefs.setString('pencarian_numpad', _textFieldController.text);
          });
          // Navigator.push(
          //     context, MaterialPageRoute(builder: (context) => LaguSionBaru()));
        },
        child: currentTab == 4
            ? IconButton(
                onPressed: () {
                  _displayDialog(context);
                },
                icon: Icon(
                  Icons.dialpad,
                  size: 33,
                  color: Colors.white,
                ),
              )
            : Image(
                image: AssetImage('assets/logo.png'),
                fit: BoxFit.cover,
                width: 25,
              ),
        backgroundColor: Colors.deepOrange,
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: bottomNavigationSize,
          child: Column(
            children: [
              Container(
                height: 60,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        MaterialButton(
                          minWidth: 40,
                          onPressed: () {
                            setState(() {
                              currentTab = 0;
                            });
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.home,
                                color: currentTab == 0
                                    ? Colors.deepOrange
                                    : Colors.grey,
                              ),
                              Text(
                                'Home',
                                style: TextStyle(
                                  color: currentTab == 0
                                      ? Colors.deepOrange
                                      : Colors.grey,
                                ),
                              ),
                            ],
                          ),
                        ),
                        MaterialButton(
                          minWidth: 40,
                          onPressed: () {
                            setState(() {
                              currentTab = 1;
                            });
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.queue_music,
                                color: currentTab == 1
                                    ? Colors.deepOrange
                                    : Colors.grey,
                              ),
                              Text(
                                'Playlist',
                                style: TextStyle(
                                  color: currentTab == 1
                                      ? Colors.deepOrange
                                      : Colors.grey,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),

                    // Right Tab bar icons

                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        MaterialButton(
                          minWidth: 40,
                          onPressed: () {
                            setState(() {
                              currentTab = 2;
                            });
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.chrome_reader_mode,
                                color: currentTab == 2
                                    ? Colors.deepOrange
                                    : Colors.grey,
                              ),
                              tampil_text(
                                'lagu',
                                14,
                                jenis_font,
                                FontWeight.normal,
                                currentTab == 2
                                    ? Colors.deepOrange
                                    : Colors.grey,
                              ),
                            ],
                          ),
                        ),
                        MaterialButton(
                          minWidth: 40,
                          onPressed: () {
                            setState(() {
                              currentTab = 3;
                            });
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 5),
                                child: Icon(
                                  Buku.buku,
                                  color: currentTab == 3
                                      ? Colors.deepOrange
                                      : Colors.grey,
                                  size: 20,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 3),
                                child: tampil_text(
                                  'buku',
                                  14,
                                  jenis_font,
                                  FontWeight.normal,
                                  currentTab == 3
                                      ? Colors.deepOrange
                                      : Colors.grey,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              MiniPlayerWidget()
            ],
          ),
        ),
      ),
    );
  }



}

class MenuMore {
  static const String Unduh = 'Unduh';
  static const String LaguUtama = 'Jadikan Lagu Utama';

  static const List<String> choices = <String>[
    Unduh,
    LaguUtama,
  ];
}

class SongSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 200,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 10,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              transform: Matrix4.translationValues(-8, 0, 0),
                              child: Icon(
                                Icons.play_arrow,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.6),
                                size: 25.0,
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(8.0, 0.0, 0.0),
                              child: PopupMenuButton<String>(
                                child: Icon(Icons.more_vert, size: 18),
                                itemBuilder: (BuildContext context) {
                                  return MenuMore.choices.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            Container(
                              height: 10,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}

class SongItem extends StatelessWidget {
  final title;
  final artist;
  final image;
  final durasi;
  final settings;
  SongItem(this.title, this.artist, this.image, this.durasi, this.settings);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onTap: () => Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => DetailMusicApp()),
      // ),
      child: Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 0.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 45.0,
                    width: 45.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        image,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                transform: Matrix4.translationValues(
                    -MediaQuery.of(context).size.width * 3 / 100, 0, 0),
                padding: EdgeInsets.only(right: 0),
                width: 180,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Theme.of(context).brightness.toString() ==
                                  'Brightness.dark'
                              ? Colors.white
                              : Colors.black,
                          fontSize: 16.5,
                          fontFamily: 'Roboto'),
                    ),
                    SizedBox(height: 5.0),
                    Text(
                      artist,
                      style: TextStyle(
                        color: Theme.of(context).brightness.toString() ==
                                'Brightness.dark'
                            ? Colors.white
                            : Colors.black.withOpacity(0.5),
                        fontSize: 16.5,
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        transform: Matrix4.translationValues(30.0, 0.0, 0.0),
                        padding: EdgeInsets.only(left: 0, top: 5),
                        child: Text(
                          settings,
                          style: TextStyle(fontSize: 10),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(
                              left:
                                  MediaQuery.of(context).size.width * 7 / 100),
                          height: 25.0,
                          width: 25.0,
                          child: Icon(
                            Icons.play_arrow,
                            color: Theme.of(context).brightness.toString() ==
                                    'Brightness.dark'
                                ? Colors.white
                                : Colors.black.withOpacity(0.6),
                            size: 25.0,
                          )),
                    ],
                  ),
                ],
              ),
              GestureDetector(
                onTap: () => print("Hello guys"),
                child: Padding(
                    padding: EdgeInsets.only(right: 12),
                    child: Column(
                      children: <Widget>[
                        Container(
                          transform: Matrix4.translationValues(15.0, 0.0, 0.0),
                          child: Icon(
                            Icons.more_vert,
                            color: Theme.of(context).brightness.toString() ==
                                    'Brightness.dark'
                                ? Colors.white
                                : Colors.black.withOpacity(0.6),
                            size: 20.0,
                          ),
                        ),
                        Container(
                          transform: Matrix4.translationValues(8.0, 0.0, 0.0),
                          child: Text(durasi),
                        ),
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
