import 'dart:convert';
import 'dart:math';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/bloc/lagusion_player_control_cubit.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/sidebar/feedback.dart';
import 'package:music_mulai/global_audio_player.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;
import 'package:flutter_bloc/flutter_bloc.dart';

// class DetailBookApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(body: DetailBook());
//   }
// }

class DetailBook extends StatefulWidget {
  final audiobook;
  final index;
  final id;
  final judul;
  final thumbnail;
  DetailBook(
    this.judul,
    this.id,
    this.index,
    this.audiobook,
    this.thumbnail,
  );
  @override
  _DetailBookState createState() => _DetailBookState();
}

class _DetailBookState extends State<DetailBook> {
  var language = 'indonesia';
  Duration _duration = Duration();
  Duration _position = Duration();
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  LagusionPlayerControlCubit _audioControlCubit;

  var ambil_durasi;
  var ambil_posisi;
  var _audiobook;
  var music;
  var status;
  var lirik;
  var index;
  var judul;
  var thumbnail;
  var repeat_status = 'none';
  var newvalue;
  var pojok_kanan = false;

  @override
  void initState() {
    super.initState();
    _audioControlCubit = context.bloc();
    newvalue = _position.inSeconds.toDouble();
    print(widget.id['id']);
    initPlayer();
    thumbnail = widget.thumbnail;
    // cekStatus();
    index = widget.index;
    repeat_status = 'none';
    cek_audio_play();
    cekDurasi();
    ukuranText();
    lirik = widget.audiobook[index]["lyrics"];
  }

  var value;

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
    });
    Future.delayed(Duration(seconds: 1), () {
      ukuranText();
    });
  }

  cekStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    music = prefs.getString('music').toString();
    setState(() {});
  }

  var cek_music = 'tidak sama';

  cek_audio_play() async {
    var id_musik;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    id_musik = prefs.getString('audio_play');
    setState(() {
      if (widget.audiobook[index]['id'].toString() == id_musik) {
        cek_music = 'sama';
        status = 'play';
        print('continue');
        advancedPlayer.play(widget.audiobook[index]['music'].toString());
        playAudio(
            index, widget.audiobook, widget.thumbnail);
        // set_audio_play(widget.audiobook[index]['id'].toString());
      } else {
        cek_music = 'tidak sama';
        status = 'play';
        advancedPlayer.stop();
        advancedPlayer.play(widget.audiobook[index]['music'].toString());
        _audioControlCubit.playAudio(
            index, widget.audiobook, AudioPlayerDetailType.BOOK);
        // set_audio_play(widget.audiobook[index]['id'].toString());
      }
    });
    set_audio_play(widget.audiobook[index]['id'].toString());
  }

  void playAudio(index, audioBook, thumb) {
    _audioControlCubit.playAudio(index, audioBook, AudioPlayerDetailType.BOOK,
        thumb: thumb);
  }

  set_audio_play(id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('audio_play', id);
      prefs.commit();
    });
  }

  var counterPlay = 0;

  void cekDurasi() async {
    setState(() {
      advancedPlayer.onPlayerCompletion.listen((event) {
        if (repeat_status == "repeat_all") {
          advancedPlayer.play(widget.audiobook[index]['music'].toString());
        } else if (repeat_status == "repeat_one") {
          counterPlay = counterPlay + 1;
          if (counterPlay == 1) {
            next();
          } else {
            advancedPlayer.play(widget.audiobook[index]['music'].toString());
          }
        } else if (repeat_status == "shuffle") {
          Random random = new Random();
          int randomNumber = random.nextInt(widget.audiobook.length);

          setState(() {
            index = randomNumber;
          });

          advancedPlayer.stop();
          advancedPlayer.play(widget.audiobook[index]['music'].toString());
        } else {
          next();
        }
      });
      ambil_durasi = _duration;
      ambil_posisi = _position;
      print(ambil_posisi.toString() + ' PER ' + ambil_durasi.toString());
    });
    Future.delayed(Duration(seconds: 1), () {
      if (mounted) {
        cekDurasi();
      }
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, bahasa) {
    return Text(
      bahasa == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: ukuranfont == 0
          ? TextStyle(fontFamily: fontstyle)
          : TextStyle(
              fontSize: ukuranfont + (5 * value),
              fontFamily: fontstyle,
            ),
    );
  }

  void initPlayer() {
    advancedPlayer = LagusionAudioPlayer.instance;
    audioCache = AudioCache(fixedPlayer: advancedPlayer);

    advancedPlayer.durationHandler = (d) => {
          if (mounted)
            {
              setState(() {
                _duration = d;
              })
            }
        };

    advancedPlayer.positionHandler = (p) => {
          if (mounted)
            {
              setState(() {
                _position = p;
              })
            }
        };
  }

  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';

  next() async {
    setState(() {
      if (widget.audiobook.length - 1 == index) {
        index = 0;
        cekDurasi();
      } else {
        index = index + 1;
        cekDurasi();
      }
    });
    advancedPlayer.stop();
    advancedPlayer.play(widget.audiobook[index]['music'].toString());
    playAudio(
        index, widget.audiobook, widget.thumbnail);
    lirik = widget.audiobook[index]["lyrics"];
    status = 'play';
  }

  prev() async {
    setState(() {
      if (index == 0) {
        index = widget.audiobook.length - 1;
        cekDurasi();
      } else {
        index = index - 1;
        cekDurasi();
      }
    });
    print(widget.index);
    advancedPlayer.stop();
    advancedPlayer.play(widget.audiobook[index]['music'].toString());
    playAudio(
        index, widget.audiobook, widget.thumbnail);
    status = 'play';
  }

  repeat() async {
    // setState(() {
    //   if (repeat_status == "on") {
    //     repeat_status = "off";
    //   } else {
    //     repeat_status = "on";
    //   }
    // });
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Song Repeat'),
          children: <Widget>[
            RadioListTile(
              value: 'Repeat All',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.repeat),
                  SizedBox(
                    width: 5,
                  ),
                  Text('Repeat All'),
                ],
              ),
            ),
            RadioListTile(
              value: 'Shuffle',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.shuffle),
                  SizedBox(
                    width: 5,
                  ),
                  Text('Shuffle'),
                ],
              ),
            ),
            RadioListTile(
              value: 'Repeat One',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.repeat_one),
                  SizedBox(
                    width: 5,
                  ),
                  Text('Repeat One'),
                ],
              ),
            ),
            RadioListTile(
              value: 'None',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.link_off),
                  SizedBox(
                    width: 5,
                  ),
                  Text('None'),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);
    advancedPlayer.seek(newDuration);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: BackButton()),
        title: GestureDetector(
          onTap: () {
            setState(() {
              pojok_kanan = false;
            });
          },
          child: Container(
            transform: Matrix4.translationValues(-20.0, 0.0, 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 35.0,
                      width: 35.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          thumbnail,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(left: 5),
                  width: MediaQuery.of(context).size.width - 275,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.judul,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 16.5 + (5 * value), fontFamily: 'Roboto'),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        widget.audiobook[index]["title"],
                        style: TextStyle(fontSize: 13 + (5 * value)),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Share.share('Dengarkan Audio Book ' +
                  widget.audiobook[index]["title"] +
                  ' di aplikasi Lagu Sion Plus');
            },
            icon: Icon(Icons.share),
          ),
          IconButton(
            onPressed: () {
              if (pojok_kanan == true) {
                pojok_kanan = false;
              } else {
                pojok_kanan = true;
              }
            },
            icon: Icon(Icons.more_vert),
          ),
        ],
      ),
      body: GestureDetector(
        onTap: () {
          setState(() {
            pojok_kanan = false;
          });
        },
        child: WillPopScope(
          onWillPop: () async {
            final database = Provider.of<AppDatabase>(context, listen: false);
            final recentBook = RecentBook(
              book_id: widget.audiobook[index]["id"],
              created_at: DateTime.now(),
            );

            database.insertRecentBook(recentBook);
            // advancedPlayer.stop();
            return true;
          },
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      // height: MediaQuery.of(context).size.height * 62 / 100,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: NetworkImage(thumbnail),
                                    fit: BoxFit.cover)),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                    Theme.of(context).brightness.toString() !=
                                            'Brightness.dark'
                                        ? Colors.white.withOpacity(0.5)
                                        : Colors.black.withOpacity(0.5),
                                    Theme.of(context).brightness.toString() !=
                                            'Brightness.dark'
                                        ? Colors.white.withOpacity(0.93)
                                        : Colors.black.withOpacity(0.93),
                                  ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 12.0),
                            child: ListView(
                              children: <Widget>[
                                SizedBox(height: 32.0),
                                Container(
                                  padding: EdgeInsets.only(left: 10, right: 10),
                                  child: Row(children: <Widget>[
                                    Container(
                                        width: 100,
                                        height: 100,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image:
                                                    NetworkImage(thumbnail)))),
                                    Padding(
                                      padding: EdgeInsets.only(left: 20),
                                      child: Text(
                                        widget.audiobook[index]["title"],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 22),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    )
                                  ]),
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 25),
                                  child: Text(lirik.toString(),
                                      style: TextStyle(
                                          color: Theme.of(context)
                                                      .brightness
                                                      .toString() ==
                                                  'Brightness.dark'
                                              ? Colors.white
                                              : Colors.black,
                                          fontSize: 15 + (5 * value))),
                                ),
                                SizedBox(
                                  height: 6.0,
                                ),
                                SizedBox(height: 16.0),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          ambil_posisi.toString().substring(2, 7),
                          style: TextStyle(
                              color: Theme.of(context).brightness.toString() ==
                                      'Brightness.dark'
                                  ? Colors.white
                                  : Colors.black.withOpacity(0.7),
                              fontSize: 15 + (5 * value)),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 125,
                          child: Slider(
                            // value: _position.inSeconds.toDouble(),
                            value: newvalue,
                            min: 0.0,
                            max: _duration.inSeconds.toDouble() + 1,
                            onChanged: (double value) {
                              setState(() {
                                seekToSecond(value.toInt());
                                newvalue = value;
                              });
                            },
                            activeColor: Colors.amber[900],
                          ),
                        ),
                        Text(widget.audiobook[index]["duration"].toString(),
                            style: TextStyle(
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.7),
                                fontSize: 15 + (5 * value)))
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        repeat_status == 'none'
                            ? GestureDetector(
                                onTap: () async {
                                  final SharedPreferences prefs =
                                      await SharedPreferences.getInstance();
                                  setState(() {
                                    repeat_status = 'repeat_all';
                                    prefs.setString(
                                        'pengulangan_lagu', repeat_status);
                                  });
                                },
                                child: Icon(
                                  Icons.repeat,
                                  color:
                                      Theme.of(context).brightness.toString() ==
                                              'Brightness.dark'
                                          ? Colors.white
                                          : Colors.black54,
                                  size: 32.0,
                                ),
                              )
                            : repeat_status == 'repeat_all'
                                ? GestureDetector(
                                    onTap: () async {
                                      final SharedPreferences prefs =
                                          await SharedPreferences.getInstance();
                                      setState(() {
                                        repeat_status = 'repeat_one';
                                        prefs.setString(
                                            'pengulangan_lagu', repeat_status);
                                        prefs.commit();
                                      });
                                    },
                                    child: Icon(
                                      Icons.repeat,
                                      color: Colors.deepOrange,
                                      size: 32.0,
                                    ),
                                  )
                                : repeat_status == 'repeat_one'
                                    ? GestureDetector(
                                        onTap: () async {
                                          final SharedPreferences prefs =
                                              await SharedPreferences
                                                  .getInstance();
                                          setState(() {
                                            repeat_status = 'shuffle';
                                            prefs.setString('pengulangan_lagu',
                                                repeat_status);
                                            prefs.commit();
                                          });
                                        },
                                        child: Icon(
                                          Icons.repeat_one,
                                          color: Colors.deepOrange,
                                          size: 32.0,
                                        ),
                                      )
                                    : repeat_status == 'shuffle'
                                        ? GestureDetector(
                                            onTap: () async {
                                              final SharedPreferences prefs =
                                                  await SharedPreferences
                                                      .getInstance();
                                              setState(() {
                                                repeat_status = 'none';
                                                prefs.setString(
                                                    'pengulangan_lagu',
                                                    repeat_status);
                                                prefs.commit();
                                              });
                                            },
                                            child: Icon(
                                              Icons.shuffle,
                                              color: Colors.deepOrange,
                                              size: 32.0,
                                            ),
                                          )
                                        : Container(),
                        Row(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                prev();
                              },
                              child: Icon(
                                Icons.skip_previous,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black54,
                                size: 42.0,
                              ),
                            ),
                            SizedBox(
                              width: 32,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.amber[900],
                                  borderRadius: BorderRadius.circular(50.0)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: IconButton(
                                  icon: Icon(
                                    status == "play"
                                        ? Icons.pause
                                        : Icons.play_arrow,
                                    size: 32.0,
                                    color: Colors.white,
                                  ),
                                  onPressed: () async {
                                    if (ambil_durasi.toString() ==
                                        '0:00:00.000000') {
                                      print('wait');
                                    } else {
                                      status == 'play'
                                          ? setState(() {
                                              status = 'pause';
                                              advancedPlayer.pause();
                                            })
                                          : setState(() {
                                              status = 'play';
                                              advancedPlayer.play(widget
                                                  .audiobook[index]['music']
                                                  .toString());
                                              playAudio(
                                                  index, widget.audiobook, widget.thumbnail);
                                            });
                                    }
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 32,
                            ),
                            GestureDetector(
                              onTap: () {
                                next();
                              },
                              child: Icon(
                                Icons.skip_next,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black54,
                                size: 42.0,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(width: 20),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                ],
              ),
              pojok_kanan == true
                  ? Positioned(
                      top: 0,
                      right: 10,
                      child: Container(
                        padding: EdgeInsets.only(top: 20),
                        width: 200,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Theme.of(context).brightness.toString() ==
                                      'Brightness.dark'
                                  ? Colors.black
                                  : Colors.grey.withOpacity(0.9),
                              blurRadius:
                                  10.0, // has the effect of softening the shadow
                              spreadRadius:
                                  0.0, // has the effect of extending the shadow
                              offset: Offset(
                                0.0, // horizontal, move right 10
                                0.0, // vertical, move down 10
                              ),
                            )
                          ],
                          color: Theme.of(context).brightness.toString() ==
                                  'Brightness.dark'
                              ? Color(0xff4d4d4d)
                              : Colors.white,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 20),
                                      child: tampil_text(
                                          'tema', 15, 'Roboto', language)),
                                  RadioListTile<Brightness>(
                                    value: Brightness.light,
                                    groupValue: Theme.of(context).brightness,
                                    onChanged: (Brightness value) {
                                      DynamicTheme.of(context)
                                          .setBrightness(Brightness.light);
                                    },
                                    title: const Text('Default'),
                                  ),
                                  RadioListTile<Brightness>(
                                    value: Brightness.dark,
                                    groupValue: Theme.of(context).brightness,
                                    onChanged: (Brightness value) {
                                      DynamicTheme.of(context)
                                          .setBrightness(Brightness.dark);
                                    },
                                    title: const Text('Gelap'),
                                  ),
                                ],
                              ),
                            ),
                            Divider(),
                            ListTile(
                              title: tampil_text(
                                  'ukuran_text', 15, 'Roboto', language),
                              subtitle: Slider(
                                onChanged: (double value_) async {
                                  setState(() {
                                    value = value_;
                                  });
                                  SharedPreferences prefs =
                                      await SharedPreferences.getInstance();
                                  prefs.setDouble('ukuran_text', value_);
                                  prefs.commit();
                                },
                                value: value,
                                activeColor: Colors.amber[900],
                              ),
                            ),
                            Divider(),
                            GestureDetector(
                              onTap: () {
                                pojok_kanan = false;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => FeedBackPage()));
                              },
                              child: Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  padding: EdgeInsets.only(bottom: 10),
                                  child: tampil_text(
                                      'feedback', 15, 'Roboto', language)),
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
