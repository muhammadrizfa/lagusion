import 'dart:convert';
import 'dart:math';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/bloc/lagusion_player_control_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/global_audio_player.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;

// class DownloadedDetailIbadahApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(body: DownloadedDetailIbadah());
//   }
// }

class DownloadedDetailIbadah extends StatefulWidget {
  final ibadah;
  final index;
  final id;
  final judul;
  final thumbnail;
  DownloadedDetailIbadah(
    this.judul,
    this.id,
    this.index,
    this.ibadah,
    this.thumbnail,
  );
  @override
  _DownloadedDetailIbadahState createState() => _DownloadedDetailIbadahState();
}

class _DownloadedDetailIbadahState extends State<DownloadedDetailIbadah> {
  Duration _duration = Duration();
  Duration _position = Duration();
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  LagusionPlayerControlCubit _audioControlCubit;

  var ambil_durasi;
  var ambil_posisi;
  var _ibadah;
  var music;
  var status;
  var lirik;
  var index;
  var judul;
  var thumbnail;
  var repeat_status = 'none';
  var newvalue;

  @override
  void initState() {
    super.initState();
    _audioControlCubit = context.bloc();
    newvalue = _position.inSeconds.toDouble();
    // print(widget.id.id);
    initPlayer();
    thumbnail = widget.thumbnail;
    // cekStatus();
    index = widget.index;
    repeat_status = "none";
    cek_audio_play();
    cekDurasi();
    ukuranText();
    lirik = widget.ibadah[index].lyrics;
  }

  var value;

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
    });
    Future.delayed(Duration(seconds: 1), () {
      ukuranText();
    });
  }

  cekStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    music = prefs.getString('music').toString();
    setState(() {});
  }

  var cek_music = 'tidak sama';

  cek_audio_play() async {
    var id_musik;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    id_musik = prefs.getString('audio_play');
    setState(() {
      if (widget.ibadah[index].id.toString() == id_musik) {
        cek_music = 'sama';
        status = 'play';
        print('continue');
        advancedPlayer.play(widget.ibadah[index].music.toString());
        _audioControlCubit.playAudio(
            index, widget.ibadah, AudioPlayerDetailType.PRAY);
        // set_audio_play(widget.audiobook[index].id.toString());
      } else {
        cek_music = 'tidak sama';
        status = 'play';
        advancedPlayer.stop();
        advancedPlayer.play(widget.ibadah[index].music.toString());
        _audioControlCubit.playAudio(
            index, widget.ibadah, AudioPlayerDetailType.PRAY);
        // set_audio_play(widget.audiobook[index].id.toString());
      }
    });
    set_audio_play(widget.ibadah[index].id.toString());
  }

  set_audio_play(id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('audio_play', id);
      prefs.commit();
    });
  }

  var counterPlay = 0;

  void cekDurasi() async {
    setState(() {
      advancedPlayer.onPlayerCompletion.listen((event) {
        if (repeat_status == "repeat_all") {
          advancedPlayer.play(widget.ibadah[index].music.toString());
        } else if (repeat_status == "repeat_one") {
          counterPlay = counterPlay + 1;
          if (counterPlay == 1) {
            next();
          } else {
            advancedPlayer.play(widget.ibadah[index].music.toString());
          }
        } else if (repeat_status == "shuffle") {
          Random random = new Random();
          int randomNumber = random.nextInt(widget.ibadah.length);

          setState(() {
            index = randomNumber;
          });

          advancedPlayer.stop();
          advancedPlayer.play(widget.ibadah[index].music.toString());
        } else {
          next();
        }
      });
      ambil_durasi = _duration;
      ambil_posisi = _position;
      print(ambil_posisi.toString() + ' PER ' + ambil_durasi.toString());
    });
    Future.delayed(Duration(seconds: 1), () {
      if (mounted) {
        cekDurasi();
      }
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, bahasa) {
    return Text(
      bahasa == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: ukuranfont == 0
          ? TextStyle(fontFamily: fontstyle)
          : TextStyle(
              fontSize: ukuranfont + (5 * value),
              fontFamily: fontstyle,
            ),
    );
  }

  void initPlayer() {
    advancedPlayer = LagusionAudioPlayer.instance;
    audioCache = AudioCache(fixedPlayer: advancedPlayer);

    advancedPlayer.durationHandler = (d) => {
          if (mounted)
            {
              setState(() {
                _duration = d;
              })
            }
        };

    advancedPlayer.positionHandler = (p) => {
          if (mounted)
            {
              setState(() {
                _position = p;
              })
            }
        };
  }

  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';

  next() async {
    setState(() {
      if (widget.ibadah.length - 1 == index) {
        index = 0;
        cekDurasi();
      } else {
        index = index + 1;
        cekDurasi();
      }
    });
    advancedPlayer.stop();
    advancedPlayer.play(widget.ibadah[index].music.toString());
    _audioControlCubit.playAudio(
        index, widget.ibadah, AudioPlayerDetailType.PRAY);
    lirik = widget.ibadah[index]["lyrics"];
    status = 'play';
  }

  prev() async {
    setState(() {
      if (index == 0) {
        index = widget.ibadah.length - 1;
        cekDurasi();
      } else {
        index = index - 1;
        cekDurasi();
      }
    });
    print(widget.index);
    advancedPlayer.stop();
    advancedPlayer.play(widget.ibadah[index].music.toString());
    _audioControlCubit.playAudio(
        index, widget.ibadah, AudioPlayerDetailType.PRAY);
    status = 'play';
  }

  repeat() async {
    // setState(() {
    //   if (repeat_status == "on") {
    //     repeat_status = "off";
    //   } else {
    //     repeat_status = "on";
    //   }
    // });
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Song Repeat'),
          children: <Widget>[
            RadioListTile(
              value: 'Repeat All',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.repeat),
                  SizedBox(
                    width: 5,
                  ),
                  Text('Repeat All'),
                ],
              ),
            ),
            RadioListTile(
              value: 'Shuffle',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.shuffle),
                  SizedBox(
                    width: 5,
                  ),
                  Text('Shuffle'),
                ],
              ),
            ),
            RadioListTile(
              value: 'Repeat One',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.repeat_one),
                  SizedBox(
                    width: 5,
                  ),
                  Text('Repeat One'),
                ],
              ),
            ),
            RadioListTile(
              value: 'None',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.link_off),
                  SizedBox(
                    width: 5,
                  ),
                  Text('None'),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);
    advancedPlayer.seek(newDuration);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: BackButton()),
        title: Container(
          transform: Matrix4.translationValues(-20.0, 0.0, 0.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 35.0,
                    width: 35.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        thumbnail,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(left: 5),
                width: MediaQuery.of(context).size.width - 275,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.judul,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 16.5 + (5 * value), fontFamily: 'Roboto'),
                    ),
                    SizedBox(height: 5.0),
                    Text(
                      widget.ibadah[index]["title"],
                      style: TextStyle(fontSize: 13 + (5 * value)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Share.share('Dengarkan Audio Book ' +
                  widget.ibadah[index]["title"] +
                  ' di aplikasi Lagu Sion Plus');
            },
            icon: Icon(Icons.share),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.more_vert),
          ),
        ],
      ),
      body: WillPopScope(
        onWillPop: () async {
          // final database = Provider.of<AppDatabase>(context, listen: false);
          // final recentBook = RecentBook(
          //   book_id: widget.ibadah[index]["id"],
          //   created_at: DateTime.now(),
          // );

          // database.insertRecentBook(recentBook);
          // advancedPlayer.stop();
          return true;
        },
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    // height: MediaQuery.of(context).size.height * 62 / 100,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(thumbnail),
                                  fit: BoxFit.cover)),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                                  Theme.of(context).brightness.toString() !=
                                          'Brightness.dark'
                                      ? Colors.white.withOpacity(0.5)
                                      : Colors.black.withOpacity(0.5),
                                  Theme.of(context).brightness.toString() !=
                                          'Brightness.dark'
                                      ? Colors.white.withOpacity(0.93)
                                      : Colors.black.withOpacity(0.93),
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: ListView(
                            children: <Widget>[
                              SizedBox(height: 32.0),
                              Container(
                                padding: EdgeInsets.only(left: 10, right: 10),
                                child: Row(children: <Widget>[
                                  Container(
                                      width: 100,
                                      height: 100,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: NetworkImage(thumbnail)))),
                                  Padding(
                                    padding: EdgeInsets.only(left: 20),
                                    child: Text(
                                      widget.ibadah[index]["title"],
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  )
                                ]),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, top: 25),
                                child: Text(lirik.toString(),
                                    style: TextStyle(
                                        color: Theme.of(context)
                                                    .brightness
                                                    .toString() ==
                                                'Brightness.dark'
                                            ? Colors.white
                                            : Colors.black,
                                        fontSize: 15.0)),
                              ),
                              SizedBox(
                                height: 6.0,
                              ),
                              SizedBox(height: 16.0),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        ambil_posisi.toString().substring(2, 7),
                        style: TextStyle(
                            color: Theme.of(context).brightness.toString() ==
                                    'Brightness.dark'
                                ? Colors.white
                                : Colors.black.withOpacity(0.7),
                            fontSize: 15 + (5 * value)),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width - 125,
                        child: Slider(
                          // value: _position.inSeconds.toDouble(),
                          value: newvalue,
                          min: 0.0,
                          max: _duration.inSeconds.toDouble() + 1,
                          onChanged: (double value) {
                            setState(() {
                              seekToSecond(value.toInt());
                              newvalue = value;
                            });
                          },
                          activeColor: Colors.amber[900],
                        ),
                      ),
                      Text(widget.ibadah[index]["duration"].toString(),
                          style: TextStyle(
                              color: Theme.of(context).brightness.toString() ==
                                      'Brightness.dark'
                                  ? Colors.white
                                  : Colors.black.withOpacity(0.7),
                              fontSize: 15 + (5 * value)))
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          repeat();
                        },
                        child: repeat_status == 'Repeat All'
                            ? Icon(
                                Icons.repeat,
                                color: Colors.deepOrange,
                                size: 32.0,
                              )
                            : repeat_status == 'Repeat One'
                                ? Icon(
                                    Icons.repeat_one,
                                    color: Colors.deepOrange,
                                    size: 32.0,
                                  )
                                : repeat_status == 'Shuffle'
                                    ? Icon(
                                        Icons.shuffle,
                                        color: Colors.deepOrange,
                                        size: 32.0,
                                      )
                                    : Icon(
                                        Icons.repeat,
                                        color: Theme.of(context)
                                                    .brightness
                                                    .toString() ==
                                                'Brightness.dark'
                                            ? Colors.white
                                            : Colors.black54,
                                        size: 32.0,
                                      ),
                      ),
                      Row(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              prev();
                            },
                            child: Icon(
                              Icons.skip_previous,
                              color: Theme.of(context).brightness.toString() ==
                                      'Brightness.dark'
                                  ? Colors.white
                                  : Colors.black54,
                              size: 42.0,
                            ),
                          ),
                          SizedBox(
                            width: 32,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.amber[900],
                                borderRadius: BorderRadius.circular(50.0)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: IconButton(
                                icon: Icon(
                                  status == "play"
                                      ? Icons.pause
                                      : Icons.play_arrow,
                                  size: 32.0,
                                  color: Colors.white,
                                ),
                                onPressed: () async {
                                  if (ambil_durasi.toString() ==
                                      '0:00:00.000000') {
                                    print('wait');
                                  } else {
                                    status == 'play'
                                        ? setState(() {
                                            status = 'pause';
                                            advancedPlayer.pause();
                                          })
                                        : setState(() {
                                            status = 'play';
                                            advancedPlayer.play(widget
                                                .ibadah[index].music
                                                .toString());
                                            _audioControlCubit.playAudio(
                                                index,
                                                widget.ibadah,
                                                AudioPlayerDetailType.PRAY);
                                          });
                                  }
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 32,
                          ),
                          GestureDetector(
                            onTap: () {
                              next();
                            },
                            child: Icon(
                              Icons.skip_next,
                              color: Theme.of(context).brightness.toString() ==
                                      'Brightness.dark'
                                  ? Colors.white
                                  : Colors.black54,
                              size: 42.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: 20),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
