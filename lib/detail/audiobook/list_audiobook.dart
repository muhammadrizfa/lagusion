import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:music_mulai/bloc/audio_book_model.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/audiobook/detail_book.dart';
import 'package:music_mulai/detail/lagu/search_numpad.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:convert';

import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class ListAudioBook extends StatefulWidget {
  final AudioBookModel audioBookModel;

  const ListAudioBook({Key key, this.audioBookModel}) : super(key: key);
  @override
  _ListAudioBookState createState() => _ListAudioBookState();
}

class _ListAudioBookState extends State<ListAudioBook> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.audioBookModel.audioBookName.capitalize(),
          overflow: TextOverflow.ellipsis,
        ),
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10, right: 0, top: 10),
        child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.audioBookModel.parts.length,
            itemBuilder: (context, int index) {
              final part = widget.audioBookModel.parts[index];
              return Container(
                padding: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        // Provider.of<HomeModel>(context).stop();
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailBook(
                                  widget.audioBookModel.audioBookName,
                                  part.toJson(),
                                  index,
                                  widget.audioBookModel.parts.map((e) => e.toJson()).toList(),
                                  widget.audioBookModel.thumbnail)),
                        );
                      },
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                  color: Colors.grey.withOpacity(0.4),
                                  borderRadius:
                                  BorderRadius.circular(10)),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Image.network(
                                  widget.audioBookModel.thumbnail,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "${part.title} $progressStringAudioBook",
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                .brightness
                                                .toString() ==
                                                'Brightness.dark'
                                                ? Colors.white
                                                : Colors.black,
                                            fontSize: 16.5,
                                            fontFamily: 'Roboto'),
                                      ),
                                      SizedBox(height: 5.0),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            transform:
                                            Matrix4.translationValues(
                                                -8, 0, 0),
                                            child: Icon(
                                              Icons.play_arrow,
                                              color: Theme.of(context)
                                                  .brightness
                                                  .toString() ==
                                                  'Brightness.dark'
                                                  ? Colors.white
                                                  : Colors.black
                                                  .withOpacity(0.6),
                                              size: 25.0,
                                            ),
                                          )
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Container(
                                            transform:
                                            Matrix4.translationValues(
                                                8.0, 0.0, 0.0),
                                            child:
                                            StreamBuilder<bool>(
                                              stream: Provider.of<AppDatabase>(
                                                  context,
                                                  listen: false)
                                                  .isBookPartDownloaded(widget.audioBookModel.id, part.id),
                                              builder: (context, snapshot){
                                                if (snapshot.hasData) {
                                                  List<String> choiceMenu;
                                                  if (snapshot.data) {
                                                    choiceMenu = ["Downloaded"];
                                                  } else {
                                                    choiceMenu = MenuMore.choices;
                                                  }
                                                  return PopupMenuButton<String>(
                                                    child: Icon(Icons.more_vert, size: 18),
                                                    onSelected: (choice) async {
                                                      if (choice == MenuMore.Unduh) {
                                                        downloadBookFile(part);
                                                      }
                                                    },
                                                    itemBuilder: (BuildContext context) {
                                                      Provider.of<AppDatabase>(
                                                          context,
                                                          listen: false);
                                                      return choiceMenu
                                                          .map((String choice) {
                                                        return PopupMenuItem<String>(
                                                          value: choice,
                                                          child: Text(choice),
                                                        );
                                                      }).toList();
                                                    },
                                                  );
                                                }
                                                return Container();
                                              },
                                            ),
                                          ),
                                          Text(part.duration
                                              .replaceAll(
                                              new RegExp(
                                                  r"\s+\b|\b\s"),
                                              ""))
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Divider(),
                  ],
                ),
              );
            }),
      ),
    );
  }

  String progressStringAudioBook = "";

  Future<String> downloadFile(String urlAudio) async {
    Dio dio = Dio();
    try {
      final dir = await getApplicationDocumentsDirectory();
      final String fileName = "${Uuid().v1()}.mp3";
      final String newFilePath = "${dir.path}/$fileName";
      await dio.download(urlAudio,
          newFilePath,
          onReceiveProgress: (rec, total) {
            print("Rec : $rec, Total : $total");
            setState(() {
              progressStringAudioBook =
                  ((rec / total) * 100).toStringAsFixed(0) + '%';
            });
          });
      print("Location downloaded file ->> $newFilePath");
      setState(() {
        progressStringAudioBook = "";
      });
      return newFilePath;
    } catch (e) {
      print(e);
      return null;
    }
  }

  bool downloadingAudioBook = false;

  Future<void> downloadBookFile(Parts part) async {
    setState(() {
      downloadingAudioBook = true;
    });
    final database = Provider.of<AppDatabase>(context, listen: false);
    final path = await downloadFile(part.music);
    final bookPart = AudioBookPart(
        createdAt: DateTime.now(),
        bookId: widget.audioBookModel.id,
        partId: part.id,
        data:json.encode(part.toJson())
    );
    database.insertBookParts(bookPart);

    final downloadedPart = DownloadedBookPartData(
        createdAt: DateTime.now(),
        bookId: widget.audioBookModel.id,
        partId: part.id,
        localFilePath: path
    );
    database.insertDownloadedBookPart(downloadedPart);

    setState(() {
      downloadingAudioBook = false;
    });
  }

}

class MenuMore {
  static const String Unduh = 'Unduh';
  static const List<String> choices = <String>[
    Unduh,
  ];
}
