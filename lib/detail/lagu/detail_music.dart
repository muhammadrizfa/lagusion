import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/bloc/lagusion_player_control_cubit.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/sidebar/feedback.dart';
import 'package:music_mulai/global_audio_player.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:translator/translator.dart';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../util/list_ext.dart';
import '../../util/file_ext.dart';

class DetailMusicApp extends StatefulWidget {
  final List<SongModel> listMusic;
  final int indexCurrentPlay;

  const DetailMusicApp({Key key, this.listMusic, this.indexCurrentPlay})
      : super(key: key);

  @override
  _DetailMusicAppState createState() => _DetailMusicAppState();
}

class _DetailMusicAppState extends State<DetailMusicApp> {
  Duration _duration = new Duration();
  Duration _position = new Duration();
  final cacheManager = DefaultCacheManager();
  AudioPlayer advancedPlayer;
  AudioCache audioCache;
  final CarouselController _coruselController = CarouselController();

  var music;
  var posisi;
  var ambil_posisi;
  var ambil_durasi;
  var repeat_status = 'none';
  var newvalue;
  var value;
  var defaultLyrics;
  var _isTranslated;
  var language = 'indonesia';

  GoogleTranslator translator = new GoogleTranslator();

  void translate() {
    setState(() {
      _isTranslated = true;
    });
  }

  var show_more = false;

  var checker;
  var onOrOff = false;
  var _currentIndexPlaying = 0;
  var _currentIndexSlider = 1;
  var styleFontLirik;
  List<SongModel> _listMusicModel = List();
  List<SongModel> _listSongVersion = List();
  SongModel currentMusic;
  int index;

  LagusionPlayerControlCubit _audioControlCubit;

  @override
  void initState() {
    super.initState();
    _audioControlCubit = context.bloc<LagusionPlayerControlCubit>();
    newvalue = _position.inSeconds.toDouble();
    initPlayer();
    repeat_status = "none";
    cek_music_play();
    cekDurasi();
    ukuranText();
    notifyCurrentPlayingVersion();
    defaultLyrics = "-No lyrics-";
    index = widget.indexCurrentPlay;
  }

  void notifyCurrentPlayingVersion() {
    _listSongVersion.clear();
    currentMusic = widget.listMusic[widget.indexCurrentPlay];
    _listSongVersion.add(currentMusic);

    if (currentMusic.songVersions.isEmpty) {
      _listSongVersion.insert(0, SongModel(title: ""));
      _listSongVersion.insert(2, SongModel(title: ""));
      _listSongVersion[1].alias = "Audio";
      setState(() {
        _currentIndexSlider = 1;
        _currentIndexPlaying = 1;
      });
    } else {
      _listSongVersion[0].alias = "Audio 1";
    }

    currentMusic.songVersions.forEachIndex((element, position) {
      position++;
      var song = SongModel.fromJson(element.song.toJson(),
          otherSongVersionName: element.versionName,
          otherVersionUrl: element.file);

      song.alias = "Audio ${position + 1}";
      _listSongVersion.add(song);
    });

    if (_listSongVersion.length == 2) {
      _listSongVersion.add(SongModel(title: ""));
    }
  }

  var id_favorit;

  void playAudio(String url) async {
    final database = Provider.of<AppDatabase>(context, listen: false);
    final int songId = currentMusic.id;
    final downloadedMusic = await database.getDownloadedSongById(songId).first;
    final fileInfo = await cacheManager.getFileFromCache(url);
    if (DownloadedSongData == null) {
      if (fileInfo != null) {
        print('FROM CACHE');
        advancedPlayer.play(fileInfo.file.path, isLocal: true);
      } else {
        print('FROM URL');
        advancedPlayer.play(url);
        cacheManager.downloadFile(url);
      }
    } else {
      if (_listSongVersion.isEmpty) {
        print('FROM DB');
        advancedPlayer.play(downloadedMusic.localFilePath, isLocal: true);
      } else {
        if (fileInfo != null) {
          print('FROM CACHE');
          advancedPlayer.play(fileInfo.file.path, isLocal: true);
        } else {
          print('FROM URL');
          advancedPlayer.play(url);
          cacheManager.downloadFile(url);
        }
      }
    }

    _audioControlCubit.playAudio(
        index,
        widget.listMusic.map((e) => e.toJson()).toList(),
        AudioPlayerDetailType.HOME);
  }

  StreamBuilder<List<Favorit>> checkerFavorit(BuildContext context, id, index) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchWhereFavorits(id),
      builder: (context, AsyncSnapshot<List<Favorit>> snapshot) {
        final songs = snapshot.data ?? List();
        // print(songs[index].id);
        if (songs.toString() == '[]' || songs == null) {
          onOrOff = false;
          return Icon(Icons.favorite_border);
        } else {
          id_favorit = songs[0].id == null ? null : songs[0].id;

          onOrOff = true;
          return Icon(Icons.favorite);
        }
      },
    );
  }

  var playlist_id = 1;

  StreamBuilder<List<JudulPlaylist>> playlist(BuildContext context) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchAllJudulPlaylists(),
      builder: (context, AsyncSnapshot<List<JudulPlaylist>> snapshot) {
        final judul_playlist = snapshot.data ?? List();
        return StatefulBuilder(
          builder: (_, setState) {
            return ListView.builder(
                itemCount: judul_playlist.length,
                itemBuilder: (context, index) {
                  return RadioListTile(
                    value: judul_playlist[index].id,
                    groupValue: playlist_id,
                    onChanged: (value) {
                      setState(() {
                        playlist_id = value;
                      });
                    },
                    title: Text(judul_playlist[index].judul_playlist),
                  );
                });
          },
        );
      },
    );
  }

  var counterPlay = 0;

  void cekDurasi() async {
    setState(() {
      advancedPlayer.onPlayerCompletion.listen((event) {
        if (repeat_status == "repeat_all") {
          playAudio(currentMusic.music);
        } else if (repeat_status == "repeat_one") {
          counterPlay = counterPlay + 1;
          if (counterPlay == 1) {
            next();
          } else {
            playAudio(currentMusic.music);
          }
        } else if (repeat_status == "shuffle") {
          Random random = new Random();
          int randomNumber = random.nextInt(widget.listMusic.length);

          setState(() {
            index = randomNumber;
          });

          advancedPlayer.stop();
          playAudio(currentMusic.music);
        } else {
          next();
        }
      });
      ambil_durasi = _duration;
      ambil_posisi = _position;
      print(ambil_posisi.toString() + ' PER ' + ambil_durasi.toString());
    });
    Future.delayed(Duration(seconds: 1), () {
      if (mounted) {
        cekDurasi();
      }
    });
  }

  void initPlayer() {
    advancedPlayer = LagusionAudioPlayer.instance;
    audioCache = AudioCache(fixedPlayer: advancedPlayer);

    advancedPlayer.durationHandler = (d) => {
          if (mounted)
            {
              setState(() {
                _duration = d;
              })
            }
        };

    advancedPlayer.positionHandler = (p) => {
          if (mounted)
            {
              setState(() {
                _position = p;
              })
            }
        };
  }

  var jenis_font = 'Roboto';

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
      if (prefs.getString('jenis_font') == null) {
        jenis_font = 'Roboto';
      } else {
        jenis_font = prefs.getString('jenis_font');
      }

      if (jenis_font == 'Roboto') {
        styleFontLirik = GoogleFonts.roboto(
          textStyle: TextStyle(
            color: Theme.of(context).brightness.toString() == 'Brightness.dark'
                ? Colors.white
                : Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 22 + (5 * value),
          ),
        );
      } else if (jenis_font == 'Average') {
        styleFontLirik = GoogleFonts.average(
          textStyle: TextStyle(
            color: Theme.of(context).brightness.toString() == 'Brightness.dark'
                ? Colors.white
                : Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 22 + (5 * value),
          ),
        );
      } else if (jenis_font == 'Aleo') {
        styleFontLirik = GoogleFonts.aleo(
          textStyle: TextStyle(
            color: Theme.of(context).brightness.toString() == 'Brightness.dark'
                ? Colors.white
                : Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 22 + (5 * value),
          ),
        );
      } else if (jenis_font == 'Delius') {
        styleFontLirik = GoogleFonts.delius(
          textStyle: TextStyle(
            color: Theme.of(context).brightness.toString() == 'Brightness.dark'
                ? Colors.white
                : Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 22 + (5 * value),
          ),
        );
      } else if (jenis_font == 'Vollkorn') {
        styleFontLirik = GoogleFonts.vollkorn(
          textStyle: TextStyle(
            color: Theme.of(context).brightness.toString() == 'Brightness.dark'
                ? Colors.white
                : Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 22 + (5 * value),
          ),
        );
      }
    });
  }

  var hideplayer = false;

  tampil_text(var_text, ukuranfont, fontstyle, bahasa) {
    var styleFont;
    if (fontstyle == 'Roboto') {
      styleFont = GoogleFonts.roboto(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    } else if (fontstyle == 'Average') {
      styleFont = GoogleFonts.average(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    } else if (fontstyle == 'Aleo') {
      styleFont = GoogleFonts.aleo(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    } else if (fontstyle == 'Delius') {
      styleFont = GoogleFonts.delius(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    } else if (fontstyle == 'Vollkorn') {
      styleFont = GoogleFonts.vollkorn(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    }
    return Text(
      bahasa == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: styleFont,
    );
  }

  cek_music_play() async {
    var id_musik;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    id_musik = prefs.getInt('music_play');
    setState(() {
      if (currentMusic.id == id_musik) {
        playAudio(currentMusic.music);
      } else {
        advancedPlayer.stop();
        playAudio(currentMusic.music);
      }
    });
    set_music_play(currentMusic.id);
  }

  set_music_play(id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setInt('music_play', id);
    });
  }

  ambilPosisi() async {
    setState(() {
      posisi = _position;
    });

    return posisi;
  }

  bool hideTopRightMoreMenu = true;

  next() async {
    setState(() {
      if (widget.listMusic.length - 1 == index) {
        index = 0;
      } else {
        index = index + 1;
      }
    });

    advancedPlayer.stop();
    playAudio(currentMusic.music);
  }

  TextEditingController namaPlaylist = new TextEditingController();

  prev() async {
    setState(() {
      if (index == 0) {
        index = widget.listMusic.length - 1;
      } else {
        index = index - 1;
      }
    });
    // print(widget.index);
    advancedPlayer.stop();
    playAudio(currentMusic.music);
  }

  repeat() async {
    // setState(() {
    //   if (repeat_status == "on") {
    //     repeat_status = "off";
    //   } else {
    //     repeat_status = "on";
    //   }
    // });
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Song Repeat'),
          children: <Widget>[
            RadioListTile(
              value: 'Repeat All',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.repeat),
                  SizedBox(
                    width: 5,
                  ),
                  Text('Repeat All'),
                ],
              ),
            ),
            RadioListTile(
              value: 'Shuffle',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.shuffle),
                  SizedBox(
                    width: 5,
                  ),
                  Text('Shuffle'),
                ],
              ),
            ),
            RadioListTile(
              value: 'Repeat One',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.repeat_one),
                  SizedBox(
                    width: 5,
                  ),
                  Text('Repeat One'),
                ],
              ),
            ),
            RadioListTile(
              value: 'None',
              groupValue: repeat_status,
              onChanged: (value) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = value;
                  prefs.setString('pengulangan_lagu', repeat_status);
                  prefs.commit();
                  Navigator.pop(context);
                });
              },
              title: Row(
                children: <Widget>[
                  Icon(Icons.link_off),
                  SizedBox(
                    width: 5,
                  ),
                  Text('None'),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);
    advancedPlayer.seek(newDuration);
  }

  var noteBalok = false;

  Duration toDuration(String isoString) {
    final minutes = int.parse(isoString.substring(0, 2));
    final seconds = int.parse(isoString.substring(3, 5));

    return Duration(
      minutes: minutes,
      seconds: seconds,
    );
  }

  Widget songVerse() {
    return currentMusic.verses.isEmpty
        ? Text(defaultLyrics.toString(),
            textAlign: TextAlign.center, style: styleFontLirik)
        : ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: currentMusic.verses.length,
            itemBuilder: (context, i) {
              var durasi_bait = currentMusic.verses[i].duration;
              var durasi_bait_sebelum =
                  i == 0 ? "00:00" : currentMusic.verses[i - 1].duration;
              var durasi_bait_sesudah = i == (currentMusic.verses.length - 1)
                  ? currentMusic.duration
                  : currentMusic.verses[i + 1].duration;
              return GestureDetector(
                onTap: () {
                  var durasi_bait_ =
                      toDuration(durasi_bait).inSeconds.toDouble();
                  setState(() {
                    seekToSecond(durasi_bait_.toInt());
                    newvalue = durasi_bait_;
                  });
                },
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: Column(
                    children: <Widget>[
                      currentMusic.verses[i].part == 'REFF' ||
                              currentMusic.verses[i].part == 'Ref'
                          ? Text(
                              "Ref:",
                              style: TextStyle(
                                color: Colors.deepOrange,
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              ),
                            )
                          : Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _position.inSeconds <
                                        toDuration(durasi_bait_sebelum)
                                            .inSeconds
                                    ? null
                                    : _position.inSeconds <
                                            toDuration(durasi_bait).inSeconds
                                        ? null
                                        : _position.inSeconds <
                                                toDuration(durasi_bait_sesudah)
                                                    .inSeconds
                                            ? Colors.deepOrange
                                            : null,
                              ),
                              child: Center(
                                child: Text(
                                  (i + 1).toString(),
                                  style: TextStyle(
                                      color: _position.inSeconds <
                                              toDuration(durasi_bait_sebelum)
                                                  .inSeconds
                                          ? null
                                          : _position.inSeconds <
                                                  toDuration(durasi_bait)
                                                      .inSeconds
                                              ? null
                                              : _position.inSeconds <
                                                      toDuration(
                                                              durasi_bait_sesudah)
                                                          .inSeconds
                                                  ? Colors.white
                                                  : null,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                      Text(currentMusic.verses[i].lyrics,
                          textAlign: TextAlign.center, style: styleFontLirik),
                    ],
                  ),
                ),
              );
            },
          );
    // }
  }

  var tipe_song;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: GestureDetector(
          onTap: () {
            setState(() {
              hideTopRightMoreMenu = true;
              show_more = false;
            });
          },
          child: Container(
            transform: Matrix4.translationValues(-20.0, 0.0, 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 35.0,
                      width: 35.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          currentMusic.thumbnail,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(left: 5),
                  width: MediaQuery.of(context).size.width - 275,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        currentMusic.title,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 16.5 + (5 * value),
                            fontFamily: jenis_font),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        currentMusic.artist + " " + "C 4/4",
                        style: TextStyle(fontSize: 13 + (5 * value)),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              final database = Provider.of<AppDatabase>(context, listen: false);
              final favorit = Favorit(
                music_id: currentMusic.id,
                created_at: DateTime.now(),
              );
              if (onOrOff == false) {
                database.insertFavorit(favorit);
              } else {
                database.deleteFavorit(favorit);
              }
            },
            icon: checkerFavorit(context, currentMusic.id, index),
          ),
          Builder(
            builder: (context) {
              if (currentMusic.correlations.isEmpty) {
                return Container();
              }
              return IconButton(
                onPressed: () {
                  showDialog<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return StatefulBuilder(
                        builder: (context, setState) {
                          return SimpleDialog(
                            children: <Widget>[
                              Container(
                                width: double.maxFinite,
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: currentMusic.correlations.length,
                                    itemBuilder: (context, position) {
                                      final currCorrelation =
                                          currentMusic.correlations[position];
                                      if (currCorrelation.correlation == 0) {
                                        return Container();
                                      }
                                      return RadioListTile(
                                        value: currCorrelation.albumName,
                                        groupValue: tipe_song,
                                        onChanged: (value) async {
                                          setState(() {
                                            tipe_song = value;
                                          });
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailMusicApp(
                                                        indexCurrentPlay: index,
                                                        listMusic:
                                                            widget.listMusic)),
                                          );
                                        },
                                        title: Text(currCorrelation.albumName),
                                      );
                                    }),
                              ),
                            ],
                          );
                        },
                      );
                    },
                  );
                },
                icon: Icon(Icons.translate),
              );
            },
          ),
          IconButton(
            onPressed: () {
              hideTopRightMoreMenu = !hideTopRightMoreMenu;

              show_more = false;
            },
            icon: Icon(Icons.more_vert),
          ),
        ],
      ),
      body: GestureDetector(
        onTap: () {
          setState(() {
            hideTopRightMoreMenu = true;
            show_more = false;
          });
        },
        child: WillPopScope(
          onWillPop: () async {
            final database = Provider.of<AppDatabase>(context, listen: false);
            final recentSong = RecentSong(
              music_id: currentMusic.id,
              created_at: DateTime.now(),
            );

            final song = Song(
              musicVersion: 1,
              idMusic: currentMusic.id,
              data: json.encode(currentMusic.toJson()),
              created_at: DateTime.now(),
            );
            database.insertSong(song);
            database.insertRecentSong(recentSong);

            return true;
          },
          child: Stack(
            children: <Widget>[
              musicPlayer(),
              repeatButton(),
              toggleShowHidePlayer(),
              moreButton(),
              moreButtonView(),
              topRightMenuView(),
            ],
          ),
        ),
      ),
    );
  }

  bool downloading = false;
  var progressString = "";
  var indexDownload;
  var _indexDownload;

  Widget playerIndicator() => Container(
        transform: Matrix4.translationValues(0, -30, 0),
        padding: EdgeInsets.only(left: 8, right: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              children: <Widget>[
                previousButton(),
                Builder(
                  builder: (context) {
                    if (_listSongVersion.length > 3) {
                      if (_currentIndexSlider > 1) {
                        return IconButton(
                          onPressed: () => _coruselController.previousPage(),
                          icon: Icon(Icons.arrow_back_ios),
                        );
                      }
                    }
                    return SizedBox(width: 32);
                  },
                ),
                playButtonContainer(),
                Builder(
                  builder: (context) {
                    if (_listSongVersion.length > 3) {
                      if (_currentIndexSlider < _listSongVersion.length - 2) {
                        return IconButton(
                          onPressed: () => _coruselController.nextPage(),
                          icon: Icon(Icons.arrow_forward_ios),
                        );
                      }
                    }

                    return SizedBox(width: 32);
                  },
                ),
                nextButton(),
              ],
            ),
          ],
        ),
      );

  Widget previousButton() => GestureDetector(
        onTap: () {
          prev();
          final database = Provider.of<AppDatabase>(context);
          final recentSong = RecentSong(
            music_id: currentMusic.id,
            created_at: DateTime.now(),
          );
          database.insertRecentSong(recentSong);
          setState(() {
            newvalue = 0.0;
            notifyCurrentPlayingVersion();
          });
        },
        child: Icon(
          Icons.skip_previous,
          color: Theme.of(context).brightness.toString() == 'Brightness.dark'
              ? Colors.white
              : Colors.black54,
          size: 42.0,
        ),
      );

  Widget nextButton() => GestureDetector(
        onTap: () {
          next();
          final database = Provider.of<AppDatabase>(context, listen: false);
          final recentSong = RecentSong(
            music_id: currentMusic.id,
            created_at: DateTime.now(),
          );

          database.insertRecentSong(recentSong);
          setState(() {
            newvalue = 0.0;
            notifyCurrentPlayingVersion();
          });
        },
        child: Icon(
          Icons.skip_next,
          color: Theme.of(context).brightness.toString() == 'Brightness.dark'
              ? Colors.white
              : Colors.black54,
          size: 42.0,
        ),
      );

  Widget playButtonContainer() => Container(
        transform: Matrix4.translationValues(0, 5, 0),
        width: MediaQuery.of(context).size.width * 0.45,
        child: CarouselSlider(
          options: CarouselOptions(
            scrollPhysics: _listSongVersion.length < 4
                ? NeverScrollableScrollPhysics()
                : BouncingScrollPhysics(),
            initialPage: 1,
            enlargeCenterPage: true,
            enableInfiniteScroll: false,
            onPageChanged: (index, reason) {
              if (index == 0) {
                _coruselController.nextPage();
              }

              if (index == _listSongVersion.length - 1) {
                _coruselController.previousPage();
              }

              setState(() {
                _currentIndexSlider = index;
              });
            },
            viewportFraction: 0.34,
            height: 70.0,
          ),
          carouselController: _coruselController,
          items: _listSongVersion.mapIndex((item, index) {
            return playButton(item, index);
          }).toList(),
        ),
      );

  Widget playButton(SongModel item, int position) => Column(
        children: <Widget>[
          Container(
            height: _currentIndexSlider == position ? 50 : 40,
            width: _currentIndexSlider == position ? 50 : 40,
            decoration: BoxDecoration(
              color: item.id == null ? Colors.grey : Colors.deepOrange,
              shape: BoxShape.circle,
            ),
            child: Center(
              child: IconButton(
                icon: Builder(
                  builder: (context) {
                    if (LagusionAudioPlayer.instance.state ==
                        AudioPlayerState.PLAYING) {
                      if (_currentIndexPlaying == position) {
                        return Icon(
                          Icons.pause,
                          size: 22.0,
                          color: Colors.white,
                        );
                      }
                    }

                    return Icon(
                      Icons.play_arrow,
                      size: 22.0,
                      color: Colors.white,
                    );
                  },
                ),
                onPressed: () async {
                  if (item.id != null) {
                    final state = LagusionAudioPlayer.instance.state;
                    if (state == AudioPlayerState.PLAYING) {
                      if (_currentIndexPlaying == position) {
                        advancedPlayer.pause();
                      } else {
                        setState(() {
                          _currentIndexPlaying = position;
                        });
                        advancedPlayer.stop();
                        playAudio(item.music);
                      }
                    } else if (state == AudioPlayerState.PAUSED) {
                      if (_currentIndexPlaying == position) {
                        advancedPlayer.resume();
                      } else {
                        setState(() {
                          _currentIndexPlaying = position;
                        });
                        advancedPlayer.stop();
                        playAudio(item.music);
                      }
                    } else {
                      setState(() {
                        _currentIndexPlaying = position;
                      });
                      advancedPlayer.stop();
                      playAudio(item.music);
                    }
                  } else {
                    return null;
                  }
                },
              ),
            ),
          ),
          Container(
            width: 60,
            child: Text(item.id != null ? item.alias : "",
                textAlign: TextAlign.center,
                maxLines: 1,
                overflow: TextOverflow.ellipsis),
          )
        ],
      );

  Widget playerContainer() => Builder(
        builder: (context) {
          if (hideplayer) {
            return Container();
          }
          return Column(
            children: [
              Container(
                transform: Matrix4.translationValues(0, -23, 0),
                width: MediaQuery.of(context).size.width,
                child: Slider(
                  value: _position.inSeconds.toDouble(),
                  // value: newvalue,
                  min: 0.0,
                  max: _duration.inSeconds.toDouble() + 1,
                  onChanged: (double value) {
                    setState(() {
                      seekToSecond(value.toInt());
                      newvalue = value;
                    });
                  },
                  activeColor: Colors.amber[900],
                ),
              ),
              Container(
                transform: Matrix4.translationValues(0, -20, 0),
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      ambil_posisi.toString().substring(2, 7),
                      style: TextStyle(
                          color: Theme.of(context).brightness.toString() ==
                                  'Brightness.dark'
                              ? Colors.white
                              : Colors.black.withOpacity(0.7),
                          fontSize: 15 + (5 * value)),
                    ),
                    Text(currentMusic.duration.toString(),
                        style: TextStyle(
                            color: Theme.of(context).brightness.toString() ==
                                    'Brightness.dark'
                                ? Colors.white
                                : Colors.black.withOpacity(0.7),
                            fontSize: 15 + (5 * value)))
                  ],
                ),
              ),
              playerIndicator(),
            ],
          );
        },
      );

  Widget musicPlayer() => Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: noteBalok == false
                  ? Stack(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(currentMusic.thumbnail),
                                  fit: BoxFit.cover)),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                                  Theme.of(context).brightness.toString() !=
                                          'Brightness.dark'
                                      ? Colors.white.withOpacity(0.5)
                                      : Colors.black.withOpacity(0.5),
                                  Theme.of(context).brightness.toString() !=
                                          'Brightness.dark'
                                      ? Colors.white.withOpacity(0.93)
                                      : Colors.black.withOpacity(0.93),
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: ListView(
                            children: <Widget>[
                              SizedBox(height: 52.0),
                              Container(
                                padding: EdgeInsets.only(left: 10, right: 10),
                                alignment: Alignment.topCenter,
                                child: Column(
                                  children: <Widget>[
                                    songVerse(),
                                  ],
                                ),
                                // Text(
                                //     lirik.replaceAll(
                                //         'REFF',
                                //         'REFF :\n' +
                                //             currentMusic["reff"]
                                //                 .toString()),
                                //     textAlign: TextAlign.center,
                                //     style: styleFontLirik),
                              ),
                              SizedBox(
                                height: 6.0,
                              ),
                              SizedBox(height: 16.0),
                            ],
                          ),
                        )
                      ],
                    )
                  : Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(currentMusic.musicalNotes),
                              fit: BoxFit.cover)),
                    ),
            ),
          ),
          playerContainer()
        ],
      );

  Widget repeatButton() => Positioned(
      left: 10,
      bottom: hideplayer == true ? 30 : 170,
      child: repeat_status == 'none'
          ? GestureDetector(
              onTap: () async {
                final SharedPreferences prefs =
                    await SharedPreferences.getInstance();
                setState(() {
                  repeat_status = 'repeat_all';
                  prefs.setString('pengulangan_lagu', repeat_status);
                });
              },
              child: Icon(
                Icons.repeat,
                color:
                    Theme.of(context).brightness.toString() == 'Brightness.dark'
                        ? Colors.white
                        : Colors.black54,
                size: 32.0,
              ),
            )
          : repeat_status == 'repeat_all'
              ? GestureDetector(
                  onTap: () async {
                    final SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    setState(() {
                      repeat_status = 'repeat_one';
                      prefs.setString('pengulangan_lagu', repeat_status);
                      prefs.commit();
                    });
                  },
                  child: Icon(
                    Icons.repeat,
                    color: Colors.deepOrange,
                    size: 32.0,
                  ),
                )
              : repeat_status == 'repeat_one'
                  ? GestureDetector(
                      onTap: () async {
                        final SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        setState(() {
                          repeat_status = 'shuffle';
                          prefs.setString('pengulangan_lagu', repeat_status);
                          prefs.commit();
                        });
                      },
                      child: Icon(
                        Icons.repeat_one,
                        color: Colors.deepOrange,
                        size: 32.0,
                      ),
                    )
                  : repeat_status == 'shuffle'
                      ? GestureDetector(
                          onTap: () async {
                            final SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              repeat_status = 'none';
                              prefs.setString(
                                  'pengulangan_lagu', repeat_status);
                              prefs.commit();
                            });
                          },
                          child: Icon(
                            Icons.shuffle,
                            color: Colors.deepOrange,
                            size: 32.0,
                          ),
                        )
                      : Container());

  Widget toggleShowHidePlayer() => Positioned(
        bottom: 0,
        left: 0,
        right: 0,
        child: GestureDetector(
          onTap: () {
            setState(() {
              hideplayer = !hideplayer;
            });
          },
          child: Center(
            child: Container(
              transform: Matrix4.translationValues(0, 10, 0),
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Theme.of(context).brightness.toString() ==
                              'Brightness.dark'
                          ? Colors.black
                          : Colors.grey.withOpacity(0.9),
                      blurRadius:
                          10.0, // has the effect of softening the shadow
                      spreadRadius:
                          0.0, // has the effect of extending the shadow
                      offset: Offset(
                        0.0, // horizontal, move right 10
                        0.0, // vertical, move down 10
                      ),
                    )
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: hideplayer == true
                  ? Icon(Icons.keyboard_arrow_up)
                  : Icon(Icons.keyboard_arrow_down),
            ),
          ),
        ),
      );

  Widget moreButton() => Positioned(
        right: 10,
        bottom: hideplayer == true ? 30 : 170,
        child: GestureDetector(
          onTap: () {
            show_more = true;
            setState(() {
              hideTopRightMoreMenu = true;
            });
          },
          child: Icon(
            Icons.more_horiz,
            color: Theme.of(context).brightness.toString() == 'Brightness.dark'
                ? Colors.white
                : Colors.black54,
            size: 32.0,
          ),
        ),
      );

  Widget moreButtonView() => show_more == false
      ? Container()
      : Positioned(
          right: 10,
          bottom: hideplayer == true ? 30 : 120,
          child: Container(
            height: 240,
            width: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.deepOrange,
            ),
            child: Column(
              children: <Widget>[
                SizedBox(height: 15),
                GestureDetector(
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text("Cerita"),
                          content: new Text(currentMusic.story == null
                              ? ''
                              : currentMusic.story),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text("Close"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                  child: Icon(
                    Icons.info,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
                SizedBox(height: 15),
                GestureDetector(
                  onTap: () {
                    final database =
                        Provider.of<AppDatabase>(context, listen: false);
                    showDialog<void>(
                      context: context,
                      builder: (BuildContext context) {
                        return StatefulBuilder(
                          builder: (_, setState) {
                            return AlertDialog(
                              title: const Text('Simpan ke Playlist'),
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                      height: 200,
                                      width: double.maxFinite,
                                      child: playlist(_)),
                                ],
                              ),
                              actions: <Widget>[
                                Row(
                                  children: <Widget>[
                                    new FlatButton(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Icon(Icons.add),
                                          new Text("Tambah Kategori"),
                                        ],
                                      ),
                                      onPressed: () {
                                        showDialog(
                                            context: context,
                                            builder: (context) {
                                              return AlertDialog(
                                                title: Text('Tambah Playlist'),
                                                content: TextField(
                                                  controller: namaPlaylist,
                                                  decoration: InputDecoration(
                                                      hintText:
                                                          "Masukkan nama playlist"),
                                                ),
                                                actions: <Widget>[
                                                  new FlatButton(
                                                    child: new Text('BATAL',
                                                        style: TextStyle(
                                                            color:
                                                                Colors.orange)),
                                                    onPressed: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                  ),
                                                  new FlatButton(
                                                    child: new Text('TAMBAHKAN',
                                                        style: TextStyle(
                                                            color:
                                                                Colors.orange)),
                                                    onPressed: () async {
                                                      // final SharedPreferences prefs =
                                                      //     await SharedPreferences.getInstance();
                                                      // prefs.setString('alamat', namaPlaylist.text);
                                                      final database = Provider
                                                          .of<AppDatabase>(
                                                              context);
                                                      final judulPlaylist =
                                                          JudulPlaylist(
                                                        judul_playlist:
                                                            namaPlaylist.text,
                                                        created_at:
                                                            DateTime.now(),
                                                      );

                                                      database
                                                          .insertJudulPlaylist(
                                                              judulPlaylist);
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                  ),
                                                ],
                                              );
                                            });
                                      },
                                    ),
                                    new FlatButton(
                                      child: new Text("OK"),
                                      onPressed: () {
                                        final song = Song(
                                          idMusic: currentMusic.id,
                                          data: json
                                              .encode(currentMusic.toJson()),
                                          musicVersion: 1,
                                          created_at: DateTime.now(),
                                        );

                                        database.insertSong(song);

                                        final playlist_insert = Playlist(
                                          music_id: currentMusic.id,
                                          playlist_id: playlist_id,
                                          created_at: DateTime.now(),
                                        );

                                        database
                                            .insertPlaylist(playlist_insert);
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            );
                          },
                        );
                      },
                    );
                  },
                  child: Icon(
                    Icons.library_add,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
                SizedBox(height: 15),
                GestureDetector(
                  onTap: () {
                    noteBalok = !noteBalok;
                  },
                  child: Icon(
                    Icons.queue_music,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
                SizedBox(height: 15),
                GestureDetector(
                  onTap: () {
                    Share.share('Dengarkan lagu ' +
                        currentMusic.title +
                        ' di aplikasi Lagu Sion Plus');
                  },
                  child: Icon(
                    Icons.share,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        show_more = false;
                      });
                    },
                    child: Hero(
                      tag: "tapShow",
                      child: Icon(
                        Icons.close,
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );

  Widget topRightMenuView() => Builder(
        builder: (context) {
          if (hideTopRightMoreMenu) {
            return Container();
          }
          return Positioned(
            top: 0,
            right: 10,
            child: Container(
              padding: EdgeInsets.only(top: 20),
              width: 200,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Theme.of(context).brightness.toString() ==
                            'Brightness.dark'
                        ? Colors.black
                        : Colors.grey.withOpacity(0.9),
                    blurRadius: 10.0, // has the effect of softening the shadow
                    spreadRadius: 0.0, // has the effect of extending the shadow
                    offset: Offset(
                      0.0, // horizontal, move right 10
                      0.0, // vertical, move down 10
                    ),
                  )
                ],
                color:
                    Theme.of(context).brightness.toString() == 'Brightness.dark'
                        ? Color(0xff4d4d4d)
                        : Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child:
                                tampil_text('tema', 15, jenis_font, language)),
                        RadioListTile<Brightness>(
                          value: Brightness.light,
                          groupValue: Theme.of(context).brightness,
                          onChanged: (Brightness value) {
                            DynamicTheme.of(context)
                                .setBrightness(Brightness.light);
                          },
                          title: const Text('Default'),
                        ),
                        RadioListTile<Brightness>(
                          value: Brightness.dark,
                          groupValue: Theme.of(context).brightness,
                          onChanged: (Brightness value) {
                            DynamicTheme.of(context)
                                .setBrightness(Brightness.dark);
                          },
                          title: const Text('Gelap'),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  ListTile(
                    title: tampil_text('ukuran_text', 15, jenis_font, language),
                    subtitle: Slider(
                      onChanged: (double value_) async {
                        setState(() {
                          ukuranText();
                          value = value_;
                        });
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        prefs.setDouble('ukuran_text', value_);
                      },
                      value: value,
                      activeColor: Colors.amber[900],
                    ),
                  ),
                  Divider(),
                  topRightMenuDownloadView(),
                  Divider(),
                  GestureDetector(
                    onTap: () {
                      hideTopRightMoreMenu = !hideTopRightMoreMenu;
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FeedBackPage()));
                    },
                    child: Container(
                        alignment: Alignment.centerLeft,
                        margin:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child:
                            tampil_text('feedback', 15, jenis_font, language)),
                  ),
                  Divider(),
                ],
              ),
            ),
          );
        },
      );

  Widget topRightMenuDownloadView() {
    final database = Provider.of<AppDatabase>(context, listen: false);
    final int songId = currentMusic.id;
    return StreamBuilder<DownloadedSongData>(
      stream: database.getDownloadedSongById(songId),
      builder: (context, snapshot) {
        if (snapshot.data != null) {
          return Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Align(
                child: tampil_text('tersimpan', 15, jenis_font, language),
                alignment: Alignment.centerLeft,
              ));
        }

        return GestureDetector(
          onTap: () async {
            final song = Song(
              musicVersion: 1,
              idMusic: currentMusic.id,
              data: json.encode(currentMusic.toJson()),
              created_at: DateTime.now(),
            );
            await database.insertSong(song);
            FileInfo fileInfo =
                await cacheManager.getFileFromCache(currentMusic.music);
            String pathFile;
            if (fileInfo == null) {
              fileInfo = await cacheManager
                  .getFileStream(currentMusic.music, withProgress: true)
                  .listen((event) {
                if (event is DownloadProgress) {
                  setState(() {
                    indexDownload = index;
                    downloading = true;
                    progressString = "${event.progress}/ ${event.totalSize}";
                  });
                }
              }).asFuture();
            }

            var dir = await getApplicationDocumentsDirectory();
            File newMusicFile =
                await fileInfo.file.copy("${dir.path}/${fileInfo.file.name}");
            pathFile = newMusicFile.path;

            final downloadedSongData = DownloadedSongData(
                songId: currentMusic.id,
                localFilePath: pathFile,
                createdAt: DateTime.now());
            await database.insertDownloadedSong(downloadedSongData);
          },
          child: Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: indexDownload == index
                  ? downloading == true
                      ? Text(
                          'Downloading : ' + progressString,
                        )
                      : tampil_text('unduh', 15, jenis_font, language)
                  : tampil_text('unduh', 15, jenis_font, language)),
        );
      },
    );
  }
}

class RadioActive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xfff48520), width: 2),
        borderRadius: BorderRadius.circular(50),
      ),
      height: 20,
      width: 20,
      child: Container(
        height: 10,
        width: 10,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.white, width: 2),
            borderRadius: BorderRadius.circular(50),
            color: Color(0xfff48520)),
      ),
    );
  }
}

class RadioInactive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xfff48520), width: 2),
        borderRadius: BorderRadius.circular(50),
      ),
      height: 20,
      width: 20,
      child: Container(),
    );
  }
}
