import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:provider/provider.dart';

class SearchNumpad extends StatefulWidget {
  final String search;
  SearchNumpad({Key key, this.search = ''});
  @override
  _SearchNumpadState createState() => _SearchNumpadState();
}

class _SearchNumpadState extends State<SearchNumpad> {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';
  var flume =
      'https://i.scdn.co/image/8d84f7b313ca9bafcefcf37d4e59a8265c7d3fff';

  List<dynamic> laguSion;
  List _searchResult = [];

  Future getDataLagu() async {
    http.Response response = await http.get(apiMaster + '/song', headers: {
      'Accept': 'application/json',
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> laguDataMentah = json.decode(response.body);
      laguSion = laguDataMentah["songs"];
      laguSion.forEach((data) {
        if (data['title'].contains(widget.search)) _searchResult.add(data);
      });
      print(laguSion);
    });
  }

  void initState() {
    super.initState();
    getDataLagu();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Search',
          overflow: TextOverflow.ellipsis,
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 12, top: 10),
            child: Icon(Icons.search),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10, right: 0, top: 10),
        child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _searchResult == null ? 10 : _searchResult.length,
            itemBuilder: (context, int index) {
              return Container(
                padding: EdgeInsets.only(right: 10),
                child: _searchResult == null
                    ? SongSkeleton()
                    : Column(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              // Provider.of<HomeModel>(context).stop();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                  DetailMusicApp(
                                  indexCurrentPlay: index,
                                  listMusic: _searchResult
                                      .map((e) => SongModel.fromJson(e))
                                      .toList(),
                                )
                                ),
                              );
                            },
                            child: Container(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                        color: Colors.grey.withOpacity(0.4),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: Image.network(
                                        _searchResult[index]['thumbnail'],
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Flexible(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              _searchResult[index]['title'],
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Theme.of(context)
                                                              .brightness
                                                              .toString() ==
                                                          'Brightness.dark'
                                                      ? Colors.white
                                                      : Colors.black,
                                                  fontSize: 16.5,
                                                  fontFamily: 'Roboto'),
                                            ),
                                            SizedBox(height: 5.0),
                                            Text(
                                              _searchResult[index]['artist'],
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                            .brightness
                                                            .toString() ==
                                                        'Brightness.dark'
                                                    ? Colors.white
                                                    : Colors.black
                                                        .withOpacity(0.5),
                                                fontSize: 16.5,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  'C 4/4',
                                                  style:
                                                      TextStyle(fontSize: 10),
                                                ),
                                                Container(
                                                  transform:
                                                      Matrix4.translationValues(
                                                          -8, 0, 0),
                                                  child: Icon(
                                                    Icons.play_arrow,
                                                    color: Theme.of(context)
                                                                .brightness
                                                                .toString() ==
                                                            'Brightness.dark'
                                                        ? Colors.white
                                                        : Colors.black
                                                            .withOpacity(0.6),
                                                    size: 25.0,
                                                  ),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  transform:
                                                      Matrix4.translationValues(
                                                          8.0, 0.0, 0.0),
                                                  child:
                                                      PopupMenuButton<String>(
                                                    child: Icon(Icons.more_vert,
                                                        size: 18),
                                                    onSelected: choiceAction,
                                                    itemBuilder:
                                                        (BuildContext context) {
                                                      return MenuMore.choices
                                                          .map((String choice) {
                                                        return PopupMenuItem<
                                                            String>(
                                                          value: choice,
                                                          child: Text(choice),
                                                        );
                                                      }).toList();
                                                    },
                                                  ),
                                                ),
                                                Text(_searchResult[index]
                                                        ['duration']
                                                    .replaceAll(
                                                        new RegExp(
                                                            r"\s+\b|\b\s"),
                                                        ""))
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Divider(),
                        ],
                      ),
              );
            }),
      ),
    );
  }

  void choiceAction(String choice) {
    if (choice == MenuMore.Unduh) {
      print('Unduh');
    } else if (choice == MenuMore.LaguUtama) {
      print('Jadikan Lagu Utama');
    }
  }
}

class MenuMore {
  static const String Unduh = 'Unduh';
  static const String LaguUtama = 'Jadikan Lagu Utama';

  static const List<String> choices = <String>[
    Unduh,
    LaguUtama,
  ];
}

class SongSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 200,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 10,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              transform: Matrix4.translationValues(-8, 0, 0),
                              child: Icon(
                                Icons.play_arrow,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.6),
                                size: 25.0,
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(8.0, 0.0, 0.0),
                              child: PopupMenuButton<String>(
                                child: Icon(Icons.more_vert, size: 18),
                                itemBuilder: (BuildContext context) {
                                  return MenuMore.choices.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            Container(
                              height: 10,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
