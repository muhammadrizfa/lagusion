import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/detail/search.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:convert';

import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LaguSionBaru extends StatefulWidget {
  final judul_album;
  LaguSionBaru(this.judul_album);
  @override
  _LaguSionBaruState createState() => _LaguSionBaruState();
}

class _LaguSionBaruState extends State<LaguSionBaru> {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';
  var flume =
      'https://i.scdn.co/image/8d84f7b313ca9bafcefcf37d4e59a8265c7d3fff';

  var language;
  var jenis_font = 'Roboto';
  var value = 0.1;
  var lagu_utama = true;
  var _lagu_utama;

  cekLaguUtama() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lagu_utama = prefs.getString('lagu_utama') == null
        ? 'Album A'
        : prefs.getString('lagu_utama');
    if (_lagu_utama == widget.judul_album) {
      setState(() {
        lagu_utama = true;
      });
    } else {
      setState(() {
        lagu_utama = false;
      });
    }
  }

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
      if (prefs.getString('jenis_font') == null) {
        jenis_font = 'Roboto';
      } else {
        jenis_font = prefs.getString('jenis_font');
      }
    });
  }

  List<dynamic> laguSion;

  Future getDataLagu() async {
    http.Response response =
        await http.post(apiMaster + '/album/search', headers: {
      'Accept': 'application/json',
    }, body: {
      'album_name': widget.judul_album,
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> laguDataMentah = json.decode(response.body);
      laguSion = laguDataMentah["albums"][0]['songs'];
      print(laguSion);
    });
  }

  TextEditingController _textFieldController = TextEditingController();

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          var nilai = '';
          return StatefulBuilder(builder: (context, setState) {
            return Dialog(
              child: Container(
                margin: EdgeInsets.only(bottom: 20, top: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.centerRight,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                nilai,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Theme.of(context)
                                                .brightness
                                                .toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black),
                              ),
                              Divider()
                            ],
                          ),
                        ),
                        FlatButton(
                          child: Icon(
                            Icons.backspace,
                            color: Theme.of(context).brightness.toString() ==
                                    'Brightness.dark'
                                ? Colors.white
                                : Colors.black.withOpacity(0.5),
                          ),
                          onPressed: () {
                            setState(() {
                              if (nilai.length > 0) {
                                nilai = nilai.substring(0, nilai.length - 1);
                              }
                            });
                          },
                          onLongPress: () {
                            setState(() {
                              nilai = '';
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '1';
                                  });
                                },
                                child: Container(
                                  child: Text('1',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '2';
                                  });
                                },
                                child: Container(
                                  child: Text('2',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '3';
                                  });
                                },
                                child: Container(
                                  child: Text('3',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '4';
                                  });
                                },
                                child: Container(
                                  child: Text('4',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '5';
                                  });
                                },
                                child: Container(
                                  child: Text('5',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '6';
                                  });
                                },
                                child: Container(
                                  child: Text('6',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '7';
                                  });
                                },
                                child: Container(
                                  child: Text('7',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '8';
                                  });
                                },
                                child: Container(
                                  child: Text('8',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '9';
                                  });
                                },
                                child: Container(
                                  child: Text('9',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Stack(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SearchPage()));
                                    },
                                    child: Container(
                                      child: Text('ABC',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16)),
                                    ),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        nilai = nilai + '0';
                                      });
                                    },
                                    child: Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 30),
                                      child: Text('0',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 30)),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      // Navigator.push(
                                      //     context,
                                      //     MaterialPageRoute(
                                      //         builder: (context) =>
                                      //             SearchNumpad(
                                      //               search: nilai,
                                      //             )));
                                      Navigator.pop(context);
                                      laguSion.length < int.parse(nilai)
                                          ? showDialog(
                                              context: context,
                                              builder: (context) {
                                                return StatefulBuilder(builder:
                                                    (context, setState) {
                                                  return AlertDialog(
                                                    title: Text(
                                                        'Lagu tidak tersedia'),
                                                    content: Text(
                                                        'Lagu yang anda masukkan tidak tersedia'),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        onPressed: () =>
                                                            Navigator.pop(
                                                                context),
                                                        child: Text('OK'),
                                                      ),
                                                    ],
                                                  );
                                                });
                                              },
                                            )
                                          : Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailMusicApp(
                                                        indexCurrentPlay:
                                                            int.parse(nilai) -
                                                                1,
                                                        listMusic: laguSion
                                                            .map((e) =>
                                                                SongModel
                                                                    .fromJson(
                                                                        e))
                                                            .toList()
                                                      )),
                                            );
                                    },
                                    child: Container(
                                      child: Text('OK',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          });
        });
  }

  void initState() {
    super.initState();
    getDataLagu();
    ukuranText();
    cekLaguUtama();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(65.0), // here the desired height
        child: AppBar(
          leading: Padding(
              padding: EdgeInsets.only(top: 10),
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: BackButton())),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            transform: Matrix4.translationValues(-20.0, 0.0, 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 35.0,
                      width: 35.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          'https://a4.ugziki.co.ug/no-cover-art/nocover-800x800.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(left: 5),
                  width: MediaQuery.of(context).size.width - 180,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.judul_album.toString(),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchPage()));
              },
              icon: Icon(Icons.search),
            )
          ],
        ),
      ),
      body: Container(
        child: Column(
          children: [
            lagu_utama == true
                ? Container()
                : Container(
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Theme.of(context).brightness.toString() ==
                                  'Brightness.dark'
                              ? Colors.black
                              : Colors.grey.withOpacity(0.5),
                          blurRadius:
                              10.0, // has the effect of softening the shadow
                          spreadRadius:
                              0.0, // has the effect of extending the shadow
                          offset: Offset(
                            0.0, // horizontal, move right 10
                            0.0, // vertical, move down 10
                          ),
                        )
                      ],
                      color: Theme.of(context).brightness.toString() ==
                              'Brightness.dark'
                          ? Color(0xff4d4d4d)
                          : Colors.white,
                    ),
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 25),
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Text('Apakah Anda ingin menjadikan lagu utama?',
                              style: TextStyle(fontSize: 16)),
                          SizedBox(height: 20),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    lagu_utama = true;
                                  });
                                },
                                child: Text('Nanti',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Color(0xfff48520))),
                              ),
                              SizedBox(width: 20),
                              GestureDetector(
                                onTap: () async {
                                  SharedPreferences prefs =
                                      await SharedPreferences.getInstance();
                                  prefs.setString(
                                      'lagu_utama', widget.judul_album);
                                  prefs.commit();
                                  setState(() {
                                    lagu_utama = true;
                                  });
                                },
                                child: Text('Ya',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Color(0xfff48520))),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 10, top: 20),
                child: ListView.builder(
                    itemCount: laguSion == null ? 10 : laguSion.length,
                    itemBuilder: (context, int index) {
                      return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: laguSion == null
                              ? SongSkeleton()
                              : Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        // Provider.of<HomeModel>(context).stop();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailMusicApp(
                                                    indexCurrentPlay: index,
                                                    listMusic: laguSion
                                                        .map((e) =>
                                                            SongModel.fromJson(
                                                                e))
                                                        .toList()
                                                  )),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  laguSion[index]['thumbnail'],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Flexible(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Container(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            0.6,
                                                        child: Text(
                                                          (index + 1)
                                                                  .toString() +
                                                              '. ' +
                                                              laguSion[index]
                                                                  ['title'],
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          style: TextStyle(
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors
                                                                      .black,
                                                              fontSize: 16.5,
                                                              fontFamily:
                                                                  'Roboto'),
                                                        ),
                                                      ),
                                                      SizedBox(height: 5.0),
                                                      Text(
                                                        laguSion[index]
                                                            ['artist'],
                                                        style: TextStyle(
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.5),
                                                          fontSize: 16.5,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            'C 4/4',
                                                            style: TextStyle(
                                                                fontSize: 10),
                                                          ),
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    -8, 0, 0),
                                                            child: Icon(
                                                              Icons.play_arrow,
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors.black
                                                                      .withOpacity(
                                                                          0.6),
                                                              size: 25.0,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    8.0,
                                                                    0.0,
                                                                    0.0),
                                                            child:
                                                                PopupMenuButton<
                                                                    String>(
                                                              child: Icon(
                                                                  Icons
                                                                      .more_vert,
                                                                  size: 18),
                                                              onSelected:
                                                                  (choice) async {
                                                                if (choice ==
                                                                    MenuMore
                                                                        .Unduh) {
                                                                  var dir =
                                                                      await getExternalStorageDirectory();
                                                                  final database =
                                                                      Provider.of<
                                                                              AppDatabase>(
                                                                          context);
                                                                  final song =
                                                                      Song(
                                                                    musicVersion:
                                                                        1,
                                                                    idMusic: laguSion[
                                                                            index]
                                                                        ["id"],
                                                                    data: json.encode(
                                                                        laguSion[
                                                                            index]),
                                                                    created_at:
                                                                        DateTime
                                                                            .now(),
                                                                  );

                                                                  database
                                                                      .insertSong(
                                                                          song);
                                                                } else if (choice ==
                                                                    MenuMore
                                                                        .Share) {
                                                                  Share.share('Dengarkan lagu ' +
                                                                      laguSion[
                                                                              index]
                                                                          [
                                                                          "title"] +
                                                                      ' di aplikasi Lagu Sion Plus');
                                                                }
                                                              },
                                                              itemBuilder:
                                                                  (BuildContext
                                                                      context) {
                                                                return MenuMore
                                                                    .choices
                                                                    .map((String
                                                                        choice) {
                                                                  return PopupMenuItem<
                                                                      String>(
                                                                    value:
                                                                        choice,
                                                                    child: Text(
                                                                        choice),
                                                                  );
                                                                }).toList();
                                                              },
                                                            ),
                                                          ),
                                                          Text(laguSion[index]
                                                                  ['duration']
                                                              .replaceAll(
                                                                  new RegExp(
                                                                      r"\s+\b|\b\s"),
                                                                  ""))
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ));
                      // }),
                      // return Column(
                      //   children: <Widget>[
                      //     GestureDetector(
                      //       onTap: () {
                      //         // Provider.of<HomeModel>(context).stop();
                      //         Navigator.push(
                      //           context,
                      //           MaterialPageRoute(
                      //               builder: (context) => DetailMusicApp(
                      //                   laguSion[index]['title'],
                      //                   laguSion[index]['artist'],
                      //                   laguSion[index]['lyrics'],
                      //                   laguSion[index]['music'],
                      //                   laguSion[index]['duration'],
                      //                   laguSion[index]['thumbnail'])),
                      //         );
                      //       },
                      //       child: Container(
                      //         color: Colors.transparent,
                      //         child: Padding(
                      //           padding: const EdgeInsets.only(
                      //             left: 0.0,
                      //           ),
                      //           child: Row(
                      //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //             children: <Widget>[
                      //               Stack(
                      //                 children: <Widget>[
                      //                   Container(
                      //                     height: 40.0,
                      //                     width: 40.0,
                      //                     child: ClipRRect(
                      //                       borderRadius: BorderRadius.circular(8.0),
                      //                       child: Image.network(
                      //                         martinGarrix,
                      //                         fit: BoxFit.cover,
                      //                       ),
                      //                     ),
                      //                   ),
                      //                 ],
                      //               ),
                      //               Container(
                      //                 transform: Matrix4.translationValues(
                      //                     -MediaQuery.of(context).size.width * 4 / 100,
                      //                     0,
                      //                     0),
                      //                 padding: EdgeInsets.only(right: 0),
                      //                 width: 180,
                      //                 child: Column(
                      //                   crossAxisAlignment: CrossAxisAlignment.start,
                      //                   children: <Widget>[
                      //                     Text(
                      //                       laguSion[index]['title'],
                      //                       overflow: TextOverflow.ellipsis,
                      //                       style: TextStyle(
                      //                           color: Theme.of(context).brightness.toString() == 'Brightness.dark' ? Colors.white : Colors.black,
                      //                           fontSize: 16.5,
                      //                           fontFamily: 'Roboto'),
                      //                     ),
                      //                     SizedBox(height: 5.0),
                      //                     Text(
                      //                       laguSion[index]['artist'],
                      //                       style: TextStyle(
                      //                         color: Theme.of(context).brightness.toString() == 'Brightness.dark' ? Colors.white : Colors.black.withOpacity(0.5),
                      //                         fontSize: 16.5,
                      //                       ),
                      //                     ),
                      //                   ],
                      //                 ),
                      //               ),
                      //               Column(
                      //                 children: <Widget>[
                      //                   Row(
                      //                     mainAxisAlignment:
                      //                         MainAxisAlignment.spaceBetween,
                      //                     children: <Widget>[
                      //                       SizedBox(
                      //                         width: 20,
                      //                       ),
                      //                       Container(
                      //                         transform: Matrix4.translationValues(
                      //                             30.0, 0.0, 0.0),
                      //                         padding: EdgeInsets.only(left: 0, top: 5),
                      //                         child: Text(
                      //                           'C 4/4',
                      //                           style: TextStyle(fontSize: 10),
                      //                         ),
                      //                       ),
                      //                     ],
                      //                   ),
                      //                   GestureDetector(
                      //                     onTap: () {},
                      //                     child: Row(
                      //                       children: <Widget>[
                      //                         Container(
                      //                             padding: EdgeInsets.only(
                      //                                 left: MediaQuery.of(context)
                      //                                         .size
                      //                                         .width *
                      //                                     7 /
                      //                                     100),
                      //                             height: 25.0,
                      //                             width: 25.0,
                      //                             child: Icon(
                      //                               Icons.play_arrow,
                      //                               color:
                      //                                   Theme.of(context).brightness.toString() == 'Brightness.dark' ? Colors.white : Colors.black.withOpacity(0.6),
                      //                               size: 25.0,
                      //                             )),
                      //                       ],
                      //                     ),
                      //                   ),
                      //                 ],
                      //               ),
                      //               GestureDetector(
                      //                 onTap: () => print("Hello guys"),
                      //                 child: Padding(
                      //                     padding: EdgeInsets.only(right: 12),
                      //                     child: Column(
                      //                       children: <Widget>[
                      //                         Container(
                      //                           transform: Matrix4.translationValues(
                      //                               8.0, 0.0, 0.0),
                      //                           child: Icon(
                      //                             Icons.more_vert,
                      //                             color: Theme.of(context).brightness.toString() == 'Brightness.dark' ? Colors.white : Colors.black.withOpacity(0.6),
                      //                             size: 20.0,
                      //                           ),
                      //                         ),
                      //                         Text(laguSion[index]['duration']
                      //                             .replaceAll(
                      //                                 new RegExp(r"\s+\b|\b\s"), "")),
                      //                       ],
                      //                     )),
                      //               )
                      //             ],
                      //           ),
                      //         ),
                      //       ),
                      //     ),
                      //     // SongItem(laguSion[index]['title'], laguSion[index]['artist'],
                      //     //     martinGarrix, '1:53', 'C 4/4'),
                      //     Divider(),
                      //   ],
                      // );
                    }),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            _displayDialog(context);
          },
          backgroundColor: Colors.deepOrange,
          child: Icon(
            Icons.dialpad,
            size: 33,
          )),
    );
  }

  void choiceAction(String choice) {
    if (choice == MenuMore.Unduh) {
      print('Unduh');
    } else if (choice == MenuMore.Share) {
      print('Bagikan');
    }
  }
}

class MenuMore {
  static const String Unduh = 'Unduh';
  static const String Share = 'Bagikan';

  static const List<String> choices = <String>[
    Unduh,
    Share,
  ];
}

class SongSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 200,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 10,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              transform: Matrix4.translationValues(-8, 0, 0),
                              child: Icon(
                                Icons.play_arrow,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.6),
                                size: 25.0,
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(8.0, 0.0, 0.0),
                              child: PopupMenuButton<String>(
                                child: Icon(Icons.more_vert, size: 18),
                                itemBuilder: (BuildContext context) {
                                  return MenuMore.choices.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            Container(
                              height: 10,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}
