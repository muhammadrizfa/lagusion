import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:music_mulai/detail/lagu/search_numpad.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  stt.SpeechToText _speech;
  bool _isListening = false;
  String _text = ' ';
  double _confidence = 1.0;

  void initState() {
    tabController = new TabController(vsync: this, length: 4);
    super.initState();
    ukuranText();
    getDataLagu();
    _speech = stt.SpeechToText();
  }

  var apiMaster = "http://lagu-sion.demibangsa.com/api";

  List<dynamic> laguSion = [];

  Future getDataLagu() async {
    http.Response response = await http.get(apiMaster + '/song', headers: {
      'Accept': 'application/json',
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> laguDataMentah = json.decode(response.body);
      laguSion = laguDataMentah["songs"];
    });
  }

  var value;
  var language;
  var jenis_font = 'Roboto';

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
      if (prefs.getString('jenis_font') == null) {
        jenis_font = 'Roboto';
      } else {
        jenis_font = prefs.getString('jenis_font');
      }
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    var styleFont;
    if (fontstyle == 'Roboto') {
      styleFont = GoogleFonts.roboto(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Average') {
      styleFont = GoogleFonts.average(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Aleo') {
      styleFont = GoogleFonts.aleo(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Delius') {
      styleFont = GoogleFonts.delius(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Vollkorn') {
      styleFont = GoogleFonts.vollkorn(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    }
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: styleFont,
    );
  }

  void _listen() async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        setState(() => _isListening = true);
        _speech.listen(
          onResult: (val) => setState(() {
            _text = val.recognizedWords;
            _searchQueryController.text = _text;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _confidence = val.confidence;
            }
          }),
        );
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  TextEditingController _searchQueryController = TextEditingController();
  bool _isSearching = false;
  String searchQuery = "Search query";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        title: _buildSearchField(),
        actions: <Widget>[
          IconButton(
            onPressed: _listen,
            icon: Icon(Icons.mic, color: Colors.white),
          ),
          // Padding(
          //   padding: EdgeInsets.only(right: 18.0),
          //   child: speech.isListening
          //       ? GestureDetector(
          //           onTap: speech.isListening ? stopListening : null,
          //           child: Icon(
          //             Icons.close,
          //             color: Colors.white,
          //           ),
          //         )
          //       : GestureDetector(
          //           onTap: !_hasSpeech || speech.isListening
          //               ? null
          //               : startListening,
          //           child: Icon(
          //             Icons.mic,
          //             color: Colors.white,
          //           ),
          //         ),
          // ),
        ],
        bottom: new TabBar(
          controller: tabController,
          tabs: <Widget>[
            new Tab(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                tampil_text('judul', 12, jenis_font, FontWeight.normal, null),
                SizedBox(width: 2),
                judulresult.length == laguSion.length
                    ? Text(laguSion.length.toString())
                    : Text(judulresult.length.toString()),
              ],
            )),
            new Tab(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                tampil_text('lirik', 12, jenis_font, FontWeight.normal, null),
                SizedBox(width: 2),
                lirikresult.length == laguSion.length
                    ? Text(laguSion.length.toString())
                    : Text(lirikresult.length.toString()),
              ],
            )),
            new Tab(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 50,
                  child: tampil_text(
                      'all_judul', 12, jenis_font, FontWeight.normal, null),
                ),
                SizedBox(width: 2),
                alljudulresult.length == laguSion.length
                    ? Text(laguSion.length.toString())
                    : Text(alljudulresult.length.toString()),
              ],
            )),
            new Tab(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 50,
                  child: tampil_text(
                      'all_lirik', 12, jenis_font, FontWeight.normal, null),
                ),
                SizedBox(width: 2),
                alllirikresult.length == laguSion.length
                    ? Text(laguSion.length.toString())
                    : Text(alllirikresult.length.toString()),
              ],
            )),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/searchnumpad');
            // _displayDialog(context);
          },
          backgroundColor: Colors.deepOrange,
          child: Icon(
            Icons.dialpad,
            color: Colors.white,
            size: 33,
          )),
      body: TabBarView(
        controller: tabController,
        children: <Widget>[
          // JUDUL
          _searchQueryController.text == ''
              ? Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: ListView.builder(
                      itemCount: laguSion == null ? 10 : laguSion.length,
                      itemBuilder: (context, int index) {
                        return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: laguSion == null
                              ? SongSkeleton()
                              : Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        // Provider.of<HomeModel>(context).stop();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailMusicApp(
                                                    indexCurrentPlay: index,
                                                    listMusic: laguSion
                                                        .map((e) =>
                                                            SongModel.fromJson(
                                                                e))
                                                        .toList()
                                                  )),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  laguSion[index]['thumbnail'],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Flexible(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        (laguSion[index]
                                                                    ['index'])
                                                                .toString() +
                                                            '. ' +
                                                            laguSion[index]
                                                                ['title'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                        .brightness
                                                                        .toString() ==
                                                                    'Brightness.dark'
                                                                ? Colors.white
                                                                : Colors.black,
                                                            fontSize: 16.5 +
                                                                (5 * value),
                                                            fontFamily:
                                                                jenis_font),
                                                      ),
                                                      SizedBox(height: 5.0),
                                                      Text(
                                                        laguSion[index]
                                                            ['artist'],
                                                        style: TextStyle(
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.5),
                                                          fontSize: 16.5 +
                                                              (5 * value),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            'C 4/4',
                                                            style: TextStyle(
                                                              fontSize: 10 +
                                                                  (5 * value),
                                                            ),
                                                          ),
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    -8, 0, 0),
                                                            child: Icon(
                                                              Icons.play_arrow,
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors.black
                                                                      .withOpacity(
                                                                          0.6),
                                                              size: 25.0,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    8.0,
                                                                    0.0,
                                                                    0.0),
                                                            child:
                                                                PopupMenuButton<
                                                                    String>(
                                                              child: Icon(
                                                                  Icons
                                                                      .more_vert,
                                                                  size: 18),
                                                              onSelected:
                                                                  choiceAction,
                                                              itemBuilder:
                                                                  (BuildContext
                                                                      context) {
                                                                return MenuMore
                                                                    .choices
                                                                    .map((String
                                                                        choice) {
                                                                  return PopupMenuItem<
                                                                      String>(
                                                                    value:
                                                                        choice,
                                                                    child: Text(
                                                                        choice),
                                                                  );
                                                                }).toList();
                                                              },
                                                            ),
                                                          ),
                                                          Text(
                                                            laguSion[index]
                                                                    ['duration']
                                                                .replaceAll(
                                                                    new RegExp(
                                                                        r"\s+\b|\b\s"),
                                                                    ""),
                                                            style: TextStyle(
                                                              fontSize: 14 +
                                                                  (5 * value),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ),
                        );
                      }),
                )
              : Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: ListView.builder(
                      itemCount: judulresult == null ? 10 : judulresult.length,
                      itemBuilder: (context, int index) {
                        return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: judulresult == null
                              ? SongSkeleton()
                              : Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        // Provider.of<HomeModel>(context).stop();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailMusicApp(
                                                    indexCurrentPlay: index,
                                                    listMusic: judulresult
                                                        .map((e) =>
                                                            SongModel.fromJson(
                                                                e))
                                                        .toList()
                                                  )),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  judulresult[index]
                                                      ['thumbnail'],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Flexible(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        (judulresult[index]
                                                                    ['index'])
                                                                .toString() +
                                                            '. ' +
                                                            judulresult[index]
                                                                ['title'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                        .brightness
                                                                        .toString() ==
                                                                    'Brightness.dark'
                                                                ? Colors.white
                                                                : Colors.black,
                                                            fontSize: 16.5 +
                                                                (5 * value),
                                                            fontFamily:
                                                                jenis_font),
                                                      ),
                                                      SizedBox(height: 5.0),
                                                      Text(
                                                        judulresult[index]
                                                            ['artist'],
                                                        style: TextStyle(
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.5),
                                                          fontSize: 16.5 +
                                                              (5 * value),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            'C 4/4',
                                                            style: TextStyle(
                                                              fontSize: 10 +
                                                                  (5 * value),
                                                            ),
                                                          ),
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    -8, 0, 0),
                                                            child: Icon(
                                                              Icons.play_arrow,
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors.black
                                                                      .withOpacity(
                                                                          0.6),
                                                              size: 25.0,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    8.0,
                                                                    0.0,
                                                                    0.0),
                                                            child:
                                                                PopupMenuButton<
                                                                    String>(
                                                              child: Icon(
                                                                  Icons
                                                                      .more_vert,
                                                                  size: 18),
                                                              onSelected:
                                                                  choiceAction,
                                                              itemBuilder:
                                                                  (BuildContext
                                                                      context) {
                                                                return MenuMore
                                                                    .choices
                                                                    .map((String
                                                                        choice) {
                                                                  return PopupMenuItem<
                                                                      String>(
                                                                    value:
                                                                        choice,
                                                                    child: Text(
                                                                        choice),
                                                                  );
                                                                }).toList();
                                                              },
                                                            ),
                                                          ),
                                                          Text(
                                                            judulresult[index]
                                                                    ['duration']
                                                                .replaceAll(
                                                                    new RegExp(
                                                                        r"\s+\b|\b\s"),
                                                                    ""),
                                                            style: TextStyle(
                                                              fontSize: 14 +
                                                                  (5 * value),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ),
                        );
                      }),
                ),
          // LIRIK
          _searchQueryController.text == ''
              ? Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: ListView.builder(
                      itemCount: laguSion == null ? 10 : laguSion.length,
                      itemBuilder: (context, int index) {
                        return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: laguSion == null
                              ? SongSkeleton()
                              : Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        // Provider.of<HomeModel>(context).stop();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailMusicApp(
                                                    indexCurrentPlay: index,
                                                    listMusic: laguSion
                                                        .map((e) =>
                                                            SongModel.fromJson(
                                                                e))
                                                        .toList()
                                                  )),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  laguSion[index]['thumbnail'],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Flexible(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        (laguSion[index]
                                                                    ['index'])
                                                                .toString() +
                                                            '. ' +
                                                            laguSion[index]
                                                                ['title'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                        .brightness
                                                                        .toString() ==
                                                                    'Brightness.dark'
                                                                ? Colors.white
                                                                : Colors.black,
                                                            fontSize: 16.5 +
                                                                (5 * value),
                                                            fontFamily:
                                                                jenis_font),
                                                      ),
                                                      SizedBox(height: 5.0),
                                                      Text(
                                                        laguSion[index]
                                                            ['artist'],
                                                        style: TextStyle(
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.5),
                                                          fontSize: 16.5 +
                                                              (5 * value),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            'C 4/4',
                                                            style: TextStyle(
                                                              fontSize: 10 +
                                                                  (5 * value),
                                                            ),
                                                          ),
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    -8, 0, 0),
                                                            child: Icon(
                                                              Icons.play_arrow,
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors.black
                                                                      .withOpacity(
                                                                          0.6),
                                                              size: 25.0,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    8.0,
                                                                    0.0,
                                                                    0.0),
                                                            child:
                                                                PopupMenuButton<
                                                                    String>(
                                                              child: Icon(
                                                                  Icons
                                                                      .more_vert,
                                                                  size: 18),
                                                              onSelected:
                                                                  choiceAction,
                                                              itemBuilder:
                                                                  (BuildContext
                                                                      context) {
                                                                return MenuMore
                                                                    .choices
                                                                    .map((String
                                                                        choice) {
                                                                  return PopupMenuItem<
                                                                      String>(
                                                                    value:
                                                                        choice,
                                                                    child: Text(
                                                                        choice),
                                                                  );
                                                                }).toList();
                                                              },
                                                            ),
                                                          ),
                                                          Text(
                                                            laguSion[index]
                                                                    ['duration']
                                                                .replaceAll(
                                                                    new RegExp(
                                                                        r"\s+\b|\b\s"),
                                                                    ""),
                                                            style: TextStyle(
                                                              fontSize: 14 +
                                                                  (5 * value),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ),
                        );
                      }),
                )
              : Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: ListView.builder(
                      itemCount: lirikresult == null ? 10 : lirikresult.length,
                      itemBuilder: (context, int index) {
                        return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: lirikresult == null
                              ? SongSkeleton()
                              : Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        // Provider.of<HomeModel>(context).stop();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailMusicApp(
                                                    indexCurrentPlay: index,
                                                    listMusic: lirikresult
                                                        .map((e) =>
                                                            SongModel.fromJson(
                                                                e))
                                                        .toList()
                                                  )),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  lirikresult[index]
                                                      ['thumbnail'],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Flexible(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        (lirikresult[index]
                                                                    ['index'])
                                                                .toString() +
                                                            '. ' +
                                                            lirikresult[index]
                                                                ['title'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                        .brightness
                                                                        .toString() ==
                                                                    'Brightness.dark'
                                                                ? Colors.white
                                                                : Colors.black,
                                                            fontSize: 16.5 +
                                                                (5 * value),
                                                            fontFamily:
                                                                jenis_font),
                                                      ),
                                                      SizedBox(height: 5.0),
                                                      Text(
                                                        lirikresult[index]
                                                            ['artist'],
                                                        style: TextStyle(
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.5),
                                                          fontSize: 16.5 +
                                                              (5 * value),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            'C 4/4',
                                                            style: TextStyle(
                                                              fontSize: 10 +
                                                                  (5 * value),
                                                            ),
                                                          ),
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    -8, 0, 0),
                                                            child: Icon(
                                                              Icons.play_arrow,
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors.black
                                                                      .withOpacity(
                                                                          0.6),
                                                              size: 25.0,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    8.0,
                                                                    0.0,
                                                                    0.0),
                                                            child:
                                                                PopupMenuButton<
                                                                    String>(
                                                              child: Icon(
                                                                  Icons
                                                                      .more_vert,
                                                                  size: 18),
                                                              onSelected:
                                                                  choiceAction,
                                                              itemBuilder:
                                                                  (BuildContext
                                                                      context) {
                                                                return MenuMore
                                                                    .choices
                                                                    .map((String
                                                                        choice) {
                                                                  return PopupMenuItem<
                                                                      String>(
                                                                    value:
                                                                        choice,
                                                                    child: Text(
                                                                        choice),
                                                                  );
                                                                }).toList();
                                                              },
                                                            ),
                                                          ),
                                                          Text(
                                                            lirikresult[index]
                                                                    ['duration']
                                                                .replaceAll(
                                                                    new RegExp(
                                                                        r"\s+\b|\b\s"),
                                                                    ""),
                                                            style: TextStyle(
                                                              fontSize: 14 +
                                                                  (5 * value),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ),
                        );
                      }),
                ),
          // ALL JUDUL
          _searchQueryController.text == ''
              ? Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: ListView.builder(
                      itemCount: laguSion == null ? 10 : laguSion.length,
                      itemBuilder: (context, int index) {
                        return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: laguSion == null
                              ? SongSkeleton()
                              : Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        // Provider.of<HomeModel>(context).stop();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailMusicApp(
                                                    indexCurrentPlay: index,
                                                    listMusic: laguSion
                                                        .map((e) =>
                                                            SongModel.fromJson(
                                                                e))
                                                        .toList()
                                                  )),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  laguSion[index]['thumbnail'],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Flexible(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        (laguSion[index]
                                                                    ['index'])
                                                                .toString() +
                                                            '. ' +
                                                            laguSion[index]
                                                                ['title'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                        .brightness
                                                                        .toString() ==
                                                                    'Brightness.dark'
                                                                ? Colors.white
                                                                : Colors.black,
                                                            fontSize: 16.5 +
                                                                (5 * value),
                                                            fontFamily:
                                                                jenis_font),
                                                      ),
                                                      SizedBox(height: 5.0),
                                                      Text(
                                                        laguSion[index]
                                                            ['artist'],
                                                        style: TextStyle(
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.5),
                                                          fontSize: 16.5 +
                                                              (5 * value),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            'C 4/4',
                                                            style: TextStyle(
                                                              fontSize: 10 +
                                                                  (5 * value),
                                                            ),
                                                          ),
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    -8, 0, 0),
                                                            child: Icon(
                                                              Icons.play_arrow,
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors.black
                                                                      .withOpacity(
                                                                          0.6),
                                                              size: 25.0,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    8.0,
                                                                    0.0,
                                                                    0.0),
                                                            child:
                                                                PopupMenuButton<
                                                                    String>(
                                                              child: Icon(
                                                                  Icons
                                                                      .more_vert,
                                                                  size: 18),
                                                              onSelected:
                                                                  choiceAction,
                                                              itemBuilder:
                                                                  (BuildContext
                                                                      context) {
                                                                return MenuMore
                                                                    .choices
                                                                    .map((String
                                                                        choice) {
                                                                  return PopupMenuItem<
                                                                      String>(
                                                                    value:
                                                                        choice,
                                                                    child: Text(
                                                                        choice),
                                                                  );
                                                                }).toList();
                                                              },
                                                            ),
                                                          ),
                                                          Text(
                                                            laguSion[index]
                                                                    ['duration']
                                                                .replaceAll(
                                                                    new RegExp(
                                                                        r"\s+\b|\b\s"),
                                                                    ""),
                                                            style: TextStyle(
                                                              fontSize: 14 +
                                                                  (5 * value),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ),
                        );
                      }),
                )
              : Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: ListView.builder(
                      itemCount:
                          alljudulresult == null ? 10 : alljudulresult.length,
                      itemBuilder: (context, int index) {
                        return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: alljudulresult == null
                              ? SongSkeleton()
                              : Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        // Provider.of<HomeModel>(context).stop();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailMusicApp(
                                                    indexCurrentPlay: index,
                                                    listMusic: alljudulresult
                                                        .map((e) =>
                                                            SongModel.fromJson(
                                                                e))
                                                        .toList()
                                                  )),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  alljudulresult[index]
                                                      ['thumbnail'],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Flexible(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        (alljudulresult[index]
                                                                    ['index'])
                                                                .toString() +
                                                            '. ' +
                                                            alljudulresult[
                                                                index]['title'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                        .brightness
                                                                        .toString() ==
                                                                    'Brightness.dark'
                                                                ? Colors.white
                                                                : Colors.black,
                                                            fontSize: 16.5 +
                                                                (5 * value),
                                                            fontFamily:
                                                                jenis_font),
                                                      ),
                                                      SizedBox(height: 5.0),
                                                      Text(
                                                        alljudulresult[index]
                                                            ['artist'],
                                                        style: TextStyle(
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.5),
                                                          fontSize: 16.5 +
                                                              (5 * value),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            'C 4/4',
                                                            style: TextStyle(
                                                              fontSize: 10 +
                                                                  (5 * value),
                                                            ),
                                                          ),
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    -8, 0, 0),
                                                            child: Icon(
                                                              Icons.play_arrow,
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors.black
                                                                      .withOpacity(
                                                                          0.6),
                                                              size: 25.0,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    8.0,
                                                                    0.0,
                                                                    0.0),
                                                            child:
                                                                PopupMenuButton<
                                                                    String>(
                                                              child: Icon(
                                                                  Icons
                                                                      .more_vert,
                                                                  size: 18),
                                                              onSelected:
                                                                  choiceAction,
                                                              itemBuilder:
                                                                  (BuildContext
                                                                      context) {
                                                                return MenuMore
                                                                    .choices
                                                                    .map((String
                                                                        choice) {
                                                                  return PopupMenuItem<
                                                                      String>(
                                                                    value:
                                                                        choice,
                                                                    child: Text(
                                                                        choice),
                                                                  );
                                                                }).toList();
                                                              },
                                                            ),
                                                          ),
                                                          Text(
                                                            alljudulresult[
                                                                        index]
                                                                    ['duration']
                                                                .replaceAll(
                                                                    new RegExp(
                                                                        r"\s+\b|\b\s"),
                                                                    ""),
                                                            style: TextStyle(
                                                              fontSize: 14 +
                                                                  (5 * value),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ),
                        );
                      }),
                ),
          // ALL LIRIK
          _searchQueryController.text == ''
              ? Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: ListView.builder(
                      itemCount: laguSion == null ? 10 : laguSion.length,
                      itemBuilder: (context, int index) {
                        return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: laguSion == null
                              ? SongSkeleton()
                              : Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        // Provider.of<HomeModel>(context).stop();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailMusicApp(
                                                    indexCurrentPlay: index,
                                                    listMusic: laguSion
                                                        .map((e) =>
                                                            SongModel.fromJson(
                                                                e))
                                                        .toList()
                                                  )),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  laguSion[index]['thumbnail'],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Flexible(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        (laguSion[index]
                                                                    ['index'])
                                                                .toString() +
                                                            '. ' +
                                                            laguSion[index]
                                                                ['title'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                        .brightness
                                                                        .toString() ==
                                                                    'Brightness.dark'
                                                                ? Colors.white
                                                                : Colors.black,
                                                            fontSize: 16.5 +
                                                                (5 * value),
                                                            fontFamily:
                                                                jenis_font),
                                                      ),
                                                      SizedBox(height: 5.0),
                                                      Text(
                                                        laguSion[index]
                                                            ['artist'],
                                                        style: TextStyle(
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.5),
                                                          fontSize: 16.5 +
                                                              (5 * value),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            'C 4/4',
                                                            style: TextStyle(
                                                              fontSize: 10 +
                                                                  (5 * value),
                                                            ),
                                                          ),
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    -8, 0, 0),
                                                            child: Icon(
                                                              Icons.play_arrow,
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors.black
                                                                      .withOpacity(
                                                                          0.6),
                                                              size: 25.0,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    8.0,
                                                                    0.0,
                                                                    0.0),
                                                            child:
                                                                PopupMenuButton<
                                                                    String>(
                                                              child: Icon(
                                                                  Icons
                                                                      .more_vert,
                                                                  size: 18),
                                                              onSelected:
                                                                  choiceAction,
                                                              itemBuilder:
                                                                  (BuildContext
                                                                      context) {
                                                                return MenuMore
                                                                    .choices
                                                                    .map((String
                                                                        choice) {
                                                                  return PopupMenuItem<
                                                                      String>(
                                                                    value:
                                                                        choice,
                                                                    child: Text(
                                                                        choice),
                                                                  );
                                                                }).toList();
                                                              },
                                                            ),
                                                          ),
                                                          Text(
                                                            laguSion[index]
                                                                    ['duration']
                                                                .replaceAll(
                                                                    new RegExp(
                                                                        r"\s+\b|\b\s"),
                                                                    ""),
                                                            style: TextStyle(
                                                              fontSize: 14 +
                                                                  (5 * value),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ),
                        );
                      }),
                )
              : Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: ListView.builder(
                      itemCount:
                          alllirikresult == null ? 10 : alllirikresult.length,
                      itemBuilder: (context, int index) {
                        return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: alllirikresult == null
                              ? SongSkeleton()
                              : Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        // Provider.of<HomeModel>(context).stop();
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailMusicApp(
                                                    indexCurrentPlay: index,
                                                    listMusic: alllirikresult
                                                        .map((e) =>
                                                            SongModel.fromJson(
                                                                e))
                                                        .toList()
                                                  )),
                                        );
                                      },
                                      child: Container(
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.grey
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                                child: Image.network(
                                                  alllirikresult[index]
                                                      ['thumbnail'],
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Flexible(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        (alllirikresult[index]
                                                                    ['index'])
                                                                .toString() +
                                                            '. ' +
                                                            alllirikresult[
                                                                index]['title'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                        .brightness
                                                                        .toString() ==
                                                                    'Brightness.dark'
                                                                ? Colors.white
                                                                : Colors.black,
                                                            fontSize: 16.5 +
                                                                (5 * value),
                                                            fontFamily:
                                                                jenis_font),
                                                      ),
                                                      SizedBox(height: 5.0),
                                                      Text(
                                                        alllirikresult[index]
                                                            ['artist'],
                                                        style: TextStyle(
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.5),
                                                          fontSize: 16.5 +
                                                              (5 * value),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            'C 4/4',
                                                            style: TextStyle(
                                                              fontSize: 10 +
                                                                  (5 * value),
                                                            ),
                                                          ),
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    -8, 0, 0),
                                                            child: Icon(
                                                              Icons.play_arrow,
                                                              color: Theme.of(context)
                                                                          .brightness
                                                                          .toString() ==
                                                                      'Brightness.dark'
                                                                  ? Colors.white
                                                                  : Colors.black
                                                                      .withOpacity(
                                                                          0.6),
                                                              size: 25.0,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Container(
                                                            transform: Matrix4
                                                                .translationValues(
                                                                    8.0,
                                                                    0.0,
                                                                    0.0),
                                                            child:
                                                                PopupMenuButton<
                                                                    String>(
                                                              child: Icon(
                                                                  Icons
                                                                      .more_vert,
                                                                  size: 18),
                                                              onSelected:
                                                                  choiceAction,
                                                              itemBuilder:
                                                                  (BuildContext
                                                                      context) {
                                                                return MenuMore
                                                                    .choices
                                                                    .map((String
                                                                        choice) {
                                                                  return PopupMenuItem<
                                                                      String>(
                                                                    value:
                                                                        choice,
                                                                    child: Text(
                                                                        choice),
                                                                  );
                                                                }).toList();
                                                              },
                                                            ),
                                                          ),
                                                          Text(
                                                            alllirikresult[
                                                                        index]
                                                                    ['duration']
                                                                .replaceAll(
                                                                    new RegExp(
                                                                        r"\s+\b|\b\s"),
                                                                    ""),
                                                            style: TextStyle(
                                                              fontSize: 14 +
                                                                  (5 * value),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Divider(),
                                  ],
                                ),
                        );
                      }),
                ),
        ],
      ),
    );
  }

  Widget _buildSearchField() {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: TextField(
        controller: _searchQueryController,
        onChanged: onSearchTextChanged,
        autofocus: true,
        decoration: InputDecoration(
            hintText: _isListening
                ? 'Listening....'
                : language == 'indonesia' ? "Cari..." : "Search...",
            hintStyle: TextStyle(color: Colors.white60),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            contentPadding: EdgeInsets.only(top: 10.0)),
        style: TextStyle(color: Colors.white, fontSize: 20.0),
      ),
    );
  }

  void _startSearch() {
    ModalRoute.of(context)
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));

    setState(() {
      _isSearching = true;
    });
  }

  List judulresult = [];
  List lirikresult = [];
  List alljudulresult = [];
  List alllirikresult = [];

  Future getSearchLirik() async {
    http.Response response =
        await http.post(apiMaster + '/song/search', headers: {
      'Accept': 'application/json',
    }, body: {
      'keyword': _searchQueryController.text
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> _lirikresult = json.decode(response.body);
      lirikresult = _lirikresult["songs"];
      alllirikresult = _lirikresult["songs"];
    });
  }

  onSearchTextChanged(String text) async {
    judulresult.clear();
    lirikresult.clear();
    alljudulresult.clear();
    alllirikresult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    laguSion.forEach((data) {
      if (data['title'].toLowerCase().contains(text.toLowerCase()))
        alljudulresult.add(data);
    });
    getSearchLirik();
    // laguSion.forEach((data) {
    //   if (data['verses'][0]['lyrics']
    //       .toLowerCase()
    //       .contains(text.toLowerCase())) alllirikresult.add(data);
    // });

    laguSion.forEach((data) {
      if (data['title'].toLowerCase().contains(text.toLowerCase()))
        judulresult.add(data);
    });
    // laguSion.forEach((data) {
    //   if (data['verses'][0]['lyrics']
    //       .toLowerCase()
    //       .contains(text.toLowerCase())) lirikresult.add(data);
    // });

    setState(() {});
  }

  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
    });
  }

  void _stopSearching() {
    _clearSearchQuery();

    setState(() {
      _isSearching = false;
    });
  }

  void _clearSearchQuery() {
    setState(() {
      _searchQueryController.clear();
      updateSearchQuery("");
    });
  }

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          var nilai = '';
          return StatefulBuilder(builder: (context, setState) {
            return Dialog(
              child: Container(
                margin: EdgeInsets.only(bottom: 20, top: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.centerRight,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                nilai,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Theme.of(context)
                                                .brightness
                                                .toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black),
                              ),
                              Divider()
                            ],
                          ),
                        ),
                        FlatButton(
                          child: Icon(
                            Icons.backspace,
                            color: Theme.of(context).brightness.toString() ==
                                    'Brightness.dark'
                                ? Colors.white
                                : Colors.black.withOpacity(0.5),
                          ),
                          onPressed: () {
                            setState(() {
                              if (nilai.length > 0) {
                                nilai = nilai.substring(0, nilai.length - 1);
                              }
                            });
                          },
                          onLongPress: () {
                            setState(() {
                              nilai = '';
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '1';
                                  });
                                },
                                child: Container(
                                  child: Text('1',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '2';
                                  });
                                },
                                child: Container(
                                  child: Text('2',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '3';
                                  });
                                },
                                child: Container(
                                  child: Text('3',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '4';
                                  });
                                },
                                child: Container(
                                  child: Text('4',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '5';
                                  });
                                },
                                child: Container(
                                  child: Text('5',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '6';
                                  });
                                },
                                child: Container(
                                  child: Text('6',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '7';
                                  });
                                },
                                child: Container(
                                  child: Text('7',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '8';
                                  });
                                },
                                child: Container(
                                  child: Text('8',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '9';
                                  });
                                },
                                child: Container(
                                  child: Text('9',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Stack(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Text('ABC',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        nilai = nilai + '0';
                                      });
                                    },
                                    child: Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 30),
                                      child: Text('0',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 30)),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SearchNumpad(
                                                    search: nilai,
                                                  )));
                                    },
                                    child: Container(
                                      child: Icon(Icons.search, size: 30),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          });
        });
  }

  void choiceAction(String choice) {
    if (choice == MenuMore.Unduh) {
      print('Unduh');
    }
  }
}

class MenuMore {
  static const String Unduh = 'Unduh';

  static const List<String> choices = <String>[
    Unduh,
  ];
}

class SongSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 200,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 10,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              transform: Matrix4.translationValues(-8, 0, 0),
                              child: Icon(
                                Icons.play_arrow,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.6),
                                size: 25.0,
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(8.0, 0.0, 0.0),
                              child: PopupMenuButton<String>(
                                child: Icon(Icons.more_vert, size: 18),
                                itemBuilder: (BuildContext context) {
                                  return MenuMore.choices.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            Container(
                              height: 10,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}
