import 'package:flutter/material.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:music_mulai/detail/lagu/detail_music_note.dart';
import 'package:share/share.dart';

class BottomSheetWidget extends StatefulWidget {
  const BottomSheetWidget({Key key}) : super(key: key);

  @override
  _BottomSheetWidgetState createState() => _BottomSheetWidgetState();
}

class _BottomSheetWidgetState extends State<BottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(top: 5, left: 15, right: 15),
      height: 340,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Hero(
                tag: "tapShow",
                child: Icon(
                  Icons.keyboard_arrow_down,
                  color: Colors.deepOrange,
                ),
              ),
            ),
          ),
          Container(
            child: SizedBox(
              height: 300,
              child: ListView(
                children: <Widget>[
                  SongItems("Inst. Gabungan", "04:11"),
                  SongItems("Minus One", "04:11"),
                  SongItems("Inst. Acoustic", "04:11"),
                  SongItems("Vocal Trio", "04:11"),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SongItems extends StatelessWidget {
  final title;
  final menit;
  SongItems(this.title, this.menit);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Colors.amber[900],
                  borderRadius: BorderRadius.circular(50.0)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.play_arrow,
                  size: 32.0,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width - 125,
              child: Slider(
                onChanged: (double value) {},
                value: 0.0,
                activeColor: Colors.amber[900],
              ),
            ),
            Text(menit,
                style: TextStyle(
                    color: Theme.of(context).brightness.toString() ==
                            'Brightness.dark'
                        ? Colors.white
                        : Colors.black.withOpacity(0.7)))
          ],
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
              transform: Matrix4.translationValues(0.0, -10.0, 0.0),
              padding: EdgeInsets.only(left: 65),
              child: Text(
                title,
                style: TextStyle(fontSize: 16, color: Colors.deepOrange),
              )),
        ),
      ],
    );
  }
}
