import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/detail_pesan.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;
import 'package:provider/provider.dart';

class PesanPage extends StatefulWidget {
  @override
  _PesanPageState createState() => _PesanPageState();
}

class _PesanPageState extends State<PesanPage> {
  var language = 'indonesia';
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  Map<String, dynamic> _datagambar;
  List banner;
  List bannera = [0];

  Future<List> dataBanner() async {
    var url = apiMaster + "/banner";
    final response = await http.get(url, headers: {
      'Accept': 'application/json',
    });

    setState(() {
      _datagambar = json.decode(response.body);
      banner = _datagambar['banners'];
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: ukuranfont == 0
          ? TextStyle(fontFamily: fontstyle)
          : color == null
              ? TextStyle(
                  fontSize: ukuranfont + (5 * value),
                  fontWeight: fontweight,
                  fontFamily: fontstyle,
                )
              : TextStyle(
                  fontSize: ukuranfont + (5 * value),
                  fontWeight: fontweight,
                  fontFamily: fontstyle,
                  color: color,
                ),
    );
  }

  var value = 0.1;

  StreamBuilder<List<Inbox>> inboxShow(BuildContext context, inbox_id, index) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchWhereInboxs(inbox_id),
      builder: (context, AsyncSnapshot<List<Inbox>> snapshot) {
        final inboxs = snapshot.data ?? List();
        return inboxs.toString() != '[]'
            ? GestureDetector(
                onTap: () {
                  final database = Provider.of<AppDatabase>(context);

                  database.deleteInbox(inboxs[0]);
                },
                child: Container(
                  transform: Matrix4.translationValues(0, 5, 0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.star, color: Colors.amber),
                    ],
                  ),
                ),
              )
            : GestureDetector(
                onTap: () {
                  final database = Provider.of<AppDatabase>(context);
                  final inbox = Inbox(
                    inbox_id: inbox_id,
                    created_at: DateTime.now(),
                  );

                  database.insertInbox(inbox);
                },
                child: Container(
                  transform: Matrix4.translationValues(0, 5, 0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.star_border, color: Colors.black),
                    ],
                  ),
                ),
              );
      },
    );
  }

  void initState() {
    dataBanner();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pesan'),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 20),
              child: ListView.builder(
                itemCount: banner == null ? 0 : banner.length,
                itemBuilder: (context, index) {
                  return CardPesan(
                    banner[index]['img'],
                    banner[index]['title'],
                    banner[index]['message'],
                    banner[index],
                    inboxShow(context, banner[index]['id'], index),
                    banner[index]['created_at'],
                  );
                },
              ),
            ),
          ),
          // Expanded(
          //   child: ListView(
          //     children: <Widget>[
          //       inboxShow(context),
          //     ],
          //   ),
          // ),
        ],
      ),
    );
  }
}

class CardPesan extends StatelessWidget {
  final banner;
  final judul;
  final message;
  final banner_inbox;
  final favorit;
  final waktu;
  CardPesan(this.banner, this.judul, this.message, this.banner_inbox,
      this.favorit, this.waktu);

  @override
  String replaceCharAt(String oldString, int index, String newChar) {
    return oldString.substring(0, index) +
        newChar +
        oldString.substring(index + 1);
  }

  Widget build(BuildContext context) {
    var _waktu = replaceCharAt(waktu, 10, " ");
    var waktu_ =
        DateTime.parse(replaceCharAt(_waktu, 26, "")).add(Duration(hours: 8));
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailPesan(banner, judul, message)));
      },
      child: Container(
        padding: EdgeInsets.only(left: 10, right: 10, bottom: 20, top: 5),
        margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
        decoration: BoxDecoration(
          color: Theme.of(context).brightness.toString() == 'Brightness.dark'
              ? Color(0xff4d4d4d)
              : Colors.white,
          boxShadow: [
            BoxShadow(
              color:
                  Theme.of(context).brightness.toString() == 'Brightness.dark'
                      ? Colors.black.withOpacity(0.5)
                      : Colors.grey.withOpacity(0.5),
              blurRadius: 10.0, // has the effect of softening the shadow
              spreadRadius: 2.0, // has the effect of extending the shadow
              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 2,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Admin - " + DateFormat('d MMM').format(waktu_),
                  style: TextStyle(fontSize: 12),
                ),
                favorit
              ],
            ),
            SizedBox(
              height: 7,
            ),
            Row(
              children: <Widget>[
                Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey,
                  ),
                  child: Center(
                      child: Icon(LineAwesomeIcons.user, color: Colors.white)),
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      child: Text(
                        judul,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      child: Text(
                        message,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
