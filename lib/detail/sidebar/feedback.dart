import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class FeedBackPage extends StatefulWidget {
  @override
  _FeedBackPageState createState() => _FeedBackPageState();
}

class _FeedBackPageState extends State<FeedBackPage> {
  TextStyle _decorationStyleOf(BuildContext context) {
    final theme = Theme.of(context);
    return theme.textTheme.subhead.copyWith(color: theme.hintColor);
  }

  File file;
  Map<String, dynamic> kirimBug;

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: EdgeInsets.all(25),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(
                  width: 20,
                ),
                Text("Mohon Tunggu..."),
              ],
            ),
          ),
        );
      },
    );
    kirimBug_();
    new Future.delayed(new Duration(seconds: 2), () {
      Navigator.pop(context);
      Navigator.pop(context);
    });
  }

  Future<List> kirimBug_() async {
    String base64Image = base64Encode(file.readAsBytesSync());
    String fileName = file.path.split("/").last;
    var url = "http://lagu-sion.demibangsa.com/api/feedback";
    final response = await http.post(url, headers: {
      'Accept': 'application/json',
    }, body: {
      'from': fromController.text,
      'body': descriptionController.text,
      'image': base64Image,
      'system_log': '1.0',
    });

    kirimBug = json.decode(response.body);
  }

  void _pilihGambar() async {
    file = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 720, maxHeight: 720);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: EdgeInsets.all(25),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(
                  width: 20,
                ),
                Text("Mohon Tunggu..."),
              ],
            ),
          ),
        );
      },
    );
    new Future.delayed(new Duration(seconds: 2), () {
      setState(() {
        file = file;
      });
      if (file != null || file != '') {
        Navigator.pop(context);
      } else {
        Navigator.pop(context);
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return Dialog(
              child: Container(
                padding: EdgeInsets.all(25),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      width: 20,
                    ),
                    Text("Mohon Tunggu..."),
                  ],
                ),
              ),
            );
          },
        );
      }
    });
  }

  TextEditingController descriptionController = TextEditingController();
  TextEditingController fromController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.close,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text("Kirim Feedback"),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              _onLoading();
            },
            icon: Icon(Icons.send),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.centerLeft,
              children: <Widget>[
                Builder(builder: (context) {
                  return Text(
                    'From: ',
                    style: _decorationStyleOf(context),
                  );
                }),
                TextField(
                  controller: fromController,
                  decoration: InputDecoration(
                    prefixText: 'From:  ',
                    border: InputBorder.none,
                    prefixStyle: TextStyle(color: Colors.transparent),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              maxLines: 8,
              controller: descriptionController,
              decoration: InputDecoration.collapsed(
                  hintText: "Kirim produk feedback atau share ide mu"),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(bottom: 50),
                child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          transform: Matrix4.translationValues(0, 0, 0),
                          child: ListTile(
                            title: Text("Screenshot"),
                            subtitle: Text(
                              "Klik untuk menambahkan",
                              style: TextStyle(color: Colors.deepOrange),
                            ),
                            leading: Checkbox(
                              value: true,
                              onChanged: (bool value) {},
                            ),
                            trailing: Container(
                              child: file == null
                                  ? RaisedButton(
                                      onPressed: _pilihGambar,
                                      child: Text('Foto'))
                                  : Image.file(file),
                            ),
                          ),
                        ),
                        Text(
                          "Beberapa info sistem akan dikirimkan ke Lagu Sion. Kami akan menggunakan informasi yang anda kirim untuk masalah teknis dan meningkatkan pelayanan, sesuai dengan Kebijakan Privasi dan Ketentuan Layanan kami.",
                          style: TextStyle(fontSize: 10),
                        ),
                      ],
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
