import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/detail/search.dart';
import 'package:music_mulai/detail/sidebar/about.dart';
import 'package:music_mulai/detail/sidebar/feedback.dart';
import 'package:music_mulai/detail/sidebar/pengaturan.dart';
import 'package:music_mulai/detail/sidebar/pesan.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:convert';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;

import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LaguSionBaruHome extends StatefulWidget {
  @override
  _LaguSionBaruHomeState createState() => _LaguSionBaruHomeState();
}

class _LaguSionBaruHomeState extends State<LaguSionBaruHome> {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';
  var flume =
      'https://i.scdn.co/image/8d84f7b313ca9bafcefcf37d4e59a8265c7d3fff';

  var jenis_font = 'Roboto';
  var value = 0.1;
  var language;

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
      if (prefs.getString('jenis_font') == null) {
        jenis_font = 'Roboto';
      } else {
        jenis_font = prefs.getString('jenis_font');
      }
    });
  }

  List<dynamic> laguSion;
  var lagu_utama;

  Future getDataLagu() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    lagu_utama = prefs.getString('lagu_utama') == null
        ? laguSion[0]['album']['album_name']
        : prefs.getString('lagu_utama');
    http.Response response =
        await http.post(apiMaster + '/album/search', headers: {
      'Accept': 'application/json',
    }, body: {
      'album_name': lagu_utama,
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> laguDataMentah = json.decode(response.body);
      laguSion = laguDataMentah["albums"][0]['songs'];
      // print(laguSion);
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    var styleFont;
    if (fontstyle == 'Roboto') {
      styleFont = GoogleFonts.roboto(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Average') {
      styleFont = GoogleFonts.average(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Aleo') {
      styleFont = GoogleFonts.aleo(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Delius') {
      styleFont = GoogleFonts.delius(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Vollkorn') {
      styleFont = GoogleFonts.vollkorn(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    }
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: styleFont,
    );
  }

  void initState() {
    super.initState();
    getDataLagu();
    ukuranText();
  }

  @override
  Widget build(BuildContext context) {
    getDataLagu();
    return Scaffold(
      appBar: AppBar(
        title: Container(
          transform: Matrix4.translationValues(-20.0, 0.0, 0.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 35.0,
                    width: 35.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        'https://a4.ugziki.co.ug/no-cover-art/nocover-800x800.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(left: 5),
                width: 200,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      lagu_utama == null ? '...' : lagu_utama,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SearchPage()));
            },
            icon: Icon(Icons.search),
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 155.0,
              child: DrawerHeader(
                child: Row(
                  children: <Widget>[
                    Container(
                      transform: Matrix4.translationValues(0, -13, 0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: Colors.white,
                        ),
                        width: 60,
                        height: 60,
                        child: Icon(
                          Icons.music_note,
                          size: 30,
                          color: Colors.deepOrange,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Online MP3",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 25 + (5 * value)),
                            textAlign: TextAlign.left,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: 190,
                            child: Text(
                              "Play your favorite Songs",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15 + (5 * value)),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Colors.deepOrange,
                ),
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.home),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Home',
                    style: TextStyle(fontSize: 15 + (5 * value)),
                  ),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.settings),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text(
                      'pengaturan', 15, jenis_font, FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PengaturanPage()));
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.mail),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text('pesan', 15, jenis_font, FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PesanPage()));
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.feedback),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text('kirim_feedback', 15, jenis_font,
                      FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FeedBackPage()));
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.share),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text('bagikan_aplikasi', 15, jenis_font,
                      FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Icon(Icons.info_outline),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text('tentang_aplikasi', 15, jenis_font,
                      FontWeight.normal, null),
                ],
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AboutPage()));
              },
            ),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10, right: 0, top: 10),
        child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: laguSion == null ? 10 : laguSion.length,
            itemBuilder: (context, int index) {
              return Container(
                padding: EdgeInsets.only(right: 10),
                child: laguSion == null
                    ? SongSkeleton()
                    : Column(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              // Provider.of<HomeModel>(context).stop();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailMusicApp(
                                          indexCurrentPlay: index,
                                          listMusic: laguSion
                                              .map((e) => SongModel.fromJson(e))
                                              .toList()
                                        )),
                              );
                            },
                            child: Container(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                        color: Colors.grey.withOpacity(0.4),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: Image.network(
                                        laguSion[index]['thumbnail'],
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Flexible(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              (laguSion[index]['index'])
                                                      .toString() +
                                                  '. ' +
                                                  laguSion[index]['title'],
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Theme.of(context)
                                                              .brightness
                                                              .toString() ==
                                                          'Brightness.dark'
                                                      ? Colors.white
                                                      : Colors.black,
                                                  fontSize: 16.5 + (5 * value),
                                                  fontFamily: 'Roboto'),
                                            ),
                                            SizedBox(height: 5.0),
                                            Text(
                                              laguSion[index]['artist'],
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                            .brightness
                                                            .toString() ==
                                                        'Brightness.dark'
                                                    ? Colors.white
                                                    : Colors.black
                                                        .withOpacity(0.5),
                                                fontSize: 16.5 + (5 * value),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  'C 4/4',
                                                  style: TextStyle(
                                                    fontSize: 10 + (5 * value),
                                                  ),
                                                ),
                                                Container(
                                                  transform:
                                                      Matrix4.translationValues(
                                                          -8, 0, 0),
                                                  child: Icon(
                                                    Icons.play_arrow,
                                                    color: Theme.of(context)
                                                                .brightness
                                                                .toString() ==
                                                            'Brightness.dark'
                                                        ? Colors.white
                                                        : Colors.black
                                                            .withOpacity(0.6),
                                                    size: 25.0,
                                                  ),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: <Widget>[
                                                Container(
                                                  transform:
                                                      Matrix4.translationValues(
                                                          8.0, 0.0, 0.0),
                                                  child:
                                                      PopupMenuButton<String>(
                                                    child: Icon(Icons.more_vert,
                                                        size: 18),
                                                    onSelected: (choice) async {
                                                      if (choice ==
                                                          MenuMore.Unduh) {
                                                        var dir =
                                                            await getExternalStorageDirectory();
                                                        final database = Provider
                                                            .of<AppDatabase>(
                                                                context);
                                                        final song = Song(
                                                          idMusic:
                                                              laguSion[index]
                                                                  ["id"],
                                                          musicVersion: 1,
                                                          data: json.encode(
                                                              laguSion[index]),
                                                          created_at:
                                                              DateTime.now(),
                                                        );

                                                        database
                                                            .insertSong(song);
                                                      } else if (choice ==
                                                          MenuMore.Share) {
                                                        Share.share(
                                                            'Dengarkan lagu ' +
                                                                laguSion[index]
                                                                    ["title"] +
                                                                ' di aplikasi Lagu Sion Plus');
                                                      }
                                                    },
                                                    itemBuilder:
                                                        (BuildContext context) {
                                                      return MenuMore.choices
                                                          .map((String choice) {
                                                        return PopupMenuItem<
                                                            String>(
                                                          value: choice,
                                                          child: Text(choice),
                                                        );
                                                      }).toList();
                                                    },
                                                  ),
                                                ),
                                                Text(
                                                  laguSion[index]['duration']
                                                      .replaceAll(
                                                          new RegExp(
                                                              r"\s+\b|\b\s"),
                                                          ""),
                                                  style: TextStyle(
                                                    fontSize: 14 + (5 * value),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Divider(),
                        ],
                      ),
              );
            }),
      ),
    );
  }

  void choiceAction(String choice) {
    if (choice == MenuMore.Unduh) {
      print('Unduh');
    }
  }
}

class MenuMore {
  static const String Unduh = 'Unduh';
  static const String Share = 'Bagikan';

  static const List<String> choices = <String>[
    Unduh,
    Share,
  ];
}

class SongSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 200,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 10,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              transform: Matrix4.translationValues(-8, 0, 0),
                              child: Icon(
                                Icons.play_arrow,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.6),
                                size: 25.0,
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(8.0, 0.0, 0.0),
                              child: PopupMenuButton<String>(
                                child: Icon(Icons.more_vert, size: 18),
                                itemBuilder: (BuildContext context) {
                                  return MenuMore.choices.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            Container(
                              height: 10,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}
