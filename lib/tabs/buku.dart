
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:music_mulai/bloc/audio_book_model.dart';
import 'package:music_mulai/bloc/ibadah_model.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/audiobook/list_audiobook.dart';
import 'package:music_mulai/detail/audiobook/list_ibadah.dart';
import 'dart:async';
import 'dart:convert';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import '../util/list_ext.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class BukuPage extends StatefulWidget {
  @override
  _BukuPageState createState() => _BukuPageState();
}

class _BukuPageState extends State<BukuPage> {
  Dio dioClient;
  var value = 0.1;
  var language;
  var jenis_font = 'Roboto';

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
      if (prefs.getString('jenis_font') == null) {
        jenis_font = 'Roboto';
      } else {
        jenis_font = prefs.getString('jenis_font');
      }
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    var styleFont;
    if (fontstyle == 'Roboto') {
      styleFont = GoogleFonts.roboto(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Average') {
      styleFont = GoogleFonts.average(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Aleo') {
      styleFont = GoogleFonts.aleo(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Delius') {
      styleFont = GoogleFonts.delius(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Vollkorn') {
      styleFont = GoogleFonts.vollkorn(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    }
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: styleFont,
    );
  }

  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  List<AudioBookModel> listAudioBook = List();
  List<AudioIbadahModel> listAudioIbadah = List();

  Future getDataBook() async {
    http.Response response =
        await http.get(apiMaster + '/audio-book', headers: {
      'Accept': 'application/json',
    });
    listAudioBook.clear();
    setState(() {
      Map<String, dynamic> _audioBook = json.decode(response.body);
      _audioBook["audio_books"].forEach((v) {
        listAudioBook.add(AudioBookModel.fromJson(v));
      });
    });
  }
  Future getDataIbadah() async {
    http.Response response = await http.get(apiMaster + '/ibadah', headers: {
      'Accept': 'application/json',
    });
    listAudioIbadah.clear();
    setState(() {
      Map<String, dynamic> _ibadah = json.decode(response.body);
      _ibadah["ibadahs"].forEach((v) {
        listAudioIbadah.add(AudioIbadahModel.fromJson(v));
      });
    });
  }

  String replaceCharAt(String oldString, int index, String newChar) {
    return oldString.substring(0, index) +
        newChar +
        oldString.substring(index + 1);
  }

  void initState() {
    super.initState();
    getDataBook();
    ukuranText();
    getDataIbadah();
  }

  @override
  void dispose() {
    if (dioClient != null) dioClient.close();
    super.dispose();
  }
  
  var downloadingAudioBook = false;
  var progressString = '';
  var progressStringAudioBook = '';
  var indexAudioBook = 0;
  var i = 0;

  Future<void> downloadIbadahFile(AudioIbadahModel ibadah) async {
    final database = Provider.of<AppDatabase>(context, listen: false);
    ibadah.parts.forEachIndex((part, index) async {
      setState(() {
        downloadingAudioBook = true;
        indexAudioBook =  index;
      });
      final path = await downloadFile(part.music);
      final ibadahPart = IbadahPart(
        created_at: DateTime.now(),
        ibadahId: ibadah.id,
        partId: part.id,
        data:json.encode(part.toJson())
      );
      database.insertIbadahParts(ibadahPart);
      final downloadedPart = DownloadedIbadahPartData(
        createdAt: DateTime.now(),
        ibadahId: ibadah.id,
        partId: part.id,
        localFilePath: path
      );
      database.insertDownloadedIbadahPart(downloadedPart);
    });

    final ibadahs = Ibadah(
      ibadahId: ibadah.id,
      data: json.encode(ibadah.toJson()),
      created_at: DateTime.now(),
    );
    database.insertIbadah(ibadahs);
    
    final dlIbadaBook = DownloadedIbadahData(
      createdAt: DateTime.now(),
      ibadahId: ibadah.id
    );
    database.insertDownloadedBookIbadah(dlIbadaBook);
    
    setState(() {
      downloadingAudioBook = false;
    });
  }

  Future<void> downloadBookFile(AudioBookModel book) async {
    final database = Provider.of<AppDatabase>(context, listen: false);
    book.parts.forEachIndex((part, index) async {
      setState(() {
        downloadingAudioBook = true;
        indexAudioBook =  index;
      });
      final path = await downloadFile(part.music);
      final bookPart = AudioBookPart(
          createdAt: DateTime.now(),
          bookId: book.id,
          partId: part.id,
          data:json.encode(part.toJson())
      );
      database.insertBookParts(bookPart);

      final downloadedPart = DownloadedBookPartData(
          createdAt: DateTime.now(),
          bookId: book.id,
          partId: part.id,
          localFilePath: path
      );
      database.insertDownloadedBookPart(downloadedPart);
    });

    final books = AudioBook(
      bookId : book.id,
      data: json.encode(book.toJson()),
      created_at: DateTime.now(),
    );
    database.insertAudioBook(books);

    final dlBook = DownloadedBookData(
        createdAt: DateTime.now(),
        bookId: book.id
    );
    database.insertDownloadedBook(dlBook);

    setState(() {
      downloadingAudioBook = false;
    });
  }

  Future<String> downloadFile(String urlAudio) async {
    Dio dio = dioClient ?? Dio();
    try {
      final dir = await getApplicationDocumentsDirectory();
      final String fileName = "${Uuid().v1()}.mp3";
      final String newFilePath = "${dir.path}/$fileName";
      await dio.download(urlAudio,
          newFilePath,
          onReceiveProgress: (rec, total) {
        print("Rec : $rec, Total : $total");
        setState(() {
          progressStringAudioBook =
              ((rec / total) * 100).toStringAsFixed(0) + '%';
        });
      });
      print("Location downloaded file ->> $newFilePath");
      return newFilePath;
    } catch (e) {
      print(e);
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 15, right: 10, top: 25),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                audioIbadahSegment(),
                SizedBox(height: 10),
                audioBookSegment()
              ],
            ),
          ),
        ),
        downloadingAudioBook == true
            ? Positioned(
                right: 10,
                bottom: 50,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: Colors.deepOrange,
                    borderRadius: BorderRadius.circular(50),
                  ),
                  child: Center(
                    child: Text(
                      'Downloading : ' +
                          progressStringAudioBook +
                          ' ' +
                          (indexAudioBook + 1).toString() +
                          '/' +
                          listAudioBook.length.toString(),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              )
            : Container(),
      ],
    );
  }

  Widget audioIbadahSegment() => Builder(
        builder: (context) {
          if (listAudioIbadah.isEmpty) {
            return Container();
          }
          return Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(color: Colors.amber[800]),
                        child: SizedBox(
                          height: 50,
                          width: 5,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      tampil_text(
                          'ibadah', 25, jenis_font, FontWeight.bold, null),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 20),
              Container(
                height: 165,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: listAudioIbadah.length,
                    itemBuilder: (context, index) {
                      final audioIbadah = listAudioIbadah[index];
                      return Row(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ListIbadah(
                                        audioBookModel: audioIbadah,
                                      )),
                            ),
                            child: Container(
                              width: 100,
                              padding: EdgeInsets.only(bottom: 10),
                              margin: EdgeInsets.only(bottom: 10),
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Theme.of(context)
                                                .brightness
                                                .toString() ==
                                            'Brightness.dark'
                                        ? Colors.black
                                        : Colors.grey.withOpacity(0.5),
                                    blurRadius: 10.0,
                                    // has the effect of softening the shadow
                                    spreadRadius: 0.0,
                                    // has the effect of extending the shadow
                                    offset: Offset(
                                      0.0, // horizontal, move right 10
                                      0.0, // vertical, move down 10
                                    ),
                                  )
                                ],
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Color(0xff4d4d4d)
                                        : Colors.white,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Stack(
                                    children: <Widget>[
                                      Container(
                                        height: 100,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: new NetworkImage(
                                                "https://i.pinimg.com/originals/76/34/68/763468669f1e3d3de9f9077860cb4767.jpg"),
                                            fit: BoxFit.cover,
                                          ),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(5),
                                              topRight: Radius.circular(5)),
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 5,
                                        right: 5,
                                        child: Icon(
                                          Icons.play_circle_filled,
                                          color: Theme.of(context)
                                                      .brightness
                                                      .toString() ==
                                                  'Brightness.dark'
                                              ? Color(0xff4d4d4d)
                                              : Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        width: 80,
                                        padding: EdgeInsets.only(left: 5),
                                        child: Text(
                                          audioIbadah.ibadahBookName,
                                          style: TextStyle(
                                              fontSize: 13 + (5 * value)),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      StreamBuilder<bool>(
                                        stream: Provider.of<AppDatabase>(
                                            context,
                                            listen: false)
                                            .isIbadahBookDownloaded(
                                            audioIbadah.id),
                                        builder: (context, snapshot){
                                          if (snapshot.hasData) {
                                            List<String> choiceMenu;
                                            if (snapshot.data) {
                                              choiceMenu = ["Downloaded"];
                                            } else {
                                              choiceMenu = MenuMore.choices;
                                            }
                                            return PopupMenuButton<String>(
                                              child: Icon(Icons.more_vert, size: 18),
                                              onSelected: (choice) async {
                                                if (choice == MenuMore.Unduh) {
                                                  downloadIbadahFile(audioIbadah);
                                                }
                                              },
                                              itemBuilder: (BuildContext context) {
                                                Provider.of<AppDatabase>(
                                                    context,
                                                    listen: false);
                                                return choiceMenu
                                                    .map((String choice) {
                                                  return PopupMenuItem<String>(
                                                    value: choice,
                                                    child: Text(choice),
                                                  );
                                                }).toList();
                                              },
                                            );
                                          }
                                          return Container();
                                        },
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 5),
                                      child: Text(
                                        DateFormat('d MMM y')
                                            .format(DateTime.now()),
                                        style: TextStyle(
                                            fontSize: 12 + (5 * value)),
                                        overflow: TextOverflow.ellipsis,
                                      )),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                        ],
                      );
                    }),
              )
            ],
          );
        },
      );

  Widget audioBookSegment() => Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(color: Colors.amber[800]),
                    child: SizedBox(
                      height: 50,
                      width: 5,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text(
                      'audiobook', 25, jenis_font, FontWeight.bold, null)
                ],
              ),
            ],
          ),
          SizedBox(height: 20),
          Container(
              child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    childAspectRatio:
                        (MediaQuery.of(context).size.width - 110) /
                            (MediaQuery.of(context).size.width),
                  ),
                  itemCount: listAudioBook.length,
                  itemBuilder: (context, index) {
                    final audioBook = listAudioBook[index];

                    return Wrap(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ListAudioBook(
                                      audioBookModel: audioBook,
                                    )),
                          ),
                          child: Container(
                            width: (MediaQuery.of(context).size.width / 3.7),
                            padding: EdgeInsets.only(bottom: 10),
                            margin: EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color:
                                      Theme.of(context).brightness.toString() ==
                                              'Brightness.dark'
                                          ? Colors.black
                                          : Colors.grey.withOpacity(0.5),
                                  blurRadius: 10.0,
                                  // has the effect of softening the shadow
                                  spreadRadius: 0.0,
                                  // has the effect of extending the shadow
                                  offset: Offset(
                                    0.0, // horizontal, move right 10
                                    0.0, // vertical, move down 10
                                  ),
                                )
                              ],
                              color: Theme.of(context).brightness.toString() ==
                                      'Brightness.dark'
                                  ? Color(0xff4d4d4d)
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Stack(
                                  children: <Widget>[
                                    Container(
                                      height: 100,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: new NetworkImage(
                                              audioBook.thumbnail),
                                          fit: BoxFit.cover,
                                        ),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(5),
                                            topRight: Radius.circular(5)),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 5,
                                      right: 5,
                                      child: Icon(
                                        Icons.play_circle_filled,
                                        color: Theme.of(context)
                                                    .brightness
                                                    .toString() ==
                                                'Brightness.dark'
                                            ? Color(0xff4d4d4d)
                                            : Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        padding: EdgeInsets.only(left: 5),
                                        child: Text(
                                          audioBook.audioBookName,
                                          style: TextStyle(
                                              fontSize: 13 + (5 * value)),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ),
                                    StreamBuilder<bool>(
                                      stream: Provider.of<AppDatabase>(
                                          context,
                                          listen: false)
                                          .isBookBookDownloaded(
                                          audioBook.id),
                                      builder: (context, snapshot){
                                        if (snapshot.hasData) {
                                          List<String> choiceMenu;
                                          if (snapshot.data) {
                                            choiceMenu = ["Downloaded"];
                                          } else {
                                            choiceMenu = MenuMore.choices;
                                          }
                                          return PopupMenuButton<String>(
                                            child: Icon(Icons.more_vert, size: 18),
                                            onSelected: (choice) async {
                                              if (choice == MenuMore.Unduh) {
                                                downloadBookFile(audioBook);
                                              }
                                            },
                                            itemBuilder: (BuildContext context) {
                                              Provider.of<AppDatabase>(
                                                  context,
                                                  listen: false);
                                              return choiceMenu
                                                  .map((String choice) {
                                                return PopupMenuItem<String>(
                                                  value: choice,
                                                  child: Text(choice),
                                                );
                                              }).toList();
                                            },
                                          );
                                        }
                                        return Container();
                                      },
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    child: Text(
                                      audioBook.parts.isEmpty
                                          ? ""
                                          : audioBook.parts.first.title,
                                      style:
                                          TextStyle(fontSize: 12 + (5 * value)),
                                      overflow: TextOverflow.ellipsis,
                                    )),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: 10),
                      ],
                    );
                  }))
        ],
      );
}

class CardBook extends StatefulWidget {
  final title;
  final image;
  final subtitle;
  CardBook(this.title, this.image, this.subtitle);
  @override
  _CardBookState createState() => _CardBookState();
}

class _CardBookState extends State<CardBook> {
  var value = 0.1;
  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
    });
    Future.delayed(Duration(seconds: 1), () {
      ukuranText();
    });
  }

  void initState() {
    ukuranText();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onTap: () => Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => DetailBook('pause')),
      // ),
      child: Container(
        width: 100,
        padding: EdgeInsets.only(bottom: 10),
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color:
                  Theme.of(context).brightness.toString() == 'Brightness.dark'
                      ? Colors.black
                      : Colors.grey.withOpacity(0.5),
              blurRadius: 10.0, // has the effect of softening the shadow
              spreadRadius: 0.0, // has the effect of extending the shadow
              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          color: Theme.of(context).brightness.toString() == 'Brightness.dark'
              ? Color(0xff4d4d4d)
              : Colors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: new NetworkImage(widget.image),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5)),
                  ),
                ),
                Positioned(
                  bottom: 5,
                  right: 5,
                  child: Icon(
                    Icons.play_circle_filled,
                    color: Theme.of(context).brightness.toString() ==
                            'Brightness.dark'
                        ? Color(0xff4d4d4d)
                        : Colors.white,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 80,
                  padding: EdgeInsets.only(left: 5),
                  child: Text(
                    widget.title,
                    style: TextStyle(fontSize: 13 + (5 * value)),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                PopupMenuButton<String>(
                  child: Icon(Icons.more_vert, size: 18),
                  onSelected: (choice) async {
                    // if (choice == MenuMore.Unduh) {
                    //   BukuPage.downloadIbadahFile(index);
                    // }
                  },
                  itemBuilder: (BuildContext context) {
                    return MenuMore.choices.map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Text(choice),
                      );
                    }).toList();
                  },
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Text(
                  widget.subtitle,
                  style: TextStyle(fontSize: 12 + (5 * value)),
                  overflow: TextOverflow.ellipsis,
                )),
          ],
        ),
      ),
    );
  }
}

class MenuMore {
  static const String Unduh = 'Unduh';

  static const List<String> choices = <String>[
    Unduh,
    // LaguUtama,
  ];
}
