import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/lagu/detail_tabLagu.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;

class LaguPage extends StatefulWidget {
  @override
  _LaguPageState createState() => _LaguPageState();
}

class _LaguPageState extends State<LaguPage> {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";

  List album;

  var value = 0.1;
  var language;

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
    });
    Future.delayed(Duration(seconds: 1), () {
      ukuranText();
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: ukuranfont == 0
          ? TextStyle(fontFamily: fontstyle)
          : color == null
              ? TextStyle(
                  fontSize: ukuranfont + (5 * value),
                  fontWeight: fontweight,
                  fontFamily: fontstyle,
                )
              : TextStyle(
                  fontSize: ukuranfont + (5 * value),
                  fontWeight: fontweight,
                  fontFamily: fontstyle,
                  color: color,
                ),
    );
  }

  Future getAlbum() async {
    http.Response response = await http.get(apiMaster + '/album', headers: {
      'Accept': 'application/json',
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> _album = json.decode(response.body);
      album = _album["albums"];
    });
  }

  void initState() {
    super.initState();
    getAlbum();
    ukuranText();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 15, right: 10, top: 25),
          child: GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: 2.4 / 3,
            ),
            itemCount: album == null ? 0 : album.length,
            itemBuilder: (context, index) {
              return Wrap(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  LaguSionBaru(album[index]['album_name'])));
                    },
                    child: CardLagu(
                        album[index]['album_name'],
                        "https://a4.ugziki.co.ug/no-cover-art/nocover-800x800.png",
                        album[index]['number_of_songs'].toString() +
                            " Siap Didownload"),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                ],
              );
            },
          ),
        ),
      ],
    );
  }
}

class CardLagu extends StatefulWidget {
  final title;
  final image;
  final subtitle;
  CardLagu(this.title, this.image, this.subtitle);
  @override
  _CardLaguState createState() => _CardLaguState();
}

class _CardLaguState extends State<CardLagu> {
  var value = 0.1;
  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
    });
    Future.delayed(Duration(seconds: 1), () {
      ukuranText();
    });
  }

  void initState() {
    ukuranText();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 3.7,
      // height: MediaQuery.of(context).size.height / 5,
      padding: EdgeInsets.only(bottom: 10),
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).brightness.toString() == 'Brightness.dark'
                ? Colors.black
                : Colors.grey.withOpacity(0.5),
            blurRadius: 10.0, // has the effect of softening the shadow
            spreadRadius: 0.0, // has the effect of extending the shadow
            offset: Offset(
              0.0, // horizontal, move right 10
              0.0, // vertical, move down 10
            ),
          )
        ],
        color: Theme.of(context).brightness.toString() == 'Brightness.dark'
            ? Color(0xff4d4d4d)
            : Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height / 8,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: new NetworkImage(widget.image),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5)),
                ),
              ),
              Positioned(
                bottom: 5,
                right: 5,
                child: Icon(
                  Icons.play_circle_filled,
                  color: Theme.of(context).brightness.toString() ==
                          'Brightness.dark'
                      ? Colors.black
                      : Colors.white,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 5),
                child: Text(
                  widget.title,
                  style: TextStyle(fontSize: 13 + (5 * value)),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              PopupMenuButton<String>(
                child: Icon(Icons.more_vert, size: 15),
                onSelected: (choice) async {
                  if (choice == MenuMore.LaguUtama) {
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    prefs.setString('lagu_utama', widget.title);
                    prefs.commit();
                  }
                },
                itemBuilder: (BuildContext context) {
                  return MenuMore.choices.map((String choice) {
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList();
                },
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                widget.subtitle,
                style: TextStyle(fontSize: 10 + (5 * value)),
                overflow: TextOverflow.ellipsis,
              )),
        ],
      ),
    );
  }

  void choiceAction(String choice) {
    if (choice == MenuMore.Unduh) {
      print('Unduh');
    } else if (choice == MenuMore.LaguUtama) {
      print('Jadikan Lagu Utama');
    }
  }
}

class MenuMore {
  static const String Unduh = 'Unduh';
  static const String LaguUtama = 'Jadikan Lagu Utama';

  static const List<String> choices = <String>[
    LaguUtama,
  ];
}
