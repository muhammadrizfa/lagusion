import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/lagu/detail_favorit.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:music_mulai/detail/lagu/detail_playlist.dart';
import 'package:music_mulai/main.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;

class PlaylistPage extends StatefulWidget {
  @override
  _PlaylistPageState createState() => _PlaylistPageState();
}

class _PlaylistPageState extends State<PlaylistPage> {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  List<dynamic> laguSion;
  var flume =
      'https://i.scdn.co/image/8d84f7b313ca9bafcefcf37d4e59a8265c7d3fff';
  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';

  var value = 0.1;
  var language;
  List dataPlaylist = [0, 1, 2, 3, 4];

  var jenis_font = 'Roboto';

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
      if (prefs.getString('jenis_font') == null) {
        jenis_font = 'Roboto';
      } else {
        jenis_font = prefs.getString('jenis_font');
      }
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    var styleFont;
    if (fontstyle == 'Roboto') {
      styleFont = GoogleFonts.roboto(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Average') {
      styleFont = GoogleFonts.average(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Aleo') {
      styleFont = GoogleFonts.aleo(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Delius') {
      styleFont = GoogleFonts.delius(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Vollkorn') {
      styleFont = GoogleFonts.vollkorn(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    }
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: styleFont,
    );
  }

  StreamBuilder<List<Playlist>> recentlyAddedSong(BuildContext context) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchAllRecentPlaylist(),
      builder: (context, AsyncSnapshot<List<Playlist>> snapshot) {
        final songs = snapshot.data ?? List();

        return Column(
          children: <Widget>[
            songs.toString() == '[]'
                ? Container()
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(color: Colors.amber[800]),
                            child: SizedBox(
                              height: 50,
                              width: 5,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          tampil_text('recently_added', 25, jenis_font,
                              FontWeight.bold, null),
                        ],
                      ),
                    ],
                  ),
            SizedBox(height: 10),
            Container(
              child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: laguSion == null
                    ? 0
                    : laguSion.length < 3 ? laguSion.length : 3,
                itemBuilder: (_, int index) {
                  var id = laguSion[index]['id'];
                  return recentlyAddedShow(context, id, index);
                },
              ),
            ),
          ],
        );
      },
    );
  }

  StreamBuilder<List<Playlist>> recentlyAddedShow(
      BuildContext context, id, index) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchWhereRecentPlaylist(id),
      builder: (context, AsyncSnapshot<List<Playlist>> snapshot) {
        final songs = snapshot.data ?? List();
        return Container(
          padding: EdgeInsets.only(right: 10),
          child: songs.toString() == '[]'
              ? Container()
              : Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        // Provider.of<HomeModel>(context).stop();
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailMusicApp(
                                  indexCurrentPlay: index,
                                  listMusic: laguSion
                                      .map((e) => SongModel.fromJson(e))
                                      .toList())),
                        );
                      },
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                  color: Colors.grey.withOpacity(0.4),
                                  borderRadius: BorderRadius.circular(10)),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Image.network(
                                  laguSion[index]['thumbnail'],
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        (laguSion[index]['index']).toString() +
                                            '. ' +
                                            laguSion[index]['title'],
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                        .brightness
                                                        .toString() ==
                                                    'Brightness.dark'
                                                ? Colors.white
                                                : Colors.black,
                                            fontSize: 16 + (5 * value),
                                            fontFamily: jenis_font),
                                      ),
                                      SizedBox(height: 5.0),
                                      Text(
                                        laguSion[index]['artist'],
                                        style: TextStyle(
                                          color: Theme.of(context)
                                                      .brightness
                                                      .toString() ==
                                                  'Brightness.dark'
                                              ? Colors.white
                                              : Colors.black.withOpacity(0.5),
                                          fontSize: 16 + (5 * value),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'C 4/4',
                                            style: TextStyle(
                                              fontSize: 10 + (5 * value),
                                            ),
                                          ),
                                          Container(
                                            transform:
                                                Matrix4.translationValues(
                                                    -8, 0, 0),
                                            child: Icon(
                                              Icons.play_arrow,
                                              color: Theme.of(context)
                                                          .brightness
                                                          .toString() ==
                                                      'Brightness.dark'
                                                  ? Colors.white
                                                  : Colors.black
                                                      .withOpacity(0.6),
                                              size: 25.0,
                                            ),
                                          )
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Container(
                                            transform:
                                                Matrix4.translationValues(
                                                    8.0, 0.0, 0.0),
                                            child: PopupMenuButton<String>(
                                              child: Icon(Icons.more_vert,
                                                  size: 18),
                                              onSelected: (choice) async {
                                                if (choice == MenuMore.Unduh) {
                                                  // downloadFile(index);
                                                  final database =
                                                      Provider.of<AppDatabase>(
                                                          context);
                                                  final song = Song(
                                                    musicVersion: 1,
                                                    idMusic: laguSion[index]
                                                        ["id"],
                                                    data: json.encode(
                                                        laguSion[index]),
                                                    created_at: DateTime.now(),
                                                  );

                                                  database.insertSong(song);
                                                }
                                              },
                                              itemBuilder:
                                                  (BuildContext context) {
                                                return MenuMore.choices
                                                    .map((String choice) {
                                                  return PopupMenuItem<String>(
                                                    value: choice,
                                                    child: Text(choice),
                                                  );
                                                }).toList();
                                              },
                                            ),
                                          ),
                                          Text(
                                            laguSion[index]['duration']
                                                .replaceAll(
                                                    new RegExp(r"\s+\b|\b\s"),
                                                    ""),
                                            style: TextStyle(
                                              fontSize: 14 + (5 * value),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Divider(),
                  ],
                ),
        );
      },
    );
  }

  // List banyakPlaylist = [];

  StreamBuilder<List<JudulPlaylist>> judulPlaylist(BuildContext context) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchAllJudulPlaylists(),
      builder: (context, AsyncSnapshot<List<JudulPlaylist>> snapshot) {
        final playlists = snapshot.data ?? List();

        return GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: (MediaQuery.of(context).size.width - 110) /
                  (MediaQuery.of(context).size.width),
            ),
            itemCount: playlists.length + 2,
            itemBuilder: (context, index) {
              // countPlaylist(playlists, index - 1);

              return index + 1 == playlists.length + 2
                  ? GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: Text('Tambah Playlist'),
                                content: TextField(
                                  controller: namaPlaylist,
                                  decoration: InputDecoration(
                                      hintText: "Masukkan nama playlist"),
                                ),
                                actions: <Widget>[
                                  new FlatButton(
                                    child: new Text('BATAL',
                                        style: TextStyle(color: Colors.orange)),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  new FlatButton(
                                    child: new Text('TAMBAHKAN',
                                        style: TextStyle(color: Colors.orange)),
                                    onPressed: () async {
                                      final database = Provider.of<AppDatabase>(
                                          context,
                                          listen: false);
                                      final judulPlaylist = JudulPlaylist(
                                        judul_playlist: namaPlaylist.text,
                                        created_at: DateTime.now(),
                                      );

                                      database
                                          .insertJudulPlaylist(judulPlaylist);
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            });
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 15),
                            height: 115,
                            width: 115,
                            decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.14),
                              borderRadius: BorderRadius.circular(100),
                            ),
                            child: Center(
                                child: Icon(
                              Icons.add_circle,
                              color: Colors.deepOrange,
                              size: 40,
                            )),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                        ],
                      ),
                    )
                  : index == 0
                      ? Wrap(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            DetailFavorit('Favorit')));
                              },
                              child: StreamBuilder<int>(
                                stream: database.watchCounterFavorites(),
                                builder: (context, snapshot) {
                                  return CardPlay(
                                      "Favorit",
                                      "https://a4.ugziki.co.ug/no-cover-art/nocover-800x800.png",
                                      (snapshot.data ?? 0).toString() +
                                          " Lagu");
                                },
                              ),
                            ),
                          ],
                        )
                      : GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailPlaylist(
                                        playlists[index - 1].judul_playlist,
                                        playlists[index - 1].id)));
                          },
                          child: Wrap(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 3.7,
                                // height: MediaQuery.of(context).size.height / 5,
                                padding: EdgeInsets.only(bottom: 10),
                                margin: EdgeInsets.only(bottom: 10),
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.5),
                                      blurRadius:
                                          10.0, // has the effect of softening the shadow
                                      spreadRadius:
                                          0.0, // has the effect of extending the shadow
                                      offset: Offset(
                                        0.0, // horizontal, move right 10
                                        0.0, // vertical, move down 10
                                      ),
                                    )
                                  ],
                                  color:
                                      Theme.of(context).brightness.toString() ==
                                              'Brightness.dark'
                                          ? Color(0xff4d4d4d)
                                          : Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              8,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: new NetworkImage(
                                                  'https://a4.ugziki.co.ug/no-cover-art/nocover-800x800.png'),
                                              fit: BoxFit.cover,
                                            ),
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(5),
                                                topRight: Radius.circular(5)),
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 5,
                                          right: 5,
                                          child: Icon(
                                            Icons.play_circle_filled,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          width: 80,
                                          padding: EdgeInsets.only(left: 5),
                                          child: Text(
                                            playlists[index - 1].judul_playlist,
                                            style: TextStyle(
                                              fontSize: 13 + (5 * value),
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        PopupMenuButton<String>(
                                          child:
                                              Icon(Icons.more_vert, size: 15),
                                          onSelected: (choice) async {
                                            if (choice == PlaylistMenu.Hapus) {
                                              showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return AlertDialog(
                                                      title: Text(
                                                          'Apakah anda yakin?'),
                                                      actions: <Widget>[
                                                        new FlatButton(
                                                          child: new Text(
                                                              'BATAL',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .orange)),
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                        ),
                                                        new FlatButton(
                                                          child: new Text(
                                                              'HAPUS',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .orange)),
                                                          onPressed: () async {
                                                            final database =
                                                                Provider.of<
                                                                        AppDatabase>(
                                                                    context);

                                                            database
                                                                .deleteJudulPlaylist(
                                                                    playlists[
                                                                        index -
                                                                            1]);
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  });
                                            }
                                          },
                                          itemBuilder: (BuildContext context) {
                                            return PlaylistMenu.choices
                                                .map((String choice) {
                                              return PopupMenuItem<String>(
                                                value: choice,
                                                child: Text(choice),
                                              );
                                            }).toList();
                                          },
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Padding(
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 5),
                                        child: StreamBuilder<int>(
                                          stream: database.watchCounterPlayList(
                                              playlists[index - 1].id),
                                          builder: (context, snapshot) {
                                            return Text(
                                              (snapshot.data ?? 0).toString() +
                                                  ' Lagu',
                                              style: TextStyle(
                                                fontSize: 12 + (5 * value),
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            );
                                          },
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
            });
      },
    );
  }

  TextEditingController namaPlaylist = new TextEditingController();

  Future getDataLagu() async {
    http.Response response = await http.get(apiMaster + '/song', headers: {
      'Accept': 'application/json',
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> laguDataMentah = json.decode(response.body);
      laguSion = laguDataMentah["songs"];
    });
  }

  void initState() {
    super.initState();
    getDataLagu();
    ukuranText();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 10, top: 25),
      child: ListView(
        children: <Widget>[
          recentlyAddedSong(context),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(color: Colors.amber[800]),
                    child: SizedBox(
                      height: 50,
                      width: 5,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  tampil_text(
                      'playlist', 25, jenis_font, FontWeight.bold, null),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          judulPlaylist(context),

          // Wrap(
          //   alignment: WrapAlignment.spaceBetween,
          //   children: <Widget>[
          //     SizedBox(
          //       width: 10,
          //     ),
          //     CardPlay(
          //         "Lagu Buka S",
          //         "https://2.bp.blogspot.com/-Qj5AoG7dIL0/UuW6p9sc4rI/AAAAAAAAAWQ/AjlvH0Xiqsc/s1600/Nabi-Sulaiman-dan-Ratu-Bilqis.jpg",
          //         "23 Lagu"),
          //     SizedBox(
          //       width: 10,
          //     ),
          //     CardPlay(
          //         "Lagu Ketika S",
          //         "https://2.bp.blogspot.com/-mktiimkDjVY/U1Zd5-G8enI/AAAAAAAAAkI/bbmRpAszOHY/s1600/Kisah-Nabi-Dawud-AS.jpg",
          //         "15 Lagu"),
          //     SizedBox(
          //       width: 10,
          //     ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  void choiceAction(String choice) {
    if (choice == MenuMore.Unduh) {
      print('Unduh');
    }
  }
}

class MenuMore {
  static const String Unduh = 'Unduh';
  // static const String LaguUtama = 'Jadikan Lagu Utama';

  static const List<String> choices = <String>[
    Unduh,
    // LaguUtama,
  ];
}

class PlaylistMenu {
  static const String Hapus = 'Hapus';

  static const List<String> choices = <String>[
    Hapus,
  ];
}

class SongSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 200,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 10,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              transform: Matrix4.translationValues(-8, 0, 0),
                              child: Icon(
                                Icons.play_arrow,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.6),
                                size: 25.0,
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(8.0, 0.0, 0.0),
                              child: PopupMenuButton<String>(
                                child: Icon(Icons.more_vert, size: 18),
                                onSelected: (choice) async {},
                                itemBuilder: (BuildContext context) {
                                  return MenuMore.choices.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            Container(
                              height: 10,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}

class CardPlay extends StatefulWidget {
  final title;
  final image;
  final subtitle;
  var playlist;
  CardPlay(this.title, this.image, this.subtitle, {playlist});
  @override
  _CardPlayState createState() => _CardPlayState();
}

class _CardPlayState extends State<CardPlay> {
  var value = 0.1;
  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
    });
    Future.delayed(Duration(seconds: 1), () {
      ukuranText();
    });
  }

  void initState() {
    super.initState();
    ukuranText();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 3.7,
      // height: MediaQuery.of(context).size.height / 5,
      padding: EdgeInsets.only(bottom: 10),
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            blurRadius: 10.0, // has the effect of softening the shadow
            spreadRadius: 0.0, // has the effect of extending the shadow
            offset: Offset(
              0.0, // horizontal, move right 10
              0.0, // vertical, move down 10
            ),
          )
        ],
        color: Theme.of(context).brightness.toString() == 'Brightness.dark'
            ? Color(0xff4d4d4d)
            : Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height / 8,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: new NetworkImage(widget.image),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5)),
                ),
              ),
              Positioned(
                bottom: 5,
                right: 5,
                child: Icon(
                  Icons.play_circle_filled,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: 80,
                padding: EdgeInsets.only(left: 5),
                child: Text(
                  widget.title,
                  style: TextStyle(
                    fontSize: 13 + (5 * value),
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              widget.title == 'Favorit'
                  ? Container()
                  : PopupMenuButton<String>(
                      child: Icon(Icons.more_vert, size: 18),
                      onSelected: (choice) async {
                        if (choice == PlaylistMenu.Hapus) {
                          final database = Provider.of<AppDatabase>(context);

                          database.deleteJudulPlaylist(widget.playlist);
                        }
                      },
                      itemBuilder: (BuildContext context) {
                        return PlaylistMenu.choices.map((String choice) {
                          return PopupMenuItem<String>(
                            value: choice,
                            child: Text(choice),
                          );
                        }).toList();
                      },
                    ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                widget.subtitle,
                style: TextStyle(
                  fontSize: 12 + (5 * value),
                ),
                // overflow: TextOverflow.ellipsis,
              )),
        ],
      ),
    );
  }
}
