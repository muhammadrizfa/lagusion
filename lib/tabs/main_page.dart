import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/audiobook/list_audiobook.dart';
import 'package:music_mulai/detail/detail_pesan.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:music_mulai/detail/lagu/see_all_lagu.dart';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;
import 'package:music_mulai/tabs/buku.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../util/file_ext.dart';

class HalamanUtama extends StatefulWidget {
  @override
  _HalamanUtamaState createState() => _HalamanUtamaState();
}

class _HalamanUtamaState extends State<HalamanUtama> {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  List<dynamic> laguSion;
  final cacheManager = DefaultCacheManager();
  bool downloading = false;
  var progressString = "";
  var onMusicIdDownload;
  var _indexDownload;
  var value = 0.1;
  var language;
  var jenis_font = 'Roboto';

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
      if (prefs.getString('jenis_font') == null) {
        jenis_font = 'Roboto';
      } else {
        jenis_font = prefs.getString('jenis_font');
      }
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    var styleFont;
    if (fontstyle == 'Roboto') {
      styleFont = GoogleFonts.roboto(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Average') {
      styleFont = GoogleFonts.average(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Aleo') {
      styleFont = GoogleFonts.aleo(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Delius') {
      styleFont = GoogleFonts.delius(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    } else if (fontstyle == 'Vollkorn') {
      styleFont = GoogleFonts.vollkorn(
        textStyle: TextStyle(
          fontSize: ukuranfont + (5 * value),
          fontWeight: fontweight,
          color: color,
        ),
      );
    }
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: styleFont,
    );
  }

  Future<void> downloadFile(SongModel currentMusic) async {
    final database = Provider.of<AppDatabase>(context, listen: false);
    final song = Song(
      musicVersion: 1,
      idMusic: currentMusic.id,
      data: json.encode(currentMusic.toJson()),
      created_at: DateTime.now(),
    );
    await database.insertSong(song);
    FileInfo fileInfo = await cacheManager.getFileFromCache(currentMusic.music);
    String pathFile;
    if (fileInfo == null) {
      fileInfo = await cacheManager
          .getFileStream(currentMusic.music, withProgress: true)
          .listen((event) {
        if (event is DownloadProgress) {
          setState(() {
            onMusicIdDownload = currentMusic.id;
            downloading = true;
            progressString = "${event.progress}/ ${event.totalSize}";
          });
        }
      }).asFuture();
    }

    var dir = await getApplicationDocumentsDirectory();
    File newMusicFile =
        await fileInfo.file.copy("${dir.path}/${fileInfo.file.name}");
    pathFile = newMusicFile.path;

    final downloadedSongData = DownloadedSongData(
        songId: currentMusic.id,
        localFilePath: pathFile,
        createdAt: DateTime.now());
    await database.insertDownloadedSong(downloadedSongData);
    setState(() {
      downloading = false;
    });
  }

  Future getDataLagu() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    http.Response response = await http.get(apiMaster + '/song', headers: {
      'Accept': 'application/json',
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> laguDataMentah = json.decode(response.body);
      laguSion = laguDataMentah["songs"];
      if (prefs.getString('lagu_utama') == null) {
        prefs.setString('lagu_utama', laguSion[0]['album']['album_name']);
        prefs.commit();
      }
    });
  }

  List laguSionTrending;

  Future getDataLaguTrending() async {
    http.Response response =
        await http.get(apiMaster + '/song/trending', headers: {
      'Accept': 'application/json',
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> laguDataTrendingMentah = json.decode(response.body);
      laguSionTrending = laguDataTrendingMentah["songs"];
    });
  }

  List<dynamic> audioBook;

  Future getDataAudioBook() async {
    http.Response response =
        await http.get(apiMaster + '/audio-book', headers: {
      'Accept': 'application/json',
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> _audioBook = json.decode(response.body);
      audioBook = _audioBook["audio_books"];
      // print(audioBook);
    });
  }

  void initState() {
    super.initState();
    getDataAudioBook();
    getDataLagu();
    getDataLaguTrending();
    dataBanner();
    ukuranText();
  }

  StreamBuilder<List<SongModel>> showRecentSong(BuildContext context) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchAllSongFromRecentSong(),
      builder: (context, snapshot) {
        print('recent song $snapshot');
        final listSong = snapshot.data ?? List();
        if (listSong.isEmpty) {
          return Container();
        }
        return Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(color: Colors.amber[800]),
                      child: SizedBox(
                        height: 50,
                        width: 5,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    tampil_text(
                        'recent_song', 25, jenis_font, FontWeight.bold, null),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SeeAllLagu('recent song')));
                  },
                  child: tampil_text('see_all', 15, jenis_font, FontWeight.w500,
                      Colors.orange),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: laguSion == null
                    ? 3
                    : listSong.length < 3 ? listSong.length : 3,
                itemBuilder: (_, index) {
                  if (laguSion == null) {
                    return SongSkeleton();
                  } else {
                    return itemListMusic(listSong[index], index);
                  }
                },
              ),
            ),
          ],
        );
      },
    );
  }

  StreamBuilder<List<SongModel>> downloadedSong(BuildContext context) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchSongWithDownloadedSong(),
      builder: (context, AsyncSnapshot<List<SongModel>> snapshot) {
        if (snapshot.data != null) {
          if (snapshot.data.isNotEmpty) {
            final List<SongModel> songs = snapshot.data;
            return Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(color: Colors.amber[800]),
                          child: SizedBox(
                            height: 50,
                            width: 5,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        tampil_text(
                            'unduhan', 25, jenis_font, FontWeight.bold, null),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 10),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: songs.length,
                  itemBuilder: (_, int index) {
                    return Container(
                      padding: EdgeInsets.only(right: 10),
                      child: songs == null
                          ? SongSkeleton()
                          : Column(
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DetailMusicApp(
                                                indexCurrentPlay: index,
                                                listMusic: songs,
                                              )),
                                    );
                                  },
                                  child: Container(
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                            child: Image.network(
                                              songs[index].thumbnail,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Flexible(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    songs[index].title,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: Theme.of(context)
                                                                    .brightness
                                                                    .toString() ==
                                                                'Brightness.dark'
                                                            ? Colors.white
                                                            : Colors.black,
                                                        fontSize:
                                                            16 + (5 * value),
                                                        fontFamily: jenis_font),
                                                  ),
                                                  SizedBox(height: 5.0),
                                                  Text(
                                                    songs[index].artist,
                                                    style: TextStyle(
                                                      color: Theme.of(context)
                                                                  .brightness
                                                                  .toString() ==
                                                              'Brightness.dark'
                                                          ? Colors.white
                                                          : Colors.black
                                                              .withOpacity(0.5),
                                                      fontSize:
                                                          16 + (5 * value),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        'C 4/4',
                                                        style: TextStyle(
                                                          fontSize:
                                                              10 + (5 * value),
                                                        ),
                                                      ),
                                                      Container(
                                                        transform: Matrix4
                                                            .translationValues(
                                                                -8, 0, 0),
                                                        child: Icon(
                                                          Icons.play_arrow,
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.6),
                                                          size: 25.0,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  Column(
                                                    children: <Widget>[
                                                      Text(
                                                        songs[index]
                                                            .duration
                                                            .replaceAll(
                                                                new RegExp(
                                                                    r"\s+\b|\b\s"),
                                                                ""),
                                                        style: TextStyle(
                                                          fontSize:
                                                              14 + (5 * value),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Divider(),
                              ],
                            ),
                    );
                  },
                ),
              ],
            );
          }
        }

        return Container();
      },
    );
  }

  StreamBuilder<List<RecentBook>> recentBook(BuildContext context, id, index) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchWhereRecentBook(id),
      builder: (context, AsyncSnapshot<List<RecentBook>> snapshot) {
        final audioBooksRecent = snapshot.data ?? List();
        return Container(
          padding: EdgeInsets.only(right: 10),
          child: audioBooksRecent.toString() == '[]'
              ? Container()
              : Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {},
                      child: CardBook(audioBook[index]['audio_book_name'],
                          audioBook[index]['thumbnail'], "3. Di padang gurun"),
                    ),
                  ],
                ),
        );
      },
    );
  }

  StreamBuilder<List<SongModel>> showFavoritSong(BuildContext context) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchAllFavoritesSong(),
      builder: (context, snapshot) {
        final listFavoriteSong = snapshot.data;
        if (listFavoriteSong != null) {
          return Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(color: Colors.amber[800]),
                        child: SizedBox(
                          height: 50,
                          width: 5,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      tampil_text(
                          'favorit', 25, jenis_font, FontWeight.bold, null),
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SeeAllLagu('favorit')));
                    },
                    child: tampil_text('see_all', 15, jenis_font,
                        FontWeight.w500, Colors.orange),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Container(
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: listFavoriteSong.length,
                  itemBuilder: (_, index) {
                    return itemListMusic(listFavoriteSong[index], index);
                  },
                ),
              ),
            ],
          );
        }
        return Container();
      },
    );
  }

  StreamBuilder<List<RecentBook>> showRecentBook(BuildContext context) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchAllRecentBook(),
      builder: (context, AsyncSnapshot<List<RecentBook>> snapshot) {
        final audioBooksRecent = snapshot.data ?? List();
        return audioBooksRecent.toString() == '[]'
            ? Container()
            : Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(color: Colors.amber[800]),
                            child: SizedBox(
                              height: 50,
                              width: 5,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          tampil_text('recent_book', 25, jenis_font,
                              FontWeight.bold, null),
                        ],
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushReplacementNamed(context, "/buku");
                        },
                        child: tampil_text('see_all', 15, jenis_font,
                            FontWeight.w500, Colors.orange),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 165,
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: audioBook == null ? 0 : audioBook.length,
                      itemBuilder: (_, int index) {
                        var id = audioBook[index]['id'];
                        return recentBook(context, id, index);
                      },
                    ),
                  ),
                ],
              );
      },
    );
  }

  int _currentBanner = 0;

  Map<String, dynamic> _datagambar;
  List banner;
  List bannera = [0];

  Future<List> dataBanner() async {
    var url = apiMaster + "/banner";
    final response = await http.get(url, headers: {
      'Accept': 'application/json',
    });

    setState(() {
      _datagambar = json.decode(response.body);
      banner = _datagambar['banners'];
      var i = 0;
      bannera = [];
      while (i < banner.length) {
        bannera.add(i);
        i++;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        bannera.toString() == "[]"
            ? Container()
            : CarouselSlider(
                options: CarouselOptions(
                  enlargeCenterPage: true,
                  onPageChanged: (index, reason) {
                    // _onLoading();
                    setState(() {
                      _currentBanner = index;
                    });
                  },
                  height: 200.0,
                ),
                items: bannera.map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return banner == null
                          ? Container(
                              height: 200,
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.grey.withOpacity(0.3),
                              ),
                            )
                          : GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DetailPesan(
                                          banner[i]['img'],
                                          banner[i]['title'],
                                          banner[i]['message'])),
                                );
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.symmetric(horizontal: 5.0),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.network(banner[i]['img'],
                                      fit: BoxFit.cover),
                                ),
                              ),
                            );
                    },
                  );
                }).toList(),
              ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: bannera.map((url) {
              int index = bannera.indexOf(url);
              return GestureDetector(
                onTap: () {
                  print(_currentBanner);
                },
                child: Container(
                  width: 8.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _currentBanner == index
                        ? Colors.orange
                        : Colors.grey.withOpacity(0.3),
                  ),
                ),
              );
            }).toList(),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: EdgeInsets.only(left: 15, right: 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(color: Colors.amber[800]),
                        child: SizedBox(
                          height: 50,
                          width: 5,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      tampil_text(
                          'trending', 25, jenis_font, FontWeight.bold, null),
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SeeAllLagu('trending')));
                    },
                    child: tampil_text('see_all', 15, jenis_font,
                        FontWeight.w500, Colors.orange),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 3,
                    itemBuilder: (context, int index) {
                      return Container(
                          padding: EdgeInsets.only(right: 10),
                          child: laguSion == null
                              ? SongSkeleton()
                              : itemListMusic(
                                  SongModel.fromJson(laguSion[index]), index));
                    }),
              ),
              SizedBox(height: 10),
              showRecentSong(context),
              SizedBox(height: 10),
              showRecentBook(context),
              SizedBox(height: 10),
              showFavoritSong(context),
              SizedBox(height: 10),
              downloadedSong(context),
              SizedBox(height: 10),
            ],
          ),
        ),
      ],
    );
  }

  Widget itemListMusic(SongModel song, int index, {bool showMoreBtn = true}) =>
      Column(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              // Provider.of<HomeModel>(context).stop();
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailMusicApp(
                          indexCurrentPlay: index,
                          listMusic: laguSion
                              .map((e) => SongModel.fromJson(e))
                              .toList()
                        )),
              );
            },
            child: Container(
              child: Row(
                children: <Widget>[
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.4),
                        borderRadius: BorderRadius.circular(10)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        song.thumbnail,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  Flexible(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${song.index}. ${song.title}",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color:
                                      Theme.of(context).brightness.toString() ==
                                              'Brightness.dark'
                                          ? Colors.white
                                          : Colors.black,
                                  fontSize: 16.5 + (5 * value),
                                  fontFamily: jenis_font),
                            ),
                            SizedBox(height: 5.0),
                            Builder(
                              builder: (context) {
                                String desc = "";
                                if (onMusicIdDownload == song.id &&
                                    downloading) {
                                  desc =
                                      "${song.artist} -- Downloading $progressString";
                                } else {
                                  desc = song.artist;
                                }
                                return Text(
                                  desc,
                                  style: TextStyle(
                                    color: Theme.of(context)
                                                .brightness
                                                .toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.5),
                                    fontSize: 16.5 + (10 * value),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  song.basicNotes,
                                  style: TextStyle(
                                    fontSize: 10 + (5 * value),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {},
                                  child: Container(
                                    transform:
                                        Matrix4.translationValues(-8, 0, 0),
                                    child: Icon(
                                      Icons.play_arrow,
                                      color: Theme.of(context)
                                                  .brightness
                                                  .toString() ==
                                              'Brightness.dark'
                                          ? Colors.white
                                          : Colors.black.withOpacity(0.6),
                                      size: 25.0,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Container(
                                  transform:
                                      Matrix4.translationValues(8.0, 0.0, 0.0),
                                  child: showMoreBtn
                                      ? StreamBuilder<DownloadedSongData>(
                                          stream: Provider.of<AppDatabase>(
                                                  context)
                                              .getDownloadedSongById(song.id),
                                          builder: (context, snapshot) {
                                            List<String> menu = List();
                                            if (snapshot.data == null) {
                                              menu = MenuMore.choices;
                                            } else {
                                              menu = [MenuMore.Share];
                                            }
                                            return PopupMenuButton<String>(
                                              child: Icon(Icons.more_vert,
                                                  size: 18),
                                              onSelected: (choice) async {
                                                if (choice == MenuMore.Unduh) {
                                                  downloadFile(song);
                                                } else if (choice ==
                                                    MenuMore.Share) {
                                                  Share.share('Dengarkan lagu ' +
                                                      song.title +
                                                      ' di aplikasi Lagu Sion Plus');
                                                }
                                              },
                                              itemBuilder:
                                                  (BuildContext context) {
                                                return menu
                                                    .map((String choice) {
                                                  return PopupMenuItem<String>(
                                                    value: choice,
                                                    child: Text(choice),
                                                  );
                                                }).toList();
                                              },
                                            );
                                          },
                                        )
                                      : null,
                                ),
                                Text(
                                    song.duration
                                        .replaceAll(RegExp(r"\s+\b|\b\s"), ""),
                                    style:
                                        TextStyle(fontSize: 14 + (5 * value)))
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Divider(),
        ],
      );
}

class MenuMore {
  static const String Unduh = 'Unduh';
  static const String Share = 'Bagikan';

  static const List<String> choices = <String>[
    Unduh,
    Share,
    // LaguUtama,
  ];
}

class SongSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 200,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 10,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              transform: Matrix4.translationValues(-8, 0, 0),
                              child: Icon(
                                Icons.play_arrow,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.6),
                                size: 25.0,
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(8.0, 0.0, 0.0),
                              child: PopupMenuButton<String>(
                                child: Icon(Icons.more_vert, size: 18),
                                itemBuilder: (BuildContext context) {
                                  return MenuMore.choices.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            Container(
                              height: 10,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}
