import 'package:audioplayers/audioplayers.dart';
import 'package:equatable/equatable.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/bloc/lagusion_player_control_cubit.dart';

class LagusionPlayerState extends Equatable {
  @override
  List<Object> get props => [];
}

class Idle extends LagusionPlayerState {}

class LagusionIsPlaying extends LagusionPlayerState {
  final AudioPlayerState audioState;
  final dynamic currentPlayAudio;
  final AudioPlayerDetailType type;
  final List<dynamic> playList;
  final int currentIndex;
  final String thumb;
  LagusionIsPlaying(this.audioState, this.currentPlayAudio, this.type, this.playList, this.currentIndex, this.thumb);

  @override
  List<Object> get props => [audioState, currentPlayAudio, type];
}
