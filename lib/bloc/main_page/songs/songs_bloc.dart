import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:music_mulai/model/search_model/search_album_model.dart' as SearchAlbumModel;
import 'package:music_mulai/model/song_service/song_list_model.dart'
    as SongListModel;
import 'package:music_mulai/service/api_repository.dart';
import 'package:music_mulai/util/shared_data_get.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'songs_event.dart';
part 'songs_state.dart';

class SongsBloc extends Bloc<SongsEvent, SongsState> {
  SongsBloc() : super(SongsInitial());

  @override
  Stream<SongsState> mapEventToState(
    SongsEvent event,
  ) async* {
    final apiRepository = ApiRepository();
    SharedPreferences _prefs = await SharedPreferences.getInstance();

    if (event is SongsGetAlbums) {
      yield SongsLoading();
      try {
        final albums =
            await apiRepository.fetchRepoSearchAlbumModel(event.albumName);
        if (albums.status == 'OK') {
          yield SongsGetAlbumsSuccess(albums: albums.albums);
        } else {
          yield HitApiFailed('Gagal mendapatkan data');
        }
      } catch (e) {
        yield HitApiFailed(
            'Gagal mendapatkan data');
      }
    } else if (event is SongsGetList) {
      try {
        final songs = await apiRepository.fetchRepoSongListModel();

        if (songs.status == 'OK') {
          yield SongsGetListSongSuccess(
              listSong: songs.songs, listSongLink: songs.links);
        } else {
          yield HitApiFailed('Gagal mendapatkan data');
        }
      } catch (e) {
        yield HitApiFailed(
            'Gagal mendapatkan data');

        //Cara mendapatkan data dari local storage
        //Jangan lupa initiate SharedPreferences terlebih dahulu
        
        final List parsedSongs = json.decode(_prefs.get('songs'));
        List<SongListModel.Song> listSongResponse =
            new SongsModelResponse.fromJson(parsedSongs).list;

        final List parsedLinks = json.decode(_prefs.get('links'));
        List<SongListModel.Link> listLinkResponse =
            new LinksModelResponse.fromJson(parsedLinks).list;

        yield SongsGetListSongSuccess(
            listSong: listSongResponse, listSongLink: listLinkResponse);
      }
    }
  }
}
