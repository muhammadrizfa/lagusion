part of 'songs_bloc.dart';

abstract class SongsEvent extends Equatable {
  const SongsEvent();

  @override
  List<Object> get props => [];
}

class SongsGetAlbums extends SongsEvent{
  final String albumName;

  SongsGetAlbums({this.albumName});
}

class SongsGetList extends SongsEvent{}