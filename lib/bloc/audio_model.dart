import 'package:equatable/equatable.dart';


class SongModel {
  int id;
  int albumId;
  String title;
  String enTitle;
  String artist;
  String thumbnail;
  String duration;
  String basicNotes;
  String story;
  String musicalNotes;
  String music;
  String alias = "";
  int views;
  String createdAt;
  String updatedAt;
  int index;
  Album album;
  List<Verses> verses = List();
  List<SongVersions> songVersions;
  List<Verses> enVerses;
  List<Correlations> correlations = List();

  SongModel(
      {this.id,
      this.albumId,
      this.title,
      this.enTitle,
      this.artist,
      this.thumbnail,
      this.duration,
      this.basicNotes,
      this.story,
      this.musicalNotes,
      this.music,
      this.views,
      this.index,
      this.album,
      this.verses,
      this.songVersions,
      this.enVerses,
      this.correlations
      });

  SongModel.fromJson(Map<String, dynamic> json,
      {String otherSongVersionName, String otherVersionUrl}) {
    id = json['id'];
    albumId = json['album_id'];
    title = json['title'];
    enTitle = json['en_title'];
    artist = json['artist'];
    thumbnail = json['thumbnail'];
    duration = json['duration'];
    basicNotes = json['basic_notes'];
    story = json['story'];
    musicalNotes = json['musical_notes'];
    music = json['music'];
    views = json['views'];
    index = json['index'];
    album = json['album'] != null ? new Album.fromJson(json['album']) : null;
    if (json['verses'] != null) {
      verses = new List<Verses>();
      json['verses'].forEach((v) {
        verses.add(new Verses.fromJson(v));
      });
    }
    if (json['song_versions'] != null) {
      songVersions = new List<SongVersions>();
      json['song_versions'].forEach((v) {
        songVersions.add(new SongVersions.fromJson(v));
      });
    }
    if (json['en_verses'] != null) {
      enVerses = new List<Verses>();
      json['en_verses'].forEach((v) {
        enVerses.add(new Verses.fromJson(v));
      });
    }

    if (otherSongVersionName != null) {
      title = otherSongVersionName;
    }

    if (otherVersionUrl != null) {
      music = otherVersionUrl;
    }

    if (json['correlations'] != null) {
      correlations = new List<Correlations>();
      json['correlations'].forEach((v) {
        correlations.add(new Correlations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['album_id'] = this.albumId;
    data['title'] = this.title;
    data['en_title'] = this.enTitle;
    data['artist'] = this.artist;
    data['thumbnail'] = this.thumbnail;
    data['duration'] = this.duration;
    data['basic_notes'] = this.basicNotes;
    data['story'] = this.story;
    data['musical_notes'] = this.musicalNotes;
    data['music'] = this.music;
    data['views'] = this.views;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['index'] = this.index;
    if (this.album != null) {
      data['album'] = this.album.toJson();
    }
    if (this.verses != null) {
      data['verses'] = this.verses.map((v) => v.toJson()).toList();
    }
    if (this.songVersions != null) {
      data['song_versions'] = this.songVersions.map((v) => v.toJson()).toList();
    }
    if (this.enVerses != null) {
      data['en_verses'] = this.enVerses.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Verses {
  int id;
  int songId;
  String duration;
  String part;
  String lyrics;
  String createdAt;
  String updatedAt;

  Verses(
      {this.id,
      this.songId,
      this.duration,
      this.part,
      this.lyrics,
      this.createdAt,
      this.updatedAt});

  Verses.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    songId = json['song_id'];
    duration = json['duration'];
    part = json['part'];
    lyrics = json['lyrics'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['song_id'] = this.songId;
    data['duration'] = this.duration;
    data['part'] = this.part;
    data['lyrics'] = this.lyrics;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class SongVersions {
  int id;
  String versionName;
  String file;
  Null createdAt;
  String updatedAt;
  int songId;
  SongModel song;

  SongVersions(
      {this.id,
      this.versionName,
      this.file,
      this.createdAt,
      this.updatedAt,
      this.songId,
      this.song});

  SongVersions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    versionName = json['version_name'];
    file = json['file'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    songId = json['song_id'];
    song = json['song'] != null ? new SongModel.fromJson(json['song']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['version_name'] = this.versionName;
    data['file'] = this.file;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['song_id'] = this.songId;
    if (this.song != null) {
      data['song'] = this.song.toJson();
    }
    return data;
  }
}

class Album {
  int id;
  String thumbnail;
  String albumName;
  String createdAt;
  String updatedAt;

  Album(
      {this.id,
      this.thumbnail,
      this.albumName,
      this.createdAt,
      this.updatedAt});

  Album.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    thumbnail = json['thumbnail'];
    albumName = json['album_name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['thumbnail'] = this.thumbnail;
    data['album_name'] = this.albumName;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Correlations {
  int id;
  int albumId;
  String title;
  String code;
  String albumName;
  int correlation;

  Correlations(
      {this.id,
        this.albumId,
        this.title,
        this.code,
        this.albumName,
        this.correlation});

  Correlations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    albumId = json['album_id'];
    title = json['title'];
    code = json['code'];
    albumName = json['album_name'];
    correlation = json['correlation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['album_id'] = this.albumId;
    data['title'] = this.title;
    data['code'] = this.code;
    data['album_name'] = this.albumName;
    data['correlation'] = this.correlation;
    return data;
  }
}
