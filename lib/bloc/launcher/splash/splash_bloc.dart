import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:music_mulai/model/audio_book_model.dart';
import 'package:music_mulai/service/api_repository.dart';
import 'package:music_mulai/util/shared_data_save.dart';
import 'package:music_mulai/util/utils.dart';
import 'package:music_mulai/util/shared_data_get.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'splash_event.dart';
part 'splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc() : super(SplashInitial());

  @override
  Stream<SplashState> mapEventToState(
    SplashEvent event,
  ) async* {
    final apiRepository = ApiRepository();
    await GlobalFunction.checkConnection();

    if (event is SplashInit) {
      if (GlobalVariable.isConnected) {
        try {
          final banner = await apiRepository.fetchRepoBannerModel();
          if (banner.status == 'OK') {
            try {
              final trendingSong =
                  await apiRepository.fetchRepoSongTrendingModel();

              if (trendingSong.status == 'OK') {
                try {
                  final audioBook =
                      await apiRepository.fetchRepoAudioBookModel();

                  if (audioBook.status == 'OK') {
                    try {
                      final listSong =
                          await apiRepository.fetchRepoSongListModel();

                      if (listSong.status == 'OK') {
                        await SharedDataSave.saveDataSongList(listSong.songs, listSong.links);
                        await SharedDataSave.saveDataTrendingSongList(trendingSong.songs);
                        await SharedDataSave.saveDataBanner(banner.banners);
                        await GetSharedData.getAllData();

                        yield SplashGetDataSuccess();
                      }
                    } catch (e) {
                      yield HitApiFailed(GlobalVariable.textGetDataFailed, GlobalVariable.isConnected);
                    }
                  }
                } catch (e) {
                  yield HitApiFailed(GlobalVariable.textGetDataFailed, GlobalVariable.isConnected);
                }
              } else {
                yield HitApiFailed(GlobalVariable.textGetDataFailed, GlobalVariable.isConnected);
              }
            } catch (e) {
              yield HitApiFailed(GlobalVariable.textGetDataFailed, GlobalVariable.isConnected);
            }
          }
        } catch (e) {
          yield HitApiFailed(GlobalVariable.textGetDataFailed, GlobalVariable.isConnected);
        }
      } else {
        yield HitApiFailed(GlobalVariable.textOfflineMode, GlobalVariable.isConnected);
      }
    }
  }

}
