import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/audio_model.dart';
import 'bloc/lagusion_player_control_cubit.dart';
import 'bloc/lagusion_player_state.dart';
import 'detail/audiobook/detail_book.dart';
import 'detail/audiobook/detail_ibadah.dart';
import 'detail/lagu/detail_music.dart';
import 'global_audio_player.dart';

class MiniPlayerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LagusionPlayerControlCubit _playerControlCubit = context.bloc();
    return BlocBuilder<LagusionPlayerControlCubit, LagusionPlayerState>(
      builder: (context, state) {
        if (state is LagusionIsPlaying) {
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  if (state.type == AudioPlayerDetailType.PRAY) {
                    return DetailIbadah(
                        state.currentPlayAudio['title'],
                        state.playList[state.currentIndex],
                        state.currentIndex,
                        state.playList,
                        state.thumb);
                  }
                  if (state.type == AudioPlayerDetailType.BOOK) {
                    return DetailBook(
                        state.currentPlayAudio['title'],
                        state.playList[state.currentIndex],
                        state.currentIndex,
                        state.playList,
                        state.thumb);
                  }

                  return DetailMusicApp(
                    indexCurrentPlay: state.currentIndex,
                    listMusic: state.playList
                        .map((e) => SongModel.fromJson(e))
                        .toList()
                  );
                }),
              );
            },
            child: Container(
              padding: EdgeInsets.all(16),
              child: Row(
                children: [
                  Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: NetworkImage(
                            state.currentPlayAudio['thumbnail'] ??
                                state.thumb ??
                                ""),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(width: 8),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          state.currentPlayAudio['title'],
                          style:
                          TextStyle(fontSize: 16, color: Colors.black87),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(state.currentPlayAudio['artist'] ?? "-",
                            style: TextStyle(
                                fontSize: 16, color: Colors.black87),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis)
                      ],
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.skip_previous),
                    onPressed: () => _playerControlCubit.previousAudio(),
                  ),
                  Builder(
                    builder: (context) {
                      if (state.audioState != AudioPlayerState.PLAYING) {
                        return IconButton(
                          icon: Icon(Icons.play_arrow),
                          onPressed: () =>
                              LagusionAudioPlayer.instance.resume(),
                        );
                      }
                      return IconButton(
                        icon: Icon(Icons.pause),
                        onPressed: () => LagusionAudioPlayer.instance.pause(),
                      );
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.skip_next),
                    onPressed: () => _playerControlCubit.nextAudio(),
                  ),
                  IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () => LagusionAudioPlayer.instance.stop(),
                  )
                ],
              ),
            ),
          );
        }

        return Container();
      },
    );
  }

}
