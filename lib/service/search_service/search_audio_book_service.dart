import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/model/search_model/search_audio_book_model.dart';

Future<SearchAudioBookModel> fetchSearchAudioBookModel(
    String baseUrl, String searchText) async {
  try {
    Map<String, String> body;

    body = {'search': searchText};

    print('fetchSearchAudioBookModel Url ' + baseUrl);

    print('fetchSearchAudioBookModel Req ' + body.toString());

    final response = await http
        .post(baseUrl, body: body)
        .timeout(const Duration(seconds: 60));

    print('fetchSearchAudioBookModel Res ' + response.body);

    return compute(parsePosts, response.body);
  } catch (_) {
    return null;
  }
}

SearchAudioBookModel parsePosts(String responseBody) {
  final parsed = json.decode(responseBody);
  var data = Map<String, dynamic>.from(parsed);
  SearchAudioBookModel eventResponse = SearchAudioBookModel.fromJson(data);
  return eventResponse;
}
