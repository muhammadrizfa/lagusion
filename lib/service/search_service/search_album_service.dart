import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/model/search_model/search_album_model.dart';

Future<SearchAlbumModel> fetchSearchAlbumModel(
    String baseUrl, String albumName) async {
  try {
    Map<String, String> body;

    body = {'album_name': albumName};

    print('fetchSearchAlbumModel Url ' + baseUrl);

    print('fetchSearchAlbumModel Req ' + body.toString());

    final response = await http
        .post(baseUrl, body: body)
        .timeout(const Duration(seconds: 60));

    print('fetchSearchAlbumModel Res ' + response.body);

    return compute(parsePosts, response.body);
  } catch (_) {
    return null;
  }
}

SearchAlbumModel parsePosts(String responseBody) {
  final parsed = json.decode(responseBody);
  var data = Map<String, dynamic>.from(parsed);
  SearchAlbumModel eventResponse = SearchAlbumModel.fromJson(data);
  return eventResponse;
}
