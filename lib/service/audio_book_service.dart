import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/model/audio_book_model.dart';

Future<AudioBookModel> fetchAudioBookModel(String url) async {
  try {
    debugPrint('fetchAudioBookModel url : ' + url);

    final response = await http.get(url);

    debugPrint('fetchAudioBookModel Response : ' + response.body);

    return compute(parsePosts, response.body);
  } catch (_) {
    return null;
  }
}

AudioBookModel parsePosts(String responseBody) {
  final parsed = json.decode(responseBody);
  var data = Map<String, dynamic>.from(parsed);
  AudioBookModel eventResponse = AudioBookModel.fromJson(data);
  return eventResponse;
}
