import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/model/ibadah_model.dart';

Future<IbadahModel> fetchIbadahModel(String url) async {
  try {
    debugPrint('fetchIbadahModel url : ' + url);

    final response = await http.get(url);

    debugPrint('fetchIbadahModel Response : ' + response.body);

    return compute(parsePosts, response.body);
  } catch (_) {
    return null;
  }
}

IbadahModel parsePosts(String responseBody) {
  final parsed = json.decode(responseBody);
  var data = Map<String, dynamic>.from(parsed);
  IbadahModel eventResponse = IbadahModel.fromJson(data);
  return eventResponse;
}
