import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/model/song_service/song_list_model.dart';

Future<SongListModel> fetchSongListModel(String url) async {
  try {
    debugPrint('fetchSongListModel url : ' + url);

    final response = await http.get(url);

    debugPrint('fetchSongListModel Response : ' + response.body);

    return compute(parsePosts, response.body);
  } catch (_) {
    return null;
  }
}

SongListModel parsePosts(String responseBody) {
  final parsed = json.decode(responseBody);
  var data = Map<String, dynamic>.from(parsed);
  SongListModel eventResponse = SongListModel.fromJson(data);
  return eventResponse;
}
