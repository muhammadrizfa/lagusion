import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/model/song_service/song_trending_model.dart';

Future<SongTrendingModel> fetchSongTrendingModel(String url) async {
  try {
    debugPrint('fetchSongTrendingModel url : ' + url);

    final response = await http.get(url);

    debugPrint('fetchSongTrendingModel Response : ' + response.body);

    return compute(parsePosts, response.body);
  } catch (_) {
    return null;
  }
}

SongTrendingModel parsePosts(String responseBody) {
  final parsed = json.decode(responseBody);
  var data = Map<String, dynamic>.from(parsed);
  SongTrendingModel eventResponse = SongTrendingModel.fromJson(data);
  return eventResponse;
}
